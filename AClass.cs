
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Wikimedia.MilHist
{
    public class AClassException : Exception
    {
        private readonly Page page;
        private readonly string message;

        public void Warning()
        {
            Cred.Instance.Warning(page.Title + ": " + message);
        }

        public AClassException(Page page, string message)
        {
            this.page = page;
            this.message = message;
        }
    }

    public class AClassMilestone : IMilestone
	{
		public string Action { get => "WAR"; }
		public string Date   { get; }
		public string Link   { get; }
		public int RevId     { get; }
        public string Result { get; }

		public override string ToString()
		{
			return ("date=" + Date + " link=" + Link + " oldid=" + RevId + " result=" + Result);
		}

		public AClassMilestone(AClassNominationPage nominationPage, string result)
        {
			DateTime dt = nominationPage.Talk.Revision.Timestamp;
            Date = dt.ToString("HH:mm:ss dd MMMM yyyy (UTC)");
			Link = nominationPage.Title;
            RevId = ArticleHistory.GetRevId(nominationPage.Article, dt);
			Result = result;
		}
    }

	public class ArchiveMainPage : Page
	{
		public const string AssessmentPath = MilHist.ProjectPath + "/Assessment";
		private readonly int year;

        public static string Path(string year)
        {
            return AssessmentPath + "/" + year;
        }

		private Section AssessmentSection(string status)
		{
			Link link = new Link(AClassArchivePage.Path(year, status));
			Section section = new Section(this, "promoted");
			section.Add(link);
			return section;
		}

		internal void Archive()
		{
			try
			{
				Load();
			}
			catch (PageMissingException)
			{
				Template archive = new Template("WPMILHIST Archive");
				archive.Add("category", "review");
				Template navigation = new Template("WPMILHIST Navigation");
				Add(archive, Token.Newline, navigation, Token.Newline, new Token(" __FORCETOC__"), Token.Newline);

				Section promoted = AssessmentSection("Promoted");
				Section failed = AssessmentSection("Failed");
				Section kept = AssessmentSection("Kept");
				Section demoted = AssessmentSection("Demoted");
				Add(promoted, failed, kept, demoted);

				Link category = new Link("Category:Requests for military history A-Class review|");
				Add(category, Token.Newline);
				Save("Create new archive page for " + year);
            }
        }

        public ArchiveMainPage(int year) : base(Path(year.ToString()))
		{
			this.year = year;
		}
	}

	public class AClassArchivePage : Page
	{
		private AClassNominationPage nomination;

        public static string Path(int year, string status)
        {
            return ArchiveMainPage.Path(DateTime.Now.Year.ToString()) + "/" + status;
        }

        public void Archive()
		{
			var status = nomination.Talk.Status;
			var year = DateTime.Now.Year;

            var mainPage = new ArchiveMainPage(year);
			mainPage.Archive();

			try
			{
				Load();
			}
			catch (PageMissingException)
			{
				Section section = new Section(this, status);
				section.Add(
					new Comment("Please add new reviews directly below this line"),
					Token.Newline,
					Token.Newline,
					new Comment("Add archived reviews at the top please"),
					Token.Newline,
					Token.Newline,
					new Token("<noinclude>"),
					new Link("Category:" + status + " requests for military history A-Class review|"),
					new Token("</noinclude>"),
					Token.Newline
				);
				Add(section);
				Save("Created new page for " + year + "/" + status);
			}

			Comment? comment = FindComment("Please add new reviews directly below this line");
			if (comment == null)
			{
				throw new AClassException(this, "Could not find 'add new reviews' comment");
			}

			var nominationTemplate = new Template(this, nomination.Title);
			Insert(EndOfLine(comment), Token.Newline, nominationTemplate);			
        }

		public AClassArchivePage(AClassNominationPage nomination): base (Path(DateTime.Now.Year, nomination.Talk.Status))
		{
			this.nomination = nomination;
        }
    }

	public class AClassArticlePage : ArticlePage
	{
		public AClassArticlePage(AClassNominationPage nomination, string title) : base(title)
		{
		}
	}

	public class AClassTalkPage : TalkPage
	{		
		private string? status;
		private Revision revision;
		private bool revisionFound;
		private string? coordinator;
		private AClassNominationPage nomination;

		public Revision Revision
		{
			get
			{
				if (!revisionFound)
				{
					var history = History();
					revision = history.First(); // debugging - initialise to a valid value
					foreach (var item in history)
					{
						AClassTalkPage itemPage = new AClassTalkPage(nomination, Title, item.RevId);
						if (!itemPage.Status.Equals(Status))
						{
							revisionFound = true;
							break;
						}
						revision = item;
					}
				}
				if (!revisionFound)
				{
					throw new AClassException(this, "Could not find item in history");
				}
				return revision;
			}
		}

		public string Coordinator
		{
			get
			{
				if (null == coordinator)
				{
					coordinator = Revision.User;
					if (!MilHist.IsCoordinator(coordinator))
					{
						throw new AClassException(this, coordinator + " is not a MILHIST coordinator");
					}
				}
                return coordinator;
			}
			set
			{
				coordinator = value;
            }
		}

		public string Status
		{
			get
			{
				if (null == status)
				{
					Load();
					Debug.WriteLine("project template=" + MilHist.ProjectTemplate);
					status = MilHist.ProjectTemplate["A-Class"];
                    if (null == status)
                    {
                        throw new AClassException(this, "unable to find A-Class parameter in MilHist project template");
                    }

					var statusNames = new Dictionary<string, string>()
					{
						{ "current", "Current"  },
						{ "pass",	 "Promoted" },
						{ "passed",  "Promoted" },
						{ "fail",	 "Failed"   },
						{ "failed",  "Failed"   },
						{ "kept",    "Kept"     },
						{ "demoted", "Demoted"  },
					};

					try
					{
						status = statusNames[status];
					}
					catch (KeyNotFoundException)
					{
						throw new AClassException(this, "unknown status: '" + status + "'");
					}
				}
                return status;
			}
			set
			{
				Load();
                Debug.Entered("project template=" + MilHist.ProjectTemplate);
				var statusNames = new Dictionary<string, string>()
                {
                    { "Current",  "current" },
                    { "Promoted", "pass"    },
                    { "Failed",   "failed"  },
                    { "Kept",     "kept"    },
                    { "Demoted",  "demoted" },
                };

                try
                {
					MilHist.ProjectTemplate["A-Class"] = statusNames[value];
                    status = value;
                }
                catch (KeyNotFoundException)
                {
                    throw new AClassException(this, "unknown status: '" + status + "'");
                }
                Debug.Exited("project template=" + MilHist.ProjectTemplate);
            }
		}

		public void Update ()
		{
            var resultNames = new Dictionary<string, string>()
            {
                { "Promoted", "approved" },
                { "Failed",   "failed"   },
                { "Kept",     "kept"     },
                { "Demoted",  "demoted"  },
            };

			try
			{
				var result = resultNames[Status];
                var milestone = new AClassMilestone(nomination, result);
                ArticleHistory.Add(milestone);
            }
            catch (KeyNotFoundException)
            {
                throw new AClassException(this, "invalid status: '" + status + "'");
            }
        }

        public AClassTalkPage(AClassNominationPage nomination, string title, int revid = -1) : base(title, revid)
		{
			this.nomination = nomination;
		}
	}

	public class AClassNominationPage : NominationPage
	{
		private AClassArticlePage? article;
		private AClassTalkPage? talk;
		private AClassArchivePage? archive;

        public new AClassArticlePage Article
        {
            get
            {
                if (null == article)
                {
                    var title = GetArticleTitle();
                    article = new AClassArticlePage(this, title);
                }
                return article;
            }
        }

        public new AClassTalkPage Talk
        {
            get
            {
                if (null == talk)
                {
                    var title = GetArticleTitle();
                    talk = new AClassTalkPage(this, "Talk:" + title);
					if (talk.IsRedirect)
					{
						var redirects = talk.RedirectsTo;
						if (null != redirects)
						{
							talk = new AClassTalkPage(this, redirects.Title);
						}
					}
                }
                return talk;
            }
        }

        public AClassArchivePage ArchivePage
		{
			get
			{
				if (null == archive)
				{
					archive = archive = new AClassArchivePage(this);
                }
				return archive;
            }
        }


		public void Close()
		{
			var top = new Template(this, "archive top");
			top.Add("status", "none");
			top.Add("result", Talk.Status + " ~~~~");
			Insert(Top(), top, Token.Newline);
			var bottom = new Template(this, "archive bottom");
			Add(bottom, Token.Newline);
		}

        public AClassNominationPage (string name): base (name)
		{
            Action = "ACR";
            TitleMatch = ArchiveMainPage.AssessmentPath + @"/(?<title>(.+))";
        }
    }

	public class AClassNominationsPage : NominationsPage
	{
        // Create as a singleton 
        private static readonly Lazy<AClassNominationsPage> _instance = new Lazy<AClassNominationsPage>(() => new AClassNominationsPage());
        public static AClassNominationsPage Instance => _instance.Value;

		private static readonly string title = ArchiveMainPage.AssessmentPath + "/A-Class review";
		private List<AClassNominationPage> aclassNominations = new List<AClassNominationPage>();

        public new List<AClassNominationPage> Nominations
		{
			get
			{
				if (! aclassNominations.Any())
				{
					var nominations = base.Nominations();
					foreach (var page in nominations)
					{
						aclassNominations.Add(new AClassNominationPage(page.Title));
					}
				}
				return aclassNominations;
			}
		}

		public void Add(AClassNominationPage nomination)
		{
			Load();
			var nominationTemplate = new Template(this, nomination.Title);
			var template = Fetch(nomination.TitleMatch);
			Insert(template, nominationTemplate, new Token("\n"));
		}

		private AClassNominationsPage() : base(title)
        {
            TitleMatch = ArchiveMainPage.AssessmentPath + @"/(?!ACR/)(?<title>.+)";
		}
	}
}