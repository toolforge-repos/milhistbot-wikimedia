using System;
using System.Collections.Generic;
using Wikimedia.Featured;

namespace Wikimedia.MilHist
{
	public class AnnouncementsTemplate : Template
	{
		public new const string Name = "WPMILHIST Announcements/Shell";
		private static Comment BotGenerated => new Comment("Bot generated");

		public static bool IsAnnouncements(string name)
		{
			return Name.Equals(name);
		}

		private static string FormatList(List<string> strings)
        {
			return String.Join(" &bull; ", strings) + BotGenerated + "\n" + "\n";
		}


		private static string FormatList(List<AClassNominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<FeaturedNominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<FeaturedReviewNominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<GANominationPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				Template template = new Template("WPMHA/GAN");
                template["1"] = nomination.Article.Title;
				template["2"] = nomination.Version;
				strings.Add(template.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<GAReassessmentPage> nominations)
		{
			List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
			}
			return FormatList(strings);
		}

		private static string FormatList(List<NominationPage> nominations)
		{
            List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
             }
            return FormatList(strings);
		}

		private static string FormatList(List<PeerReviewPage> nominations)
		{
            List<string> strings = new List<string>();
			foreach (var nomination in nominations)
			{
				var link = new Link(nomination.Title, nomination.Article.Title);
				strings.Add(link.Text);
             }
            return FormatList(strings);
		}

		public void RebuildFeatured()
		{

			var nominations = FeaturedNominationsPage.Instance.Nominations.FindAll(item => MilHist.IsMilHist(item.ArticleTalk));
            this["featured_article_candidates"] = FormatList(nominations);
		}

		public void RebuildFeaturedReview()
		{
			var nominations = FeaturedReviewNominationsPage.Instance.Nominations.FindAll(item => MilHist.IsMilHist(item.ArticleTalk));
			this["featured_article_reviews"] = FormatList(nominations);
		}

		public void RebuildFeaturedList()
		{
			var nominations = FeaturedListNominations.Instance.Nominations().FindAll(item => MilHist.IsMilHist(item.ArticleTalk));
			this["featured_list_candidates"] = FormatList(nominations);
		}

		public void RebuildAClass()
		{
			var nominations = AClassNominationsPage.Instance.Nominations;
			this["A-Class_reviews"] = FormatList(nominations);
		}

        public void RebuildPeerReview()
		{
			var nominations = PeerReviewNominationsPage.Instance.Nominations.FindAll(item => MilHist.IsMilHist(item.Article));
			this["peer_reviews"] = FormatList(nominations);
		}

		public void RebuildGoodArticles()
		{
			var nominations = GANominationsPage.Instance.GANominations.FindAll(item => MilHist.IsMilHist(item.Article));
			this["good_article_nominees"] = FormatList(nominations);
		}

		public void RebuildGoodArticleReassessments()
		{
			var nominations = GAReassessmentsPage.Instance.GAReassessments.FindAll(item => MilHist.IsMilHist(item.Article));
			this["good_article_reasessments"] = FormatList(nominations);
		}

		public void Rebuild()
		{
			RebuildFeatured();
			RebuildFeaturedReview();
			RebuildFeaturedList();
			RebuildAClass();
			RebuildPeerReview();
			RebuildGoodArticles();
			RebuildGoodArticleReassessments();
		}

		public AnnouncementsTemplate(Page page, Tokens tokens) : base(page, tokens)
		{   
		}
	}

	public class AnnouncementsPage : TemplatePage
	{
		new const string Title = "Template:WPMILHIST Announcements";

		private AnnouncementsTemplate? announcementsTemplate;

		public AnnouncementsTemplate AnnouncementsTemplate
		{
			get
			{
				if (null == announcementsTemplate)
				{
					Load();
					var template = Find(AnnouncementsTemplate.Name);
                    if (null == template)
                    {
                        throw new Exception("Cannot find '" + AnnouncementsTemplate.Name + "' on page");
                    }

					announcementsTemplate = template as AnnouncementsTemplate;
                    if (null == announcementsTemplate)
					{
						throw new Exception("Cannot find " + AnnouncementsTemplate.Name + " (" + typeof(Template) + ")");
					}
				}
				return announcementsTemplate;
			}
		}

        public List<string> GetReviews(string heading)
        {
            var reviews = new List<string>();
            var links = Links.FindAll(ind => ind.Data.Matches(heading));
			foreach (var link in links)
			{
				reviews.Add(link.Data);
			}
            return reviews;
        }

        public List<string> GetFeaturedCandidates()
        {
            return GetReviews("Featured article candidate");
        }

        public List<string> GetFeaturedReviews()
        {
			return GetReviews("Featured article review");
        }

        public List<string> GetGoodArticleReassessments()
        {
            return GetReviews("Good article reassessment");
        }

        public List<string> GetGoodArticleNominations()
		{
			var reassessments = new List<string>();
			var templates = FindAll("WPMHA/GAN");
			foreach (var template in templates)
			{
				reassessments.Add(template.Fetch("1"));
            }
			return reassessments;
        }

        public void RebuildAClass()
        {
            AnnouncementsTemplate.RebuildAClass();
        }

		public void Rebuild()
		{
			AnnouncementsTemplate.Rebuild();
		}

		public AnnouncementsPage(int revid = -1) : base(Title, revid)
		{
		}
	}
}
