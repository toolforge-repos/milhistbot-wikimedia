﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text.RegularExpressions;
using Wikimedia.Featured;

namespace Wikimedia
{
	public class AnnouncementsPage : TemplatePage
	{
		// Create as a singleton 
		private static readonly Lazy<AnnouncementsPage> _instance = new Lazy<AnnouncementsPage>(() => new AnnouncementsPage());
		public static AnnouncementsPage Instance => _instance.Value;
        private static Dictionary<Featured.ContentType, string> Lookup = new Dictionary<Featured.ContentType, string>()
        {
            { Featured.ContentType.article, "Articles" },
            { Featured.ContentType.list,    "Lists"    },
            { Featured.ContentType.picture, "Pictures" },
            { Featured.ContentType.topic,   "Topics"   },
        };

        private Comment GetComment (Featured.ContentType content)
        {
            Debug.Entered("content=" + content);
            string contentString = Lookup[content];
            string pattern = contentString + @" \((?<maximum>\d+), most recent first\)";
            var comment = FindComment(pattern);
            if (null == comment)
            {
                throw new Exception("unable to find featured content '" + content + "'");
            }
            Debug.Exited("comment=" + comment);
            return comment;
        }

        private int Maximum (Featured.ContentType content)
		{
            Debug.Entered("content=" + content);
            var comment = GetComment(content);
            string contentString = Lookup[content];
            string pattern = contentString + @" \((?<maximum>\d+), most recent first\)";
            MatchCollection matches = Regex.Matches(comment.Value, pattern);
            string maxstring
                = matches[0].Groups["maximum"].Value;
            int maximum = Int32.Parse(maxstring);
            Debug.Exited("maximum=" + maximum);
            return maximum;
        }

        private List<Link> PageLinks(Featured.ContentType content)
        {
            Debug.Entered("content=" + content);
            var pageLinks = new List<Link>();
            var comment = GetComment(content);
            var commentIndex = FindIndex(comment);
            var contents = Contents.Contents;
            for (int i = commentIndex + 1; i < contents.Count; i++)
            {
                if (contents[i].Type is TokenType.Link)
                {
                    pageLinks.Add((Link)contents[i]);
                }
                if (contents[i].Type is TokenType.Text && contents[i].Text.Matches("</div"))
                {
                    break;
                }
            }
            Debug.Exited();
            return pageLinks;
        }

        private Link PageToLink(Featured.ContentType content, Page page)
        {
            Link pageLink = new Link(page.Title);
            if (Featured.ContentType.list == content && page.Title.Matches("^List of "))
            {
                pageLink.Redirect = page.Title.Remove(0, "List of ".Length);
            }
            if (Featured.ContentType.topic == content)
            {
                pageLink.Redirect = page.Title.Remove(0, "Wikipedia:Featured topics/".Length);
            }
            return pageLink;
        }

        public void Add(Featured.ContentType content, Page page)
        {
            Debug.Entered("content=" + content);
            var pageLinks = PageLinks(content);
            var maximum = Maximum(content);
            while (pageLinks.Count >= maximum)
            {
                RemoveLine(pageLinks.First());
                pageLinks.RemoveAt(0);
            }

            Link link = PageToLink(content, page);
            Insert(StartOfLine(pageLinks.First()), new Token("* "), link, Token.Newline);
            Debug.Exited();
        }

        private AnnouncementsPage(): base("Template:Announcements/New featured content")
		{
            Load();
		}
	}
}
