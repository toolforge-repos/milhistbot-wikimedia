using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public interface IMilestone
	{
		string Action { get; }
		string Date { get; }
		string Link { get; }
		int RevId { get; }
		string Result { get; }
	}

	public class ArticleForDeletion : Template, IMilestone
	{
        new TalkPage Page;

		private DateTime _timestamp;
		private bool _timestampSet = false;

		public static bool IsArticleForDeletion(string name)
		{
			return name.Equals("old AfD", StringComparison.OrdinalIgnoreCase) || name.Equals("Old Afd multi", StringComparison.OrdinalIgnoreCase);
		}

		public string Action
		{
			get => "AFD";
		}

		public string Date
		{
			get => Timestamp().ToString("HH:mm:ss dd MMMM yyyy (UTC)");
		}

		public string Link
		{
			get => "Wikipedia:Articles for deletion/" + Fetch ("page", Page.Article.Title);
 		}

		public string Result
		{
			get => Fetch ("result").Trim(new Char[] { '\'' });
		}

		public int RevId
		{
			get => ArticleHistory.GetRevId(Page.Article, Date);
		}

		private DateTime Timestamp()
		{
			if (!_timestampSet)
			{
				_timestamp = Exists("date") ? ArticleHistory.ParseTimestamp(Fetch("date")) : Page.Article.History(1).First().Timestamp;
				_timestampSet = true;
			}
			return _timestamp;
		}

		public ArticleForDeletion(TalkPage page, Tokens tokens) : base(page, tokens)
		{
			Page = page;
		}
	}

	public class GoodArticles : Template, IMilestone
	{
        new TalkPage Page;

		private string? _timestamp;

		public static bool IsGoodArticle(string name)
		{
			return name.Equals("GA", StringComparison.OrdinalIgnoreCase);
		}

		public string Action
		{
			get => "GA";
		}

		public string Date
		{
			get => ArticleHistory.FormatTimestamp(Timestamp);
		}

		public string Link
		{
			get => Page.Title + "/GA" + Fetch("page", "1");
		}

		public string Result
		{
			get => "listed";
		}

		public int RevId
		{
			get => Exists("oldid") ? Convert.ToInt32(Fetch("oldid")) : ArticleHistory.GetRevId(Page.Article, Timestamp);
		}

		public string Timestamp
		{
			get
			{
				if (null == _timestamp)
				{
					_timestamp = Exists("date") ? Fetch("date") : Fetch("1");
				}
				return _timestamp;
			}
		}

		public GoodArticles(TalkPage page, Tokens tokens) : base(page, tokens)
		{
            Page = page;
        }
    }

	public class OldPeerReview : Template, IMilestone
	{
        new TalkPage Page;

		private DateTime _timestamp;
		private bool _timestampSet = false;

		public static bool IsOldPeerReview(string name)
		{
			return name.Equals("Old peer review", StringComparison.OrdinalIgnoreCase) || name.Equals("Oldpeerreview", StringComparison.OrdinalIgnoreCase);
		}

		public string ArticleName
		{
			get => Fetch("1", Fetch("reviewedname", Page.Article.Title));
		}

		public DateTime Timestamp()
		{
			if (!_timestampSet)
			{
				var linkPage = new Page(Link);
				var revision = linkPage.History(1).First();
				_timestamp = revision.Timestamp;
				_timestampSet = true;
			}
			return _timestamp;
		}

		public string Action
		{
			get => "PR";
		}

		public string Date
		{
			get
			{
				if (Exists("date"))
				{
					try
					{
						var date = ArticleHistory.FormatTimestamp(Fetch("date"));
						if (!date.Matches("00:00:00 01"))
						{
							return date;
						}
					}
					catch (ArticleHistoryDateException)
					{
						// Drop through
					}
				}
				return Timestamp().ToString("HH:mm:ss dd MMMM yyyy (UTC)");
			}
		}

		public string Link
		{
			get
			{
				string link;
				if (Exists("archivelink"))
				{
					link = Fetch("archivelink");
				}
				else
				{
					string archive = "/archive" + Fetch("archive", "");
					link = "Wikipedia:Peer review/" + ArticleName + archive;
				}
				return link;
			}
		}

		public string Result
		{
			get => "reviewed";
		}

		public int RevId
		{
			get => Exists("ID") ? Int32.Parse(Fetch("ID")) : ArticleHistory.GetRevId(Page.Article, Timestamp());
		}

		public OldPeerReview(TalkPage page, Tokens tokens) : base(page, tokens)
		{
            Page = page;
        }
    }

	public class Milestone : IMilestone
	{
		private readonly int id;
		private readonly string action;
		private readonly string date;
        private readonly string link;
		private readonly int revId;
		private readonly string result;

		public string Action
		{
			get => action;
		}

		public string Date
		{
			get => date;
		}

		public string Link
		{
			get => link;
		}

		public int RevId
		{
			get => revId;
		}

		public string Result
		{
			get => result;
		}

		public override string ToString()
		{
			return ("id=" + id + " date=" + Date + " link=" + Link + " oldid=" + RevId + " result=" + Result);
		}

		public Milestone(int id, string action, string date, string link, int revId, string result)
		{
			this.id = id;
			this.action = action;
			this.date = date;
			this.link = link;
			this.revId = revId;
			this.result = result;
		}

		public Milestone(string action, string date, string link, int revId, string result) : this(-1, action, date, link, revId, result)
		{
		}

	}

	public interface IArticleHistoryItem
	{
		Dictionary<string, string> KeyValuePairs();
	}

	public class DidYouKnow : Template, IArticleHistoryItem
	{
        new TalkPage Page;
        readonly Dictionary<string, string> dyk;

		public static bool IsDidYouKnow(string name)
		{
			return name.Equals("DYK talk", StringComparison.OrdinalIgnoreCase) || name.Equals("DYKtalk", StringComparison.OrdinalIgnoreCase);
		}

		public Dictionary<string, string> KeyValuePairs()
		{
			if (0 == dyk.Count)
			{
				string date = Fetch("1") + " " + Fetch("2");
				string entry = Fetch("entry", Fetch("3", ""));
				string nompage = Fetch("nompage", "Template:Did you know nominations/" + Page.Article.Title);
				dyk.Add("dykdate", date);
				dyk.Add("dykentry", entry);
				dyk.Add("dyknom", nompage);
			}
			return dyk;
		}

		public DidYouKnow(TalkPage page, Tokens tokens) : base(page, tokens)
		{
			Page = page;
			dyk = new Dictionary<string, string>();
		}
	}	

	public class InTheNews : Template, IArticleHistoryItem
	{
        new TalkPage Page;
        readonly Dictionary<string, string> itn;

		public static bool IsInTheNews(string name)
		{
			return name.Equals("ITN talk", StringComparison.OrdinalIgnoreCase) || name.Equals("ITNtalk", StringComparison.OrdinalIgnoreCase);
		}

		public Dictionary<string, string> KeyValuePairs()
		{
			if (0 == itn.Count)
			{
				if (Exists("1") && Exists("2"))
				{
					string timestamp = Fetch("1") + " " + Fetch("2");
					string date = ArticleHistory.FormatTimestamp(timestamp);
					itn.Add("itndate", date);
				}
				if (Exists("date"))
				{
					string timestamp = Fetch("date");
					string date = ArticleHistory.FormatTimestamp(timestamp);
					itn.Add("itndate", date);
				}
				for (int i = 1; ; ++i)
				{
					if (Exists("date" + i))
					{
						string timestamp = Fetch("date" + i);
						string date = ArticleHistory.FormatTimestamp(timestamp);
						itn.Add("itn" + i + "date", date);
						continue;
					}
					break;
				}
			}
			return itn;
		}

		public InTheNews(TalkPage page, Tokens tokens) : base(page, tokens)
		{
			Page = page;
			itn = new Dictionary<string, string>();
		}
	}

	public class OnThisDay : Template, IArticleHistoryItem
	{
        new TalkPage Page;
        readonly Dictionary<string, string> otd;

		static public bool IsOnThisDay(string name)
		{
			return name.Equals("OTD talk", StringComparison.OrdinalIgnoreCase) || name.Equals("OTDtalk", StringComparison.OrdinalIgnoreCase) ||
					name.Equals("On this day", StringComparison.OrdinalIgnoreCase) || name.Equals("Onthisday", StringComparison.OrdinalIgnoreCase);
		}

		public Dictionary<string, string> KeyValuePairs()
		{
			if (0 == otd.Count)
			{
				Page article = Page.Article;
					for (int i = 1; ; ++i)
				{
					if (Exists("date" + i))
					{
						string date = ArticleHistory.FormatTimestamp(Fetch("date" + i));
						otd.Add("otd" + i + "date", date);
						if (Exists("oldid" + i))
						{
							string oldid = Fetch("oldid" + i);
							otd.Add("otd" + i + "date", oldid + "\n");
						}
						else
						{
							int revid = ArticleHistory.GetRevId(article, date);
							otd.Add("otd" + i + "date", revid + "\n");
						}
						continue;
					}
					break;
				}
			}
			return otd;
		}

		public OnThisDay(TalkPage page, Tokens tokens) : base(page, tokens)
		{
			Page = page;
			otd = new Dictionary<string, string>();
		}
	}

	public class ArticleHistoryException : Exception
	{
		public ArticleHistoryException(string message) : base(message)
		{
		}
	}

	public class ArticleHistoryDateException : ArticleHistoryException
	{
		public ArticleHistoryDateException(string message) : base(message)
		{
		}
	}

	public class ArticleHistory : Template, IComparer<Milestone>
	{
        new TalkPage Page;

        public string CurrentStatus
		{
			get => this["currentstatus"] ?? "";
			set => this["currentstatus"] = value;
		}

        public static bool IsArticleHistory(string name)
		{
			return Regex.IsMatch(name, "Article *History", RegexOptions.IgnoreCase);
		}

		public Parameter? FindAction(string action, string value)
		{
			var last = FindLast(@"action\d+", action);
			if (null != last)
			{
				return Find(last.Name + value);
			}
			return null;
		}

        public static string FormatTimestamp(DateTime timestamp)
        {
            return timestamp.ToString("HH:mm:ss dd MMMM yyyy (UTC)");
        }

		public static string FormatTimestamp(string timestamp)
		{
			return FormatTimestamp(ParseTimestamp(timestamp));
		}

		public static DateTime ParseTimestamp(string timestamp)
		{
			bool parsed = false;

			if (DateTime.TryParse(timestamp, out DateTime result))
			{
				parsed = true;
			}
			else
			{
				string[] patterns = {
				"HH:mm, d MMMM yyyy (UTC)",
				"HH:mm:ss d MMMM yyyy (UTC)"
			};

				foreach (var pattern in patterns)
				{
					if (DateTime.TryParseExact(timestamp, pattern, null, DateTimeStyles.None, out result))
					{
						// Debug.WriteLine("Parsed '{0}' as {1}.",  timestamp, result.ToString("HH:mm:ss dd MMMM yyyy (UTC)"));
						parsed = true;
						break;
					}
				}
			}

			if (!parsed)
			{
				throw new ArticleHistoryDateException("unable to parse timestamp '" + timestamp + "'");
			}

			return result;
		}

		public static int GetRevId(Page page, DateTime dt)
		{
			// Debug.Entered ("dt=" + dt.ToString("HH:mm:ss dd MMMM yyyy (UTC)"));
			do
			{
				var revisions = page!.History(500);
				foreach (var revision in revisions)
				{
					DateTime dr = revision.Timestamp;
					// Debug.WriteLine ("dr=" + dr.ToString("HH:mm:ss dd MMMM yyyy (UTC)"));
					if (DateTime.Compare(dr, dt) <= 0)
					{
						// Debug.Exited ("revid=" + revision.RevId);
						return revision.RevId;
					}
				}
			} while (page!.Query!.Continue);
			throw new ArticleHistoryException("unable to find revision for " + page!.Title);
		}

		public static int GetRevId(Page page, string time)
		{
			return GetRevId(page, ParseTimestamp(time));
		}

		private int NextActionId()
		{
			int next = 1;
			var last = FindLast(@"action\d+");
			if (null != last)
			{
				MatchCollection matches = Regex.Matches(last.Name, @"action(\d+)");
				foreach (Match match in matches)
				{
					next = Math.Max (Int32.Parse(match.Groups[1].Value) + 1, next);
				}
			}
			return next;
		}

		public void Add(int id, IMilestone milestone)
		{
			Add("action" + id, milestone.Action + "\n");
			Add("action" + id + "date", milestone.Date + "\n");
			Add("action" + id + "link", milestone.Link + "\n");
			Add("action" + id + "result", milestone.Result + "\n");
			Add("action" + id + "oldid", milestone.RevId + "\n");
		}

		public void Add(IMilestone milestone)
		{
			if (!Exists(milestone))
			{
				Add(NextActionId(), milestone);
			}
		}

		public void Add(IArticleHistoryItem articleHistoryItem)
		{
			var keyValuePairs = articleHistoryItem.KeyValuePairs();
			foreach (var pair in keyValuePairs)
			{
				Add(pair.Key, pair.Value + "\n");
			}
		}

		public void Add(Template template)
		{
			if (template is IMilestone)
			{
				Add((template as IMilestone)!);
			}
			else if (template is IArticleHistoryItem)
			{
				Add((template as IArticleHistoryItem)!);
			}
			else if (template is GoodArticles)
			{
				Add("topic", template.Fetch("topic") + "\n");
			}
		}

		public new void Added(Page page)
		{
			Page.Templates.Add(this);
			Page.ArticleHistory = this;
		}

		public void Collapse(string value)
		{
			Add("collapse", value);
		}

        public int Compare(Milestone? x, Milestone? y)
		{
			if (x == null || y == null)
            {
				throw new Exception("Attempted comparison with null value");
            }	
			DateTime dx = ArticleHistory.ParseTimestamp(x.Date);
			DateTime dy = ArticleHistory.ParseTimestamp(y.Date);
			return DateTime.Compare(dx, dy);
		}

		public bool Exists(IMilestone action)
		{
			bool exists = false;
			var links = FindAll(@"action\d+link");
			foreach (var link in links)
			{
				if (link.Value.Equals(action.Link))
				{
					Debug.WriteLine("found duplicate action: " + action);
					exists = true;
				}
			}
			return exists;
		}

		public Milestone Get(int id)
		{
			Debug.Entered("id = " + id);
			string action = Fetch("action" + id);
			string date = Fetch("action" + id + "date");
			string link = Fetch("action" + id + "link", "");
			string result = Fetch("action" + id + "result");
			int oldid = Exists("action" + id + "oldid") && !Fetch("action" + id + "oldid").Equals("") ? int.Parse(Fetch("action" + id + "oldid")) : GetRevId(Page.Article, date);
			Milestone milestone = new Milestone(id, action, date, link, oldid, result);
			Debug.Exited("milestone = " + milestone.ToString());
			return milestone;
		}

		public List<Milestone> GetAll()
		{
			List<Milestone> milestones = new List<Milestone>();
			List<Parameter> actionIdParameters = FindAll(@"^action\d+$");
			foreach (var p in actionIdParameters)
			{
				int id = int.Parse(p.Name[6..]);
				milestones.Add(Get(id));
			}
			return milestones;
		}

        public List<Milestone> GetAll(string action)
        {
            List<Milestone> milestones = new List<Milestone>();
            List<Parameter> actionIdParameters = FindAll(@"^action\d+$", action);
            foreach (var p in actionIdParameters)
            {
                int id = int.Parse(p.Name[6..]);
                milestones.Add(Get(id));
            }
            return milestones;
        }

        public Milestone? GetLast(string action)
		{
			Debug.Entered("action = " + action);
			Parameter? actionParameter = FindLast(@"action\d+", action);
			if (null == actionParameter)
			{
				return null;
			}
			string idString = Regex.Match(actionParameter.Name, @"\d+").Value;
			int id = Int32.Parse(idString);
			Milestone milestone = Get(id);
			Debug.Exited("milestone = " + milestone.ToString());
			return milestone;
		}

		public void Merge()
		{
			var templates = Page.Templates.FindAll(ind => ind is IMilestone || ind is IArticleHistoryItem);
			foreach (var template in templates)
			{
				Add(template);
				Page.Remove(template);
			}
		}

		private void MoveToEnd(string name)
		{
			var parameter = Find(name);
			if (null != parameter)
			{
				Remove(parameter);
				Add(parameter);
			}
		}

		public void Sort()
		{
			List<Milestone> milestones = GetAll();

			milestones.Sort(Compare);

			// Covering the odd case where the originals were not consecutive
			{
				List<Parameter> actionParameters = FindAll(@"^action\d");
				foreach (var parameter in actionParameters)
				{
					Remove(parameter);
				}
			}

			{
				int id = 1;
				foreach (var milestone in milestones)
				{
					Add(id++, milestone);
				}
			}

			MoveToEnd("currentstatus");
			MoveToEnd("topic");
		}

        public void Update(string action, string result)
        {
            DateTime date = DateTime.Now;
            string link = Page.Title;
            int revid = ArticleHistory.GetRevId(Page.Article, date);
            Milestone milestone = new Milestone(action, date.ToString("HH:mm:ss dd MMMM yyyy (UTC)"), link, revid, result);
            Add(milestone);
        }

        public ArticleHistory(TalkPage page, Tokens tokens) : base(page, tokens)
		{
			Page = page;
			Page.ArticleHistory = this;
		}

		public ArticleHistory(TalkPage page) : base(page, "ArticleHistory")
		{
            Page = page;
            Page.ArticleHistory = this;
        }
    }
}
