using System;
namespace Wikimedia
{
    public class ArticlePage : Page
    {
        private TalkPage? talk;
        public TalkPage Talk
        {
            get => talk ??= new TalkPage(TitleWithoutNamespace);
        }

        public ArticlePage(string title, int revid = -1) : base(title, revid)
        {
        }

        // Used by derived classdes that are not articles esg. TemplatePage, WikipediaPage 
        public ArticlePage(Namespace ns, string title, int revid = -1) : this (ns.Append(title), revid)
        {
        }
    }
}

