using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;


namespace Wikimedia.MilHist
{
    public class Award
    {
        public string Name { get; set; }
        public string Nominee { get; set; }
        public string Nominator { get; set; }
        public string? Citation { get; set; }
        public int Diff { get; set; }
        public int OldId { get; set; }

        private string? approver;
        private AwardNominationTemplate? nomination;
        private AwardNominationsPage? nominations;
        private TrackingPage? tracking;
        private static readonly Dictionary<string, string> templateNames;

        public string Approver
        {
            get
            {
                if (null == approver)
                {
                    var history = Nominations.History();
                    foreach (var revision in history)
                    {
                        var page = new AwardNominationsPage(revision.RevId);
                        page.Load();
                        var nominations = page.Templates.FindAll(x => x is AwardNominationTemplate);
                        AwardNominationTemplate? nom = nominations.Find(x => ((AwardNominationTemplate)x).Matches(Nomination)) as AwardNominationTemplate;
                        if (null == nom)
                        {
                            throw new Exception("Could not find award nomination template");
                        }
                        else if (null == nom.Status)
                        {
                            throw new Exception("Could not find Status parameter in award nomination template");
                        }
                        else if (nom.Status.Equals("approved"))
                        {
                            approver = revision.User;
                        }
                        else if (nom.Status.Equals("nominated"))
                        {
                            break;
                        }
                    }
                }
                if (null == approver)
                {
                    throw new Exception("Could not find award nomination approver");
                }
                return approver;
            }
            set => approver = value;
        }

        public int Maximum
        {
            get => Name.Matches("A-Class cross") ? 5 : 3;
        }

        public AwardNominationTemplate Nomination
        {
            get
            {
                if (null == nomination)
                {
                    nomination = new AwardNominationTemplate(Nominations, this);
                }
                return nomination;
            }
            set => nomination = value;
        }

        public AwardNominationsPage Nominations
        {
            get
            {
                if (null == nominations)
                {
                    nominations = new AwardNominationsPage();
                }
                return nominations;
            }
        }

        public TrackingPage TrackingPage
        {
            get
            {
                if (null == tracking)
                {
                    tracking = new TrackingPage(this);
                }
                return tracking;
            }
            set => tracking = value;
        }

        private static string recommendation(string nominee)
        {
            string name;

            var count = AwardNominationsPage.Count(nominee);

            if (count < 35)
            {
                if (count < 5)
                {
                    name = "A-Class medal";
                }
                else if (count < 10)
                {
                    name = "A-Class medal with Oak Leaves";
                }
                else if (count < 20)
                {
                    name = "A-Class medal with Swords";
                }
                else
                {
                    name = "A-Class medal with Diamonds";
                }

            }
            else
            {
                if (count < 40)
                {
                    name = "A-Class cross";
                }
                else if (count < 46)
                {
                    name = "A-Class cross with Oak Leaves";
                }
                else if (count < 56)
                {
                    name = "A-Class cross with Swords";
                }
                else
                {
                    name = "A-Class cross with Diamonds";
                }
            }
            return name;
        }

        public void Nominate()
        {
            Nominations.Add(Nomination);
        }

        public void Congratulate()
        {
            Page page = new Page("User talk:" + Nominee);
            string subject = "Congratulations from the Military History Project";

            UserTemplate approverTemplate = new UserTemplate(page, "user0");
            var awardTemplateName = templateNames[Name];
            Template awardTemplate = new Template("subst:" + awardTemplateName);
            awardTemplate.Add(1, "On behalf of the Military History Project, I am proud to present the " + Name + " for " + Citation + " " + approverTemplate + " via ~~~~");

            Section section = new Section(page, subject, 2);
            section.Add("\n");
            section.Add(awardTemplate);
            section.Add("\n");
            page.Save(section, subject);

            Nomination.Status = "awarded";
            Nomination.Diff = page.RevId.ToString();
            Nomination.OldId = page.OldRevId.ToString();
            Nomination.Save("Awarded " + Name + " to " + Nominee);

            Diff = page.RevId;
            OldId = page.OldRevId;
        }
 
        static Award()
        {
            templateNames = new Dictionary<string, string>()
            {
                {"A-Class medal",                           "WPMILHIST A-Class medal"                 },
                {"A-Class medal with Oak Leaves",           "WPMILHIST A-Class medal (Oakleaves)"     },
                {"A-Class medal with Swords",               "WPMILHIST A-Class medal (Swords)"        },
                {"A-Class medal with Diamonds",             "WPMILHIST A-Class medal (Diamonds)"      },

                {"A-Class cross",                           "WPMILHIST A-Class cross"                 },
                {"A-Class cross with Oak Leaves",           "WPMILHIST A-Class cross with Oak Leaves" },
                {"A-Class cross with Swords",               "WPMILHIST A-Class cross with Swords"     },
                {"A-Class cross with Diamonds",             "WPMILHIST A-Class cross with Diamonds"   },

                {"WikiChevrons",                            "WPMILHIST WikiChevrons"                  },
                {"WikiChevrons with Oak Leaves" ,           "WPMILHIST WikiChevrons with Oak Leaves"  },
                {"The Milhist reviewing award (1 stripe)",  "WPMILHIST Service (review) 1 stripe"     },
                {"The Milhist reviewing award (2 stripes)", "WPMILHIST Service (review) 2 stripes"    },
                {"The Milhist reviewing award (3 stripes)", "WPMILHIST Service (review) 3 stripes"    },
                {"The Content Review Medal of Merit (Military history)", "WPMILHIST Service (review) Content Review Medal of Merit" },
            };
        }

        public Award(string name, string nominee, string nominator)
        {
            Name = name;
            Nominee = nominee;
            Nominator = nominator;
        }

        public Award(string nominee, string nominator) : this(recommendation(nominee), nominee, nominator)
        {
        }

        public Award(string nominator, AwardNominationTemplate nomination) : this(nomination.Award, nomination.Nominee, nominator)
        {
            this.nomination = nomination;
            Citation = nomination.Citation;
        }
    }

    public class AwardNominationTemplate: Template
    {
        private new const string Name = "WPMILHIST Award nomination";

        public string Award { get => this["award"] ?? ""; set => this["award"] = value; }
        public string Nominee { get => this["nominee"] ?? ""; set => this["nominee"] = value; }
        public string Citation { get => this["citation"] ?? ""; set => this["citation"] = value; }
        public string Status { get => this["status"] ?? ""; set => this["status"] = value; }
        public string Diff { get => this["diff"] ?? ""; set => this["diff"] = value; }
        public string OldId { get => this["oldid"] ?? ""; set => this["oldid"] = value; }
        public string? Nominator { get; set; }

        public static bool IsNomination(string name)
        {
            return Name.Equals(name);
        }

        public bool Matches(AwardNominationTemplate nomination)
        {
            return nomination.Award.Equals(Award) && nomination.Nominee.Equals(Nominee) && nomination.Citation.Equals(Citation);
        }

        public void Save(string summary)
        {
            Page.Save(summary);
        }

        public AwardNominationTemplate(Page page, Award award) : base(page, Name)
        {
            Award = award.Name;
            Nominee = award.Nominee;
            Citation = award.Citation ?? "";
            Nominator = award.Nominator;
            Status = "nominated";
        }

        public AwardNominationTemplate(Page page, Tokens tokens) : base(page, tokens)
        {
        }
    }

    // Deals with the nominations page
    public class AwardNominationsPage : WikipediaPage
    {
        private const string acmSectionName = "Nominations for the A-Class Medal";
        private const string accSectionName = "Nominations for the A-Class Cross";
        private const string nominationTalkPageTitle = MilHist.ProjectTalkPath + "/Awards";

        public static int Count(string nominee)
        {
            List<string> archives = new List<string>() {
                nominationTalkPageTitle,
                nominationTalkPageTitle + "/ACC/Archive 1",
                nominationTalkPageTitle + "/ACR/Archive 2",
                nominationTalkPageTitle + "/ACR/Archive 1",
            };

            var count = 0;
            foreach (var archive in archives)
            {
                var page = new WikipediaTalkPage(archive);
                page.Load();
                var section = page.FindSection(nominee + @" \((\d+)\)");
                if (null != section)
                {
                    Regex regex = new Regex(@" \((\d+)\)", RegexOptions.IgnoreCase);
                    Match match = regex.Match(section.Title);
                    Group group = match.Groups[1];
                    count = Int32.Parse(group.ToString());
                    break;
                }
            }
            return count;
        }

        public void Add(AwardNominationTemplate nomination)
        {
            var section = new Section(this, SectionName(nomination.Award));
            var count = Count(nomination.Nominee) + 1;
            string subject = nomination.Nominee + " (" + count + ")";
            string text = "\n" + nomination.Text + "\n" + "*'''Support''' As nominator " + nomination.Nominator + " via ~~~~" + "\n";

            Section subsection = new Section(this, subject, 4);
            subsection.Add("\n");
            subsection.Add(text);
            subsection.Add("\n");
            section.Add(subsection);
            section.Save(subject);
        }

        public void Add(Award award)
        {
            var nomination = new AwardNominationTemplate(this, award);
            Add(nomination);
        }

        public IEnumerable<AwardNominationTemplate> Approved()
        {
            foreach (var template in Templates)
            {
                if (template is AwardNominationTemplate)
                {
                    var nomination = template as AwardNominationTemplate;
                    if (nomination != null && nomination.Status != null)
                    {
                        if (nomination.Status.Equals("approved"))
                        {
                            yield return nomination;
                        }
                    }
                }
            }
        }

        public static string SectionName(String award)
        {
            string nominationSectionName;
            if (award.Matches("A-Class medal"))
            {
                nominationSectionName = acmSectionName;
            }
            else if (award.Matches("A-Class cross"))
            {
                nominationSectionName = accSectionName;
            }
            else
            {
                throw new Exception("unknown award: " + award);
            }
            return nominationSectionName;
        }

        public AwardNominationsPage(int revid = -1) : base(TrackingPage.nominationPageTitle, revid)
        {
            Load();
        }
    }

    // Deals with the tracking page
    public class TrackingPage : WikipediaPage
    {
        public const string nominationPageTitle = MilHist.ProjectPath +  "/Awards";
        private const string acmTracking = nominationPageTitle + "/ACM/Eligibility tracking";
        private const string accTracking = nominationPageTitle + "/ACC/Eligibility tracking";

        private readonly Award award;
        private List<Link>? tally;

        public ExtendedImage Icon(string entry)
        {
            Debug.Entered("entry=" + entry);
            var icon = new ExtendedImage("Symbol a class.svg");
            icon.Size = "15px";
            icon.ImageLink = "link=" + entry;
            Debug.Exited("image=" + icon);
            return icon;
        }

        public void Increment(string nominee, string entry)
        {
            Debug.Entered("nominee=" + nominee);
            var icon = Icon(entry);
            var line = GetTallyLine(nominee);
            // Append after second last token - last will be the newline
            Append(line[^2], new Token(Tally.Count > 0 ? ", " : " "), icon);
            Debug.Exited();
        }

        public IEnumerable<string> Entries()
        {
            foreach (var link in Tally)
            {
                yield return link.Value.Substring(10);
            }
        }

        public string Citation ()
        {
            var collection = Entries();
            var temp = new List<string>();
            for (int i = 0; i < collection.Count() - 1; i++)
            {
                temp.Add(collection.ElementAt(i));
                temp.Add(", ");
            }
            temp.Add(" and ");
            temp.Add(collection.ElementAt(collection.Count() - 1));
            var citation = String.Join("", collection);
            return citation;
        }

        public void Erase()
        {
            foreach (var link in Tally)
            {
                var index = FindIndex(link);
                RemoveRange(index, 1);
            }
        }

        public bool IsComplete()
        {
            return Tally.Count + 1 >= award.Maximum;
        }

        public List<IToken> GetTallyLine (string nominee)
        {
            IToken? getTally = FindText(nominee);
            if (null == getTally)
            {
                var text = FindAllText("# ");
                var t = text.Find(ind => String.Compare(ind.Text.Substring(5), nominee, StringComparison.OrdinalIgnoreCase) > 0);
                IToken insertionPoint = EndOfLine(null == t ? text.Last() : t);
                Insert(insertionPoint, "\n", "# '''" + nominee + ":'''");
                getTally = FindText(nominee);
            }
            if (null == getTally)
            {
                throw new Exception("unable to find award nominee '" + award.Nominee + "' on page '" + Title + "'");
            }
            var line = GetLine(getTally);
            return line;
         }

        public List<Link> Tally
        {
            get
            {
                if (null == tally)
                {
                    tally = new List<Link>();
                    var line = GetTallyLine(award.Nominee);
                    foreach (var token in line)
                    {
                        if (token is Link)
                        {
                            tally.Add((Link)token);
                        }
                    }
                }
                return tally;
            }
            set
            {
                tally = value;
            }
        }

        public static string TrackingPageTitle(string name)
        {
            string trackingPageTitle;
            if (name.Matches("A-Class medal"))
            {
                trackingPageTitle = acmTracking;
            }
            else if (name.Matches("A-Class cross"))
            {
                trackingPageTitle = accTracking;
            }
            else
            {
                throw new Exception("unknown page: " + name);
            }
            return trackingPageTitle;
        }

        public TrackingPage(Award award) : base(TrackingPageTitle(award.Name))
        {
            this.award = award;
            Load();
        }
    }

    public class HistoricalPage: WikipediaPage
    {
        private static Dictionary<string, string> awardPageTitles;

        private Tokens entry(Award award)
        {
            Tokens tokens = new Tokens();

            Link user = new Link(new Namespace("User"), award.Nominee, award.Nominee);

            Template diff = new Template("diff");
            diff.Add("page", "User talk:" + award.Nominee);
            diff.Add("diff", award.Diff.ToString());
            diff.Add("oldid", award.OldId.ToString());
            DateTime currentDate = DateTime.Now;
            diff.Add("label", currentDate.ToString("MMMM YYYY"));

            tokens.Add("* ");
            tokens.Add(user);
            tokens.Add(": for ");
            if (null == award.Citation)
            {
                throw new Exception("Null award citation");
            }
            tokens.Add(award.Citation);
            tokens.Add(" (Awarded ");
            tokens.Add(diff);
            tokens.Add(")\n");

            return tokens;
        }

        public void Add(Award award)
        {
            if (award.Name.Equals("WikiChevrons with Oak Leaves"))
            {
                var link = Links.Find(x => x.Namespace.Equals("User") && String.Compare(x.Data, award.Nominee) > 0);
                if (link == null)
                {
                    Add(Links[^1], entry(award));
                }
                else
                {
                    Insert(StartOfLine(link as IToken), entry(award));
                }
            }
            else
            {
                Section? section = Sections.Find(ind => ind.Title.Equals(award.Name));
                if (null == section)
                {
                    section = new Section(this, award.Name, 4);
                    section.Add("The ''" + award.Name + "'' was introduced in May 2014 and has been awarded to the following editors:\n");
                    section.Add(entry(award));
                }
                else
                {
                    int first = FindIndex(section);
                    int index = Sections.IndexOf(section);
                    int last = (index < Sections.Count - 1) ? FindIndex(Sections[index + 1]) : FindLastIndex();
                    var links = Links.FindAll(x => x.Namespace.Equals("User") && FindIndex(x) > first && FindIndex(x) <= last);
                    var link = links.Find(x => String.Compare(x.Data, award.Nominee) > 0);
                    if (link == null)
                    {
                         Add(links[^1], entry(award));
                    }
                    else
                    {
                        Insert(StartOfLine(link as IToken), entry(award));
                    }
                }
            }
        }

        static HistoricalPage()
        {
            awardPageTitles = new Dictionary<string, string>()
            {
                {"WikiChevrons with Oak Leaves",  TrackingPage.nominationPageTitle + "/OAK" },

                {"A-Class medal",                 TrackingPage.nominationPageTitle + "/ACM" },
                {"A-Class medal with Oak Leaves", TrackingPage.nominationPageTitle + "/ACM" },
                {"A-Class medal with Swords",     TrackingPage.nominationPageTitle + "/ACM" },
                {"A-Class medal with Diamonds",   TrackingPage.nominationPageTitle + "/ACM" },

                {"A-Class cross",                 TrackingPage.nominationPageTitle + "/ACC" },
                {"A-Class cross with Oak Leaves", TrackingPage.nominationPageTitle + "/ACC" },
                {"A-Class cross with Swords",     TrackingPage.nominationPageTitle + "/ACC" },
                {"A-Class cross with Diamonds",   TrackingPage.nominationPageTitle + "/ACC" },

            };
        }

        public HistoricalPage(Award award) : base(awardPageTitles[award.Name])
        {
        }
    }
}
