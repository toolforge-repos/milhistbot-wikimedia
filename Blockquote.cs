using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Blockquote : IToken, ITreeElement
    {
        public Page Page { get; set; }
        public TokenType Type { get; set; }
        public string Text { get; set; }
        public Tokens Contents { get; set; }

        public Tokens TreeContents()
        {
            return Contents;
        }

        public Blockquote(Page page, Tokens tokens)
        {
            Page = page;
            Contents = new Tokens();
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                switch (token.Type)
                {
                    case TokenType.OpenComment:
                        Contents.Add(new Comment(page, tokens));
                        break;
                    case TokenType.OpenNowiki:
                        Contents.Add(new Nowiki(page, tokens));
                        break;
                    case TokenType.OpenBracket:
                        Contents.Add(new Link(page, tokens));
                        break;
                    case TokenType.OpenExternalLink:
                        Contents.Add(new ExternalLink(page, tokens));
                        break;
                    case TokenType.OpenTemplate:
                        Contents.Add(new Template(page, tokens));
                        break;
                    case TokenType.OpenReference:
                        Contents.Add(new Reference(page, token));
                        break;
                    case TokenType.CloseBlockquote:
                        Text = "<blockquote>" + Contents.Text + token;
                        Type = TokenType.Blockquote;
                        return;
                    default:
                        Contents.Add(token);
                        break;
                }
            }
            tokens.InsertRange(0, Contents);
            throw new ParseException("Unclosed <blockquote> on " + page.Title + ": " + Contents.Preview());
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
