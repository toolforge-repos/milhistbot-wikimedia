using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Wikimedia
{
    public class PageMissingException : BotException
    {
        public PageMissingException(string message) : base(message)
        {
        }
    }

    public class BotException : Exception
    {
        public BotException(string message) : base(message)
        {
        }

        public BotException(Cred cred, string message) : base(message)
        {
            cred.Warning(message);
            cred.Close();
        }
    }

    public class Bot
    {
        private Cred Cred { get => Cred.Instance; }
        private const string address = "https://en.wikipedia.org";
        private const string api = "/w/api.php";
        private string? editToken;
        private string EditToken { get => (editToken ??= getEditToken()); }
        private static HttpClient HttpClient;
        private static readonly Lazy<Bot> _instance = new Lazy<Bot>(() => new Bot());
        public static Bot Instance => _instance.Value;

        public static bool AllowBots(string text, string job)
        {
            return !Regex.IsMatch(text, @"\{\{(nobots|bots\|(allow=none|deny=(?!none).*(" + job.Normalize() + @".*|all)|optout=all))\}\}", RegexOptions.IgnoreCase);
        }

        private string url(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)!
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            string url = address + api + "?" + string.Join("&", array);
            //      Console.WriteLine(url);
            return url;
        }

        private void JasonContinue(JObject json, string key, Query query)
        {
            if (json.ContainsKey("continue"))
            {
                query.Start = JasonQuery(json, "continue", key);
                query.Continue = true;
            }
            else
            {
                query.Continue = false;
            }
            // Console.WriteLine ("continue=" + query.Continue);
        }

        private void JasonError(JObject json)
        {

            if (json.ContainsKey("error"))
            {
                string code = JasonQuery(json, "error", "code");
                string info = JasonQuery(json, "error", "info");
                throw new BotException(Cred, Debug.Caller() + " failed: (" + code + "): " + info);
            }
        }

        private string JasonQuery(JObject json, string jquery, params string[] jstrings)
        {
            JToken? jtoken;
            jtoken = json[jquery];
            if (jtoken == null)
            {
                throw new BotException(jquery + " should not be null");
            }

            foreach (string jstring in jstrings)
            {
                jtoken = jtoken[jstring];
                if (jtoken == null)
                {
                    throw new BotException(jstring + " should not be null");
                }
            }
            return (string)jtoken!;
        }

        private async Task<string> GetResponseAsync(string url)
        {
            // Console.WriteLine(url);
            string responseString;
            using (HttpResponseMessage response = await HttpClient.GetAsync(url))
            {
                response.EnsureSuccessStatusCode(); // Throw if httpcode is an error
                using (HttpContent content = response.Content)
                {
                    responseString = await content.ReadAsStringAsync();
                    // Console.OutputEncoding = System.Text.Encoding.UTF8;
                    // Console.WriteLine(responseString);
                }
            }
            return responseString;
        }

        private async Task<string> PostResponseAsync(string url, HttpContent httpContent)
        {
            string responseString;
            using (HttpResponseMessage response = await HttpClient.PostAsync(url, httpContent))
            {
                response.EnsureSuccessStatusCode();
                using (HttpContent content = response.Content)
                {
                    responseString = await content.ReadAsStringAsync();
                    // Console.OutputEncoding = System.Text.Encoding.UTF8;
                    // Console.WriteLine(responseString);
                }
            }
            return responseString;
        }

        private string getEditToken()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["meta"] = "tokens";
            string editTokenUrl = url(parameters);

            var values = new Dictionary<string, string>();
            var editTokenContent = new FormUrlEncodedContent(values);

            string responseString = GetResponseAsync(editTokenUrl).Result;
            JObject json = JObject.Parse(responseString);
            editToken = JasonQuery(json, "query", "tokens", "csrftoken");
            // Console.WriteLine(editToken);

            return editToken;
        }

        public List<Page> Category(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["list"] = "categorymembers";
            if (query.Title == null)
            {
                throw new BotException("Query.Title should not be null");
            }
            parameters["cmtitle"] = "category:" + query.Title.Replace(' ', '_');
            parameters["cmlimit"] = query.Limit;
            if (null != query.Start)
            {
                parameters["cmcontinue"] = query.Start;
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string getCategoryUrl = url(parameters);

            string responseString = GetResponseAsync(getCategoryUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);
            JasonContinue(json, "cmcontinue", query);

            List<Page> pages = new List<Page>();
            var members = json["query"]!["categorymembers"]!;
            foreach (var member in members)
            {
                string title = (string)member["title"]!;
                Page page = PageFactory.GetPage(title);
                page.PageId = (int)member["pageid"]!;
                page.Ns = (int)member["ns"]!;
                pages.Add(page);
            }
            return pages;
        }

        public List<String> Categories(string title)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "categories";
            parameters["titles"] = title;
            parameters["cllimit"] = "500";
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string getCategoriesUrl = url(parameters);
            // Console.WriteLine(getCategoriesUrl);

            string responseString = GetResponseAsync(getCategoriesUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            List<String> categories = new List<string>();
            var members = json["query"]!["pages"]![0]!["categories"];
            if (null != members)
            {
                foreach (var member in members)
                {
                    var category = (string)member["title"]!;
                    categories.Add(category);
                }
            }
            return categories;
        }

        public List<Contribution> Contributions(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["list"] = "usercontribs";
            parameters["ucuser"] = query.User;
            parameters["uclimit"] = query.Limit;
            if (null != query.Start)
            {
                parameters["ucstart"] = query.Start.Substring(0, 14);
            }
            if (null != query.End)
            {
                parameters["ucend"] = query.End;
            }

            string userContributionsUrl = url(parameters);
            //      Console.WriteLine(userContributionsUrl);

            string responseString = GetResponseAsync(userContributionsUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "uccontinue", query);

            List<Contribution> userContributions = new List<Contribution>();
            var contributions = json["query"]!["usercontribs"]!;
            foreach (var contribution in contributions)
            {
                var user = (string)contribution["user"]!;
                var pageId = (int)contribution["pageid"]!;
                var revId = (int)contribution["revid"]!;
                var parentId = (int)contribution["parentid"]!;
                var namespaceId = (int)contribution["ns"]!;
                var timestamp = (DateTime)contribution["timestamp"]!;
                var comment = (string)contribution["comment"]!;
                var size = (int)contribution["size"]!;
                Contribution c = new(this, user, pageId, revId, parentId, namespaceId, timestamp, comment, size);
                userContributions.Add(c);
            }

            return userContributions;
        }

        public List<Page> Embedded(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["list"] = "embeddedin";
            parameters["eititle"] = query.Title;
            if (query.Continue)
            {
                parameters["eicontinue"] = query.Start;
            }
            string embeddedUrl = url(parameters);
            //      Console.WriteLine(embeddedUrl);

            string responseString = GetResponseAsync(embeddedUrl).Result;
            //      Console.WriteLine(responseString);

            List<Page> pages = new List<Page>();
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "eicontinue", query);

            var backlinks = json["query"]!["embeddedin"]!;
            foreach (var backlink in backlinks)
            {
                string title = (string)backlink["title"]!;
                Page page = PageFactory.GetPage(title);
                page.PageId = (int)backlink["pageid"]!;
                page.Ns = (int)backlink["ns"]!;
                pages.Add(page);
            }
            return pages;
        }

        public List<Revision> History(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "revisions";
            parameters["titles"] = query.Title;
            parameters["rvprop"] = "ids|timestamp|user|comment";
            parameters["rvslots"] = "main";
            parameters["rvlimit"] = query.Limit.ToString();
            if (null != query.Start)
            {
                parameters["rvstart"] = query.Start.Substring(0, 14);
            }
            if (null != query.End)
            {
                parameters["rvend"] = query.End;
            }
            if (null != query.Direction)
            {
                parameters["rvdir"] = query.Direction;
            }
            if (null != query.User)
            {
                parameters["rvuser"] = query.User;
            }

            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string historyUrl = url(parameters);
            //      Console.WriteLine (historyUrl);

            string responseString = GetResponseAsync(historyUrl).Result;
            //      Console.WriteLine(responseString);
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            JasonContinue(json, "rvcontinue", query);

            List<Revision> history = new List<Revision>();
            if (null != json["query"]!["pages"]![0]!["missing"])
            {
                throw new PageMissingException("History failed: page '" + query.Title + "' missing");
            }

            var revisions = json["query"]!["pages"]![0]!["revisions"]!;
            foreach (var revision in revisions)
            {
                Revision r = new Revision();
                r.Title = query.Title!;
                r.RevId = (int)revision["revid"]!;
                r.ParentId = (int)revision["parentid"]!;
                r.User = (string)revision["user"]!;
                r.Timestamp = (DateTime)revision["timestamp"]!;
                r.Comment = (string)revision["comment"]!;
                history.Add(r);
            }
            return history;
        }

        public void Load(Page page, int revid)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "revisions";
            parameters["rvprop"] = "content";
            parameters["rvslots"] = "main";
            if (revid > 0)
            {
                parameters["rvstartid"] = revid.ToString();
                parameters["rvendid"] = revid.ToString();
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            parameters["titles"] = page.Title.Replace(" ", "_");
            string queryUrl = url(parameters);
            // Console.WriteLine(query_url);

            string responseString = GetResponseAsync(queryUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            var jpage = json["query"]!["pages"]![0]!;
            if (null != jpage["invalidreason"])
            {
                throw new PageMissingException("Load (" + page.Title + ") failed: " + (string)jpage["invalidreason"]!);
            }

            if (null != jpage["missing"])
            {
                throw new PageMissingException("Load (" + page.Title + ") failed: page missing");
            }

            page.PageId = (int)jpage["pageid"]!;
            page.Ns = (int)jpage["ns"]!;
            page.Content = (string)jpage["revisions"]![0]!["slots"]!["main"]!["content"]!;
            page.ReadOnly = !AllowBots(page.Content, Cred.Job);
        }

        private string loginToken()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["meta"] = "tokens";
            parameters["type"] = "login";
            string loginTokenUrl = url(parameters);

            string responseString = GetResponseAsync(loginTokenUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            string loginToken = JasonQuery(json, "query", "tokens", "logintoken");
            // Console.WriteLine("logintoken=" + loginToken);
            return loginToken;
        }

        private void login(string username, string password, string lgtoken)
        {
            var parameters = new NameValueCollection();
            parameters["action"] = "login";
            parameters["format"] = "json";
            parameters["lgname"] = username;
            string loginUrl = url(parameters);
            // Console.WriteLine("query=" + loginUrl);

            var values = new Dictionary<string, string>();
            values["lgpassword"] = password;
            values["lgtoken"] = lgtoken;
            var loginContent = new FormUrlEncodedContent(values);

            string responseString = PostResponseAsync(loginUrl, loginContent).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            string result = JasonQuery(json, "login", "result");
            // Console.WriteLine("result=" + result);

            if (!"Success".Equals(result))
            {
                throw new BotException(Cred, "Login failed: " + responseString);
            }
        }

        private void login(string name, string password)
        {
            string lgtoken = loginToken();
            login(name, password, lgtoken);
        }

        private string csrfToken()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["meta"] = "tokens";
            string csrfTokenUrl = url(parameters);
            // Console.WriteLine("query=" + csrfTokenUrl);

            string responseString = GetResponseAsync(csrfTokenUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            string csrfToken = JasonQuery(json, "query", "tokens", "csrftoken");
            // Console.WriteLine("csrftoken=" + csrfToken);
            return csrfToken;
        }

        public void Move(string from, string to, string reason)
        {
            var parameters = new NameValueCollection();
            parameters["action"] = "move";
            parameters["format"] = "json";
            parameters["from"] = from;
            parameters["to"] = to;
            parameters["reason"] = reason;
            parameters["movetalk"] = "1";
            parameters["noredirect"] = "1";
            string moveUrl = url(parameters);
            // Console.WriteLine("query=" + moveUrl);

            var values = new Dictionary<string, string>();
            values["token"] = csrfToken();
            var csrfContent = new FormUrlEncodedContent(values);

            string responseString = PostResponseAsync(moveUrl, csrfContent).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);
        }

        public string Prediction(string title, int revid)
        {
            string liftwingUrl = "https://api.wikimedia.org/service/lw/inference/v1/models/enwiki-articlequality:predict";
            // Console.WriteLine("query=" + liftwingUrl);

            var jsonContent = JsonContent.Create(new { rev_id = revid });
            string responseString = PostResponseAsync(liftwingUrl, jsonContent).Result;
            JObject json = JObject.Parse(responseString);
            string prediction = JasonQuery(json, "enwiki", "scores", revid.ToString(), "articlequality", "score", "prediction");
            return prediction;
        }

        public string Prediction(string title)
        {
            Query query = new Query(title, 1);
            var history = History(query);
            int revid = history[0].RevId;

            int count = 0;
            do
            {
                try
                {
                     return Prediction (title, revid);
                }
                catch (Exception)
                {
                    if (count > 5)
                    {
                        throw;
                    }
                    ++count;
                    System.Threading.Thread.Sleep(count * 1000);
                }
            } while (true);
        }

        public List<Revision> RecentChanges(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["list"] = "recentchanges";
            parameters["action"] = "query";
            parameters["rcprop"] = "title|ids|timestamp|user|comment";
            parameters["rclimit"] = query.Limit.ToString();
            if (!query.Namespace.Text.Equals(""))
            {
                parameters["rcnamespace"] = query.Namespace.Text;
            }
            if (null != query.User)
            {
                parameters["rcuser"] = query.User;
            }
            if (null != query.Start)
            {
                parameters["rcstart"] = query.Start.Substring(0, 14);
            }
            if (null != query.End)
            {
                parameters["rcend"] = query.End;
            }
            if (query.Continue)
            {
                parameters["rccontinue"] = query.Start;
            }
            parameters["format"] = "json";
            parameters["formatversion"] = "2";
            string recentChangesUrl = url(parameters);
            // Console.WriteLine (get_recent_changes_url);

            string responseString = GetResponseAsync(recentChangesUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);
            JasonContinue(json, "rccontinue", query);

            List<Revision> recentChanges = new List<Revision>();
            var changes = json["query"]!["recentchanges"]!;
            foreach (var change in changes)
            {
                Revision r = new Revision();
                r.Title = (string)change["title"]!;
                r.RevId = (int)change["revid"]!;
                r.User = (string)change["user"]!;
                r.Timestamp = (DateTime)change["timestamp"]!;
                r.Comment = (string)change["comment"]!;
                recentChanges.Add(r);
            }

            return recentChanges;
        }

        public List<Page> Redirects(Query query)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "redirects";
            parameters["format"] = "json";
            parameters["titles"] = query.Title;
            parameters["rdlimit"] = query.Limit;
            if (query.Continue)
            {
                parameters["rdcontinue"] = query.Start;
            }
            string redirectsUrl = url(parameters);
            // Console.WriteLine(redirectsUrl);

            string responseString = GetResponseAsync(redirectsUrl).Result;

            List<Page> pages = new List<Page>();
            JObject json = JObject.Parse(responseString);
            JasonError(json);
            JasonContinue(json, "rdcontinue", query);

            var t = json["query"]!["pages"]!;
            foreach (JProperty jproperty in t)
            {
                string pageId = jproperty.Name;
                var redirects = json["query"]!["pages"]![pageId]!["redirects"];
                if (null != redirects)
                {
                    foreach (var redirect in redirects)
                    {
                        string title = (string)redirect["title"]!;
                        Page page = PageFactory.GetPage(title);
                        page.PageId = (int)redirect["pageid"]!;
                        page.Ns = (int)redirect["ns"]!;
                        pages.Add(page);
                    }
                }
            }
            return pages;
        }

        public Page? RedirectsTo(string title)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["prop"] = "redirects";
            parameters["format"] = "json";
            parameters["titles"] = title;
            parameters["redirects"] = "";
            string redirectsToUrl = url(parameters);
            // Console.WriteLine(redirectsToUrl);

            string responseString = GetResponseAsync(redirectsToUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);

            Page? redirectsTo = null;
            var query = json["query"];
            if (null == query)
            {
                throw new BotException("Query must not be null");
            }
            var redirects = query["redirects"];
            if (null != redirects)
            {
                foreach (var redirect in redirects)
                {
                    if (null != redirect)
                    {
                        var to = redirect["to"];
                        if (null != to)
                        {
                            redirectsTo = PageFactory.GetPage((string)to!);
                        }
                    }
                }
            }

            return redirectsTo;
        }

        public void Save(Page page, string summary)
        {
            if (page.ReadOnly)
            {
                throw new BotException(Cred, "Page does not allow bot update");
            }

            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "edit";
            parameters["title"] = page.Title;
            parameters["format"] = "json";
            parameters["bot"] = "true";
            parameters["summary"] = summary;
            string saveUrl = url(parameters);

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(EditToken), "token");
            content.Add(new StringContent(page.Content), "text");

            string responseString = PostResponseAsync(saveUrl, content).Result;
            // Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);

            var jedit = json["edit"];
            string result = JasonQuery(json, "edit", "result");
            // Console.WriteLine(result);

            if (!"Success".Equals(result))
            {
                throw new BotException(Cred, "edit failed: " + responseString);
            }

            if (null != jedit!["oldrevid"])
            {
                var oldrevid = jedit!["oldrevid"];
                page.OldRevId = (int)oldrevid!;
            }
            if (null != jedit!["newrevid"])
            {
                var newrevid = jedit!["newrevid"];
                page.RevId = (int)newrevid!;
            }
        }

        public void Save(Section section, string summary)
        {
            if (section.Page.ReadOnly)
            {
                throw new BotException(Cred, "Page does not allow bot update");
            }

            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "edit";
            parameters["title"] = section.Page.Title;
            parameters["section"] = section.Number;
            parameters["sectiontitle"] = section.Title;
            parameters["format"] = "json";
            parameters["bot"] = "true";
            parameters["summary"] = summary;
            string editUrl = url(parameters);

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(EditToken), "token");
            content.Add(new StringContent(section.Contents.Text), "text");

            string responseString = PostResponseAsync(editUrl, content).Result;
            // Console.WriteLine(responseString);

            JObject json = JObject.Parse(responseString);
            JasonError(json);
            string result = JasonQuery(json, "edit", "result");
            //      Console.WriteLine(result);

            if (!"Success".Equals(result))
            {
                throw new BotException(Cred, "edit failed: " + responseString);
            }
        }

        public string Title(int pageId)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["action"] = "query";
            parameters["format"] = "json";
            parameters["bot"] = "true";
            parameters["pageids"] = pageId.ToString();
            string titleQueryUrl = url(parameters);
            string responseString = GetResponseAsync(titleQueryUrl).Result;
            JObject json = JObject.Parse(responseString);
            JasonError(json);
            string title = JasonQuery(json, "query", "pages", pageId.ToString(), "title");
            return title;
        }

        public int Views(string title, DateTime date)
        {
            string date_string = date.ToString("yyyyMMdd00");
            string pageViewsUrl = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/" +
                    title.Replace(" ", "_") + "/daily/" + date_string + "/" + date_string;

            string responseString = GetResponseAsync(pageViewsUrl).Result;
            JObject json = JObject.Parse(responseString);
            var items = json["items"];
            if (items == null)
            {
                throw new BotException("items should not be null");
            }

            var item = items[0];
            if (item == null)
            {
                throw new BotException("items[0] should not be null");
            }

            var views = item["views"];
            if (views == null)
            {
                throw new BotException("views should not be null");
            }

            return (int)views;
        }

        public void Close()
        {
            editToken = null;
            Cred.Close();
        }

        static Bot()
        {
            HttpClient = new HttpClient();
        }

        private Bot()
        {
            try
            {
                login(Cred.User, Cred.Password);
            }
            catch (CredException cex)
            {
                throw new BotException(cex.Message);
            }
            catch (HttpRequestException hex)
            {
                throw new BotException(Cred, hex.Message);
            }
        }

    }
}
