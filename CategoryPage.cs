﻿using System;
namespace Wikimedia
{
    public class CategoryPage : ArticlePage
    {
        private CategoryTalkPage? talk;
        public new CategoryTalkPage Talk
        {
            get => talk ??= new CategoryTalkPage(TitleWithoutNamespace);
        }

        public CategoryPage(string title) : base(new Namespace("Category talk"), title)
        {
        }
    }

    public class CategoryTalkPage : TalkPage
    {
        private CategoryPage? article;
        public new CategoryPage Article
        {
            get => article ??= new CategoryPage(TitleWithoutNamespace);
        }

        public CategoryTalkPage(string title) : base(new Namespace("Category"), title)
        {
        }
    }
}