using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Comment : IToken
    { 
        public TokenType Type { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
       
        private Tokens contents;

        private void Parse(Page page, Tokens tokens)
        {
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                switch (token.Type)
                {
                    case TokenType.CloseComment:
                         return;
                    default:
                        contents.Add(token);
                        break;
                }
            }
            // Unclosed comments continue to the end of the page
        }

       public Comment(Page page, Tokens tokens)
       {
            contents = new Tokens();
            Parse(page, tokens);
            Value = contents.Text;
            Text = "<!--" + Value + "-->";
            Type = TokenType.Comment;
       }

        public override string ToString()
        {
            return Text;
        }

        public Comment(string value)
        {
            contents = new Tokens();
            Value = value;
            Text = "<!--" + Value + "-->";
            Type = TokenType.Comment;
        }
    }
}
