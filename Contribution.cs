using System;
using Wikimedia;

public class Contribution
{
    public string Comment     { get; }
    public int NamespaceId    { get; }
    public int PageId         { get; }    
    public int ParentId       { get; }
    public int RevId          { get; }    
    public int Size           { get; }
    public DateTime Timestamp { get; }
    public string User        { get; }
    
    private string? title;

    public string Title  
    {
    	get
    	{
        	if (null == title)
        	{
                var bot = Bot.Instance;
            	title = bot.Title (PageId);
        	}
        	return title;
       	}
    }
           
    public Contribution (Bot bot, string user, int pageId, int revId, int parentId, int namespaceId, DateTime timestamp, string comment, int size)
    {
        User        = user;
        PageId      = pageId;
        RevId       = revId;
        ParentId    = parentId;
        NamespaceId = namespaceId;
        Timestamp   = timestamp;
        Comment     = comment;
        Size        = size;
    }
}