using System;
using System.Linq;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Coordinators
    {
        private readonly string project;
        private List<string>? coordinators;

        public bool IsCoordinator(string name)
        {
            if (null == coordinators)
            {
                coordinators = new List<string>();
                Page page = new TemplatePage("Template:@" + project);
                page.Load();
                coordinators = page.FindAllNs("User").ConvertAll(x => x.Data.Trim());
            }
            return coordinators.Exists(x => x.Equals(name));
        }

        public Coordinators(string project)
        {
            this.project = project;
        }
    }
}
