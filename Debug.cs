using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Wikimedia
{
    public class Debug
    {
        public static bool On { get; set; }
        public static bool Offline { get; set; }
        public static string? WriteToFile { get; set; }
        public static Dictionary<string, bool> Module = new Dictionary<string, bool>();

        public static string Caller(Int32 skipFrames = 2)
        {
            var stackTrace = new StackTrace(new StackFrame(skipFrames));
            return stackTrace.GetFrame(0)!.GetMethod()!.Name;
        }

        public static void Entered(string message = "", string name = "")
        {
            WriteLine(Caller() + " entered " + message, name);
        }

        public static void StackTrace()
        {
            var stackTrace = new StackTrace(true);
            foreach (var frame in stackTrace.GetFrames())
            {
                WriteLine(String.Format ("Filename: {0} Method: {1} Line: {2} Column: {3}  ",
                    frame.GetFileName(), frame.GetMethod(), frame.GetFileLineNumber(), frame.GetFileColumnNumber()));
            }
        }

        public static void Exited(string message = "", string name = "")
        {
            WriteLine(Caller() + " exited " + message, name);
        }

        public static void Version()
        {
            Assembly thisAssem = typeof(Debug).Assembly;
            AssemblyName thisAssemblyName = thisAssem.GetName();
            Version version = thisAssemblyName.Version!;
            WriteLine(String.Format("This is version {0} of {1}.", version, thisAssemblyName.Name));
        }
        
        public static void WriteLine(string message, string name = "")
        {
            if (On || (Module.ContainsKey(name) && Module[name]))
            {
                if (null == WriteToFile)
                {
                    if (Offline)
                    {
                        Console.Error.WriteLine(message);
                    }
                    else
                    {
                        Cred.Instance.Showtime(message);
                    }
                }
                else
                {
                    File.AppendAllText(WriteToFile, message + "\n");
                }
            }
        }
    }
}