using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class ExternalLink : IToken
    {
        private const int StartIndex = 0;

        public TokenType Type { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string Url { get; set; }

        public static Tokens Parse(Page page, Tokens tokens)
        {
            var contents = new Tokens();
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                switch (token.Type)
                {
                    case TokenType.CloseExternalLink:
                        return contents;
                    default:
                        contents.Add(token);
                        break;
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed external link on " + page.Title + ": " + contents.Preview());
        }

        public ExternalLink(Page page, Tokens tokens)
        {
            var contents = Parse(page, tokens);
            string s = "http" + contents.Text;
            Text = "[" + s + "]";
            Type = TokenType.ExternalLink;

            Debug.Entered("Found external link: " + Text, "ExternalLink::init");
            var firstSpaceIndex = s.IndexOf(" ");
            if (firstSpaceIndex > 0)
            {
                Url = s.Substring(StartIndex, firstSpaceIndex);
                Value = s.Substring(firstSpaceIndex + 1);
            }
            else
            {
                Url = s;
                Value = "";
            }
            Debug.Exited("Url=" + Url + " Value=" + Value, "ExternalLink::init");
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
