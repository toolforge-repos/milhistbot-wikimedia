using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Wikimedia.MilHist;

namespace Wikimedia.Featured
{
    public enum ContentType
    {
        article, list, picture, topic,
    }

    public class FeaturedNominationsPage : NominationsPage
    {
        // Create as a singleton 
        private static readonly Lazy<FeaturedNominationsPage> _instance = new Lazy<FeaturedNominationsPage>(() => new FeaturedNominationsPage());
        public static FeaturedNominationsPage Instance => _instance.Value;

        private List<FeaturedNominationPage> facNominations = new List<FeaturedNominationPage>();

        public new List<FeaturedNominationPage> Nominations
        {
            get
            {
                if (!facNominations.Any())
                {
                    var facNominations = new List<FeaturedNominationPage>();
                    var nominations = base.Nominations();
                    foreach (var page in nominations)
                    {
                        facNominations.Add(new FeaturedNominationPage(page.Title));
                    }
                }
                return facNominations;
            }
        }

        public void Add(FeaturedNominationPage nomination)
        {
            //Load();
            //var nominationTemplate = new Template(this, nomination.Title);
            //var template = Fetch(nomination.TitleMatch);
            //Insert(template, nominationTemplate, new Token("\n"));
        }

        public FeaturedNominationsPage() : base("Wikipedia:Featured article candidates")
        {
            TitleMatch = @"Featured article candidates/(?<title>.+)/archive\d+";
        }
    }

    public class FeaturedNominationPage : NominationPage
    {
        private string? coordinator;
        private string? status;
        public static Coordinators coordinators = new Coordinators("FAC");

        public string Coordinator
        {
            get
            {
                if (null == coordinator)
                {
                    coordinator = Closed.User;
                    if (!coordinators.IsCoordinator(coordinator))
                    {
                        throw new Exception(coordinator + " is not a FAC coordinator");
                    }
                }
                return coordinator;
            }
            set
            {
                coordinator = value;
            }
        }

        public ContentType ContentType
        {
            get => ContentType.article;
        }

        public string Status
        {
            get
            {
                if (null == status)
                {
                    var facClosed = Find("FACClosed");
                    if (null == facClosed)
                    {
                        status = "open";
                    }
                    else
                    {
                        status = facClosed.Fetch("1").ToLower();
                        if (status.Equals("closed"))
                        {
                            if (Promoted())
                            {
                                status = "promoted";
                            }
                            else if (Archived())
                            {
                                status = "archived";
                            }
                        }
                    }
                }
                return status;
            }
            set => status = value;
         }


        private bool Check(WikipediaPage page)
        {
            Cred.Instance.Showtime("\tchecking if " + this.Title + " has been moved to " + page.Title + "...\n");
            page.Load();
            var found = page.FindAll(this.Title);
            return found.Any();
        }

        private bool Promoted ()
        {
            var logPage = new FeaturedLogPage(Date);
            return Check(logPage);
         }

        private bool Archived()
        {
            var archivePage = new FeaturedArchivePage(Date);
            return Check(archivePage);
        }

        public void Close()
        {
            Load();
            string promoter = Closed.User;
            string date = Closed.Timestamp.ToString("HH:mm d MMMM yyyy");
            string diff = Diff;
            string comment = "'''" + Status + "''' by [[User:" + promoter + "|" + promoter + "]] via ~~~ " + date + " [" + diff + "]";
            var top = new Template(this, "Fa top");
            top.Add("result", comment + " ~~~~");
            Insert(0, top, Token.Newline);
            var bottom = new Template(this, "Fa bottom");
            Add(bottom, Token.Newline);
        }

         public static FeaturedNominationPage GetNomination(TalkPage talk)
        {
            talk.Load();
            Template template = talk.Fetch("featured article candidates");
            var nomination = new FeaturedNominationPage(template);
            nomination.ArticleTalk = talk;
            return nomination;
        }

        public FeaturedNominationPage(string title, int revid = -1) : base(title, revid)
        {
 			Action = "FAC";
            TitleMatch = @"Wikipedia:Featured article candidates/(?<title>.+)/archive\d+";
            CloseTemplateName = "FACClosed";
        }

        public FeaturedNominationPage(Template template): this("Wikipedia:Featured article candidates/" + template.Fetch("1"))
        {
        }

    }

    public class FeaturedLogPage : WikipediaPage
    {
        public static string Path(DateTime date)
        {
            return "Wikipedia:Featured article candidates/Featured log/" + date.ToString("MMMM yyyy");
        }

        public FeaturedLogPage(DateTime date, int revid = -1) : base(Path(date), revid)
        {
        }
    }

    public class FeaturedArchivePage : WikipediaPage
    {
        public static string Path(DateTime date)
        {
            return "Wikipedia:Featured article candidates/Archived nominations/" + date.ToString("MMMM yyyy");
        }

        public FeaturedArchivePage(DateTime date, int revid = -1) : base(Path(date), revid)
        {
        }
    }

    public class FeaturedReviewNominationsPage : NominationsPage
    {
                // Create as a singleton 
        private static readonly Lazy<FeaturedReviewNominationsPage> _instance = new Lazy<FeaturedReviewNominationsPage>(() => new FeaturedReviewNominationsPage());
        public static FeaturedReviewNominationsPage Instance => _instance.Value;

        private List<FeaturedReviewNominationPage> farNominations = new List<FeaturedReviewNominationPage>();

        public new List<FeaturedReviewNominationPage> Nominations
        {
            get
            {
                if (!farNominations.Any())
                {
                    var facNominations = new List<FeaturedReviewNominationPage>();
                    var nominations = base.Nominations();
                    foreach (var page in nominations)
                    {
                        facNominations.Add(new FeaturedReviewNominationPage(page.Title));
                    }
                }
                return farNominations;
            }
        }


        private FeaturedReviewNominationsPage() : base("Wikipedia:Featured article review")
        {
           TitleMatch = @"Featured article review/(?<title>.+)/archive\d+";
        }
    }

    public class FeaturedReviewNominationPage: NominationPage
	{
        private string? status;
 
        public string Status
        {
            get
            {
                if (null == status)
                {
                    var farClosed = Find("FARClosed");
                    if (null == farClosed)
                    {
                        status = "open";
                    }
                    else
                    {
                        status = farClosed.Fetch("1").ToLower();
                        if (status.Equals("keep"))
                        {
                            status = "kept";
                        }
                        else if (status.Equals("archived"))
                        {
                            status = "delisted";
                        }
                    }
                }
                return status;
            }
            set => status = value;
        }

        public void Close()
        {
            Load();
            string promoter = Closed.User;
            string date = Closed.Timestamp.ToString("HH:mm d MMMM yyyy");
            string diff = Diff;
            string comment = "'''" + Status + "''' by [[User:" + promoter + "|" + promoter + "]] via ~~~ " + date + " [" + diff + "]";
            var top = new Template(this, "subst:FAR top");
            top.Add("result", comment + " ~~~~");
            Insert(0, top, Token.Newline);
            var bottom = new Template(this, "subst:FAR bottom");
            Add(bottom, Token.Newline);
        }

        public static FeaturedReviewNominationPage GetNomination(TalkPage talk)
        {
            talk.Load();
            Template template = talk.Fetch("featured article review");
            FeaturedReviewNominationPage nomination = new FeaturedReviewNominationPage(template);
            nomination.ArticleTalk = talk;
            return nomination;
        }

        public FeaturedReviewNominationPage(string title) : base(title)
        {
			Action = "FAR";
            TitleMatch = Nominations.TitleMatch;
            CloseTemplateName = "FARClosed";
        }

        public FeaturedReviewNominationPage(Template template) : this("Wikipedia:Featured article review/" + template.Fetch("1"))
        {
        }
    }

    public class FeaturedListPromotedArchivePage: WikipediaPage
    {
        public FeaturedListPromotedArchivePage(DateTime dt): base("Wikipedia:Featured list candidates/Featured log/" + dt.ToString("MMMM yyyy"))
        {
        }
    }

    public class FeaturedListFailedArchivePage: WikipediaPage
    {
        public FeaturedListFailedArchivePage(DateTime dt): base("Wikipedia:Featured list candidates/Failed log/" + dt.ToString("MMMM yyyy"))
        {
        }
    }

    public class FeaturedListNominations: NominationsPage
    {
        // Create as a singleton 
        private static readonly Lazy<FeaturedListNominations> _instance = new Lazy<FeaturedListNominations>(() => new FeaturedListNominations());
        public static FeaturedListNominations Instance => _instance.Value;

        private FeaturedListNominations() : base("Wikipedia:Featured list candidates")
        {
            TitleMatch = @"Featured list candidates/(?<title>.+)/archive\d+";
        }
    }

    public class FeaturedListNomination: NominationPage
	{
        private string? coordinator;
        private Coordinators? coordinators;
        private string? status;
        private Template? template;
    
        public string Coordinator
        {
            get
            {
                if (null == coordinator)
                {
                    coordinator = Closed.User;
                    if (! Coordinators.IsCoordinator(coordinator))
                    {
                        throw new Exception(coordinator + " is not a FLC coordinator");
                    }
                }
                return coordinator;
            }
            set => coordinator = value;
        }

        public Coordinators Coordinators
        {
            get
            {
                if (null == coordinators)
                {
                    coordinators  = new Coordinators("FLC");
                }
                return (Coordinators)coordinators;
            }
            set => coordinators = value;
        }

        public ContentType ContentType
        {
            get => ContentType.list;
        }

        public string Status
        {
            get
            {
                if (null == status)
                {
                    var farClosed = Find("FLCClosed");
                    if (null == farClosed)
                    {
                        status = "open";
                    }
                    else
                    {
                        status = farClosed.Fetch("1").ToLower();
                        if (status.Matches(@"^promoted|successful"))
                        {
                            status = "promoted";
                        }
                        else if (status.Matches(@"^not promoted|failed|withdrawn|archived|unsuccessful"))
                        {
                            status = "archived";
                        }
                        else
                        {
                            throw new Exception("unknown status: '" + status + "'");
                        }
                    }
                }
                return status;
            }
            set => status = value;
        }

        public Template Template
        {
            get
            {
                if (null == template)
                {
                    Nominations.Load();
                    template = Nominations.Fetch(Title);
                }
                return template;
            }
            set => template = value;
        }

        public void Close()
        {
            Load();
            string promoter = Closed.User;
            string date = Closed.Timestamp.ToString("HH:mm d MMMM yyyy");
            string diff = Diff;
            string comment = "'''" + Status + "''' by [[User:" + promoter + "|" + promoter + "]] via ~~~ " + date + " [" + diff + "]";
            var top = new Template(this, "subst:FL top");
            top.Add("result", comment + " ~~~~");
            Insert(0, top, Token.Newline);
            var bottom = new Template(this, "subst:FL bottom");
            Add(bottom, Token.Newline);
        }

        public static FeaturedListNomination GetNomination(TalkPage talk)
        {
            talk.Load();
            Template template = talk.Fetch("featured list candidates");
            FeaturedListNomination nomination = new FeaturedListNomination(template);
            nomination.ArticleTalk = talk;
            return nomination;
        }

        public FeaturedListNomination(string title) : base(title)
        {
			Action = "FLC";
            Nominations = FeaturedListNominations.Instance;
            TitleMatch = Nominations.TitleMatch;
            CloseTemplateName = "FLCClosed";
        }

        public FeaturedListNomination(Template template) : this("Wikipedia:Featured list candidates/" + template.Fetch("1"))
        {
        }

    }
}
