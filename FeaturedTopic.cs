﻿using System;
using System.Collections.Generic;

namespace Wikimedia
{
	public class FeaturedTopicPage: WikipediaPage
	{
		private Template? Get(string title)
		{
			Load();
			Template? result = null;
			int index = Links.FindIndex(x => x.Data.Equals(title));
			if (index >= 0)
			{
				List<Template> icons = FindAll("icon");
				result = icons[index];
			}
			return result;
		}

		public void Set(string title, string value)
		{
			Template? icon = Get(title);
			if (null != icon)
			{ 
				icon["1"] = value;
            }
        }

        public FeaturedTopicPage(string ftname) : base("Wikipedia:Featured topics/" + ftname) 
		{
		}
	}
}
