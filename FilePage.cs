﻿using System;
namespace Wikimedia
{
    public class FilePage : ArticlePage
    {
        private FileTalkPage? talk;
        public new FileTalkPage Talk
        {
            get => talk ??= new FileTalkPage(TitleWithoutNamespace);
        }


        public FilePage(string title, int revid = -1) : base(new Namespace("File"), title, revid)
        {
        }
    }

    public class FileTalkPage : TalkPage
    {
        private FilePage? article;
        public new FilePage Article
        {
            get => article ??= new FilePage(TitleWithoutNamespace);
        }

        public FileTalkPage(string title, int revid = -1) : base(new Namespace("File talk"), title, revid)
        {
        }
    }
}


