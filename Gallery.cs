using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia
{
    public struct Picture
    {
        public Picture(string ns, string image, Tokens caption)
        {
            Ns = ns;
            Image = image;
            Caption = caption;
        }

        public string Ns;
        public string Image;
        public Tokens Caption;

        public override string ToString() => $"{Image}|{Caption.Text})";
    }

    public class Gallery : IToken
    {
        public TokenType Type { get; set; }
        public string Text { get; set; }

        private Dictionary<string, string> attributes;
        public List<Picture> Pictures { get; }

        public override string ToString()
        {
            return Text;
        }

        //  mode=
        //  traditional Default, effect is explained below
        //  nolines No borders, less padding, captions centered under images
        //  packed All images aligned by having same height, justified, captions centered under images
        //  packed-overlay Like packed, but caption overlays the image, in a translucent box
        //  packed-hover Like packed-overlay, but caption is only visible on hover (degrades gracefully on screen readers, and falls back to packed-overlay if a touch screen is used)
        //  slideshow Slideshow
        public string Mode
        {
            get
            {
                return attributes.ContainsKey("mode") ? attributes["mode"] : "";
            }
            set
            {
                attributes["mode"] = value;
            }
        }

        // caption= Adds an overall caption above the gallery
        public string Caption
        {
            get
            {
                return attributes.ContainsKey("caption") ? attributes["caption"] : "";
            }
            set
            {
                attributes["caption"] = value;
            }
        }

        // widths= Image widths in pixels (has no effect if mode is set to packed, packed-overlay, packed-hover, or slideshow)
        public string Widths
        {
            get
            {
                return attributes.ContainsKey("widths") ? attributes["widths"] : "";
            }
            set
            {
                attributes["widths"] = value;
            }
        }

        // heights= Image heights in pixels (has no effect if mode is set to slideshow)
        public string Heights
        {
            get
            {
                return attributes.ContainsKey("heights") ? attributes["heights"] : "";
            }
            set
            {
                attributes["heights"] = value;
            }
        }

        // showfilename=yes Show each filename below the corresponding image
        public bool Showfilename
        {
            get
            {
                return attributes.ContainsKey("showfilename") && attributes["Showfilename"].Equals("yes");
            }
            set
            {
                attributes["Showfilename"] = (value ? "yes" : "no");
            }
        }

        // class= One or more class names separated with spaces and enclosed in double quotes
        public string Class
        {
            get
            {
                return attributes.ContainsKey("class") ? attributes["class"] : "";
            }
            set
            {
                attributes["class"] = value;
            }
        }

        // style= One or more CSS declarations separated with semicolons and enclosed in double quotes
        public string Style
        {
            get
            {
                return attributes.ContainsKey("style") ? attributes["style"] : "";
            }
            set
            {
                attributes["style"] = value;
            }
        }

        void parse(Token open)
        {
            attributes = new Dictionary<string, string>();
            string quote = "\"";
            string pattern = @"(?<attribute>\w+?)? *= *(?<value>(\w+|" + quote + ".+?" + quote + "))?";
            Regex regex = new Regex(pattern);
            foreach (Match match in regex.Matches(open.Text))
            {
                // string[] names  = regex.GetGroupNames();
                Group attribute = match.Groups["attribute"];
                Group value = match.Groups["value"];
                // Debug.WriteLine("Header: " + attribute.Value + " => " + value.Value);
                attributes.Add(attribute.Value, value.Value);
            }
        }

        void parse(Tokens tokens)
        {
            while (tokens.Any())
            {
                string ns = "";
                Token? t = tokens.Dequeue() as Token;
                // Debug.WriteLine("Gallery: (0) token=" + t.Text + " type=" + t.Type);
                if (null == t || !tokens.Any())
                {
                    break;
                }
                if (t.Type == TokenType.Newline)
                {
                    continue;
                }
                Token? image = t;
                Debug.WriteLine("Gallery: (1) image=" + image.Text + " type=" + image.Type);
                if ((image.Text.Equals("File") || image.Text.Equals("Image")) && tokens.Peek().Type == TokenType.Colon)
                {
                    ns = image.Text;
                    t = tokens.Dequeue() as Token;
                    // Debug.WriteLine("Gallery: (2) token=" + t.Text + " type=" + t.Type);
                    image = tokens.Dequeue() as Token;
                }
                if (null == image)
                {
                    throw new ParseException("Unable to parse Gallery");
                }

                Tokens caption = new Tokens();
                if (tokens.Peek().Type == TokenType.Pipe)
                {
                    var token = tokens.Dequeue();
                    Debug.WriteLine("Gallery: (3) token=" + token.Text + " type=" + token.Type);
                    while (tokens.Peek().Type != TokenType.Newline)
                    {
                        token = tokens.Dequeue();
                        Debug.WriteLine("Gallery: (4) token=" + token.Text + " type=" + token.Type);
                        caption.Add(token);
                    }
                }

                t = tokens.Peek() as Token;
                if (null == t || t.Type != TokenType.Newline)
                {
                    throw new ParseException("Unable to parse Gallery");
                }
                var picture = new Picture(ns, image.Text, caption);
                Pictures.Add(picture);
            }
        }

        public Gallery(Page page, IToken open, Tokens tokens) : this(page)
        {
            Tokens contents = new Tokens();
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.Reference:
                            contents.Add(new Reference(page, token));
                            break;
                        case TokenType.OpenReference:
                            contents.Add(new Reference(page, token, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            contents.Add(new Template(page, tokens));
                            break;
                        case TokenType.CloseGallery:
                            Text = open + contents.Text + token;
                            // parse (open as Token);
                            // parse (contents);
                            return;
                        default:
                            Debug.WriteLine("Add: token=" + token.Text + " type=" + token.Type);
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    Debug.WriteLine("ParseException: token=" + token.Text + " type=" + token.Type);
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed Gallery on " + page.Title + ": " + contents.Preview());
        }

        public Gallery(Page page)
        {
            Type = TokenType.Gallery;
            Text = "";
            attributes = new Dictionary<string, string>();
            Pictures = new List<Picture>();
        }
    }
}
