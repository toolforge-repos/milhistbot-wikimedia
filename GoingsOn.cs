﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Wikimedia.Featured;

namespace Wikimedia
{
	public class GoingsOn: WikipediaPage
	{
        // Create as a singleton 
        private static readonly Lazy<GoingsOn> _instance = new Lazy<GoingsOn>(() => new GoingsOn());
        public static GoingsOn Instance => _instance.Value;

        private Table Table;
        private static Dictionary<Featured.ContentType, string> Lookup = new Dictionary<Featured.ContentType, string>()
        {
            { Featured.ContentType.article, "Articles" },
            { Featured.ContentType.list,    "Lists"    },
            { Featured.ContentType.picture, "Pictures" },
            { Featured.ContentType.topic,   "Topics"   },
        };

        public DateTime Date
        {
            get;
        }

        private Link FetchLinkByValue(string match)
        {
            var link = Links.Find(ind => (Regex.IsMatch(ind.Redirect, match, RegexOptions.IgnoreCase)));
            if (null == link)
            {
                throw new Exception("unable to find subheading '" + match + "'");
            }
            return link;
        }

        public void Add (Page page, Featured.ContentType featuredContentType)
        {
            Debug.Entered("featuredContentType=" + featuredContentType);
            string subheading = Lookup[featuredContentType];
            Link link = FetchLinkByValue(subheading);
            Link pageLink = new Link(page.Title);
            string today = DateTime.Now.ToString("d MMM");
            Table.Contents.Append(Table.Contents.EndOfLineIndex(link), new Token("* "), pageLink, new Token(" (" + today + ")"), Token.Newline);
            Debug.Exited();
        }

        private DateTime GetDate()
        {
            Debug.Entered();
            var text = FindText("Goings-on in the week starting Sunday, ");
            if (null == text)
            {
                throw new Exception("unable to find Goings-on date line");
            }

            Debug.WriteLine(text.ToString() ?? "Null text");
            string? day = null;
            string? month = null;
            string? year = null;
            var line = GetLine(text);
            foreach (var token in line)
            {
                if (token is Link)
                {
                    Link link = (Link)token;
                    Debug.WriteLine("Found link: " + link.Data);
                    if (link.Data.Matches(@"(\w+) (\d+)"))
                    {
                        string pattern = @"(?<month>\w+) (?<day>\d+)";
                        MatchCollection matches = Regex.Matches(link.Data, pattern);
                        foreach (Match match in matches)
                        {
                            month = match.Groups["month"].Value;
                            day = match.Groups["day"].Value;
                            break;
                        }
                    }
                    if (link.Data.Matches(@"^\d+$"))
                    {
                        year = link.Data;
                    }
                 }
            }

            if (null == day)
            {
                throw new Exception("unable to find Goings-on day");
            }
            if (null == month)
            {
                throw new Exception("unable to find Goings-on month");
            }
            if (null == year)
            {
                throw new Exception("unable to find Goings-on year");
            }

            DateTime dateTime = DateTime.Parse(day + " " + month + " " + year);
            Debug.Exited("date=" + dateTime.ToString("dd MMMM yyyy"));
            return dateTime;
        }

        private Table GetTable()
        {
            var token = Contents.Contents.Find(x => x.Type == TokenType.Table);
            if (null == token)
            {
                throw new Exception("unable to find table");
            }
            return (Table)token;
        }

        private GoingsOn(): base("Wikipedia:Goings-on")
		{
            Load();
            Date = GetDate();
            Table = GetTable();
		}
	}
}
