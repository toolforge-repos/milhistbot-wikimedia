using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public class GANominationPage : NominationPage
	{
		public string Version
		{
			get
			{
				string version;
				Regex filter = new Regex(TitleMatch);
				var match = filter.Match(Title);
				if (match.Success)
				{
					version = match.Groups["version"].Value;
				}
				else
				{
					throw new Exception("Bad GA nomination name: '" + Title + "'");
				}
				return version;
			}
        }

		public GANominationPage(string name) : base(name)
		{
            Action = "GAN";
            TitleMatch = @"Talk:(?<title>.+)/GA(?<version>\d+)";
			Nominations = GANominationsPage.Instance;
        }
    }

	public class GANominationsPage : NominationsPage
	{
		// Create as a singleton
        private static readonly Lazy<GANominationsPage> _instance = new Lazy<GANominationsPage>(() => new GANominationsPage());
        public static GANominationsPage Instance => _instance.Value;


        private readonly List<GANominationPage> nominations;

		public List<GANominationPage> GANominations
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll("GAentry");
					foreach (Template template in nominationTemplates)
					{
						string name = "Talk:" + template["1"] + "/GA" + template["2"];
						GANominationPage nominationPage = new GANominationPage(name);
						nominations.Add(nominationPage);
					}
				}
				return nominations;
			}
		}
		private GANominationsPage() : base("Wikipedia:Good article nominations")
		{
            nominations = new List<GANominationPage>();
        }
    }

    public class GAReassessmentPage : NominationPage
    {
        public GAReassessmentPage(string name) : base(name)
        {
            Action = "GAR";
            TitleMatch = @"Talk:(.+)/GA\d+";
            Nominations = GANominationsPage.Instance;
        }
    }

    public class GAReassessmentsPage : NominationsPage
	{
		// Create as a singleton
        private static readonly Lazy<GAReassessmentsPage> _instance = new Lazy<GAReassessmentsPage>(() => new GAReassessmentsPage());
        public static GAReassessmentsPage Instance => _instance.Value;
		private readonly List<GAReassessmentPage> nominations;

		public List<GAReassessmentPage> GAReassessments
		{
			get
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll("CF/Wikipedia good article reassessment");
					foreach (Template template in nominationTemplates)
					{
                        GAReassessmentPage nominationPage = new GAReassessmentPage("Wikipedia:" + template["1"]!);
						nominations.Add(nominationPage);
					}
				}
				return nominations;
			}
		}
		public GAReassessmentsPage() : base("User:AnomieBOT/C/Wikipedia good article reassessment")
		{
			nominations = new List<GAReassessmentPage>();
        }
    }

	public static class GoodArticleListPageFactory
	{
		public static List<GoodArticleListPage> GoodArticleIndexPages;
		public static List<GoodArticleListPage> GoodArticleListPages;

		public static GoodArticleListPage? GetGoodArticleListPage(string name)
		{
			foreach (GoodArticleListPage page in GoodArticleListPages)
			{
				if (page.Contains(name))
				{
					return page;
				}
			}
			return null;
		}

		static GoodArticleListPageFactory()
		{
			GoodArticleIndexPages = new List<GoodArticleListPage>() {
					new GoodArticleListPage ("Wikipedia:Good articles/all"),
					new GoodArticleListPage ("Wikipedia:Good articles/all2"),
				};

			GoodArticleListPages = new List<GoodArticleListPage>();
			foreach (var indexPage in GoodArticleIndexPages)
			{
				indexPage.Load();
				List<Template> templates = indexPage.FindAll("Wikipedia:Good articles/[A-Z]");
				foreach (Template template in templates)
				{
					GoodArticleListPages.Add(new GoodArticleListPage(template.Name));
				}
			}
		}
    }

	public class GoodArticleListPage : WikipediaPage
	{
        public bool Contains(string name)
        {
			Load();
            return null != FindLink(name);
        }

        public GoodArticleListPage(string name) : base(name)
		{
		}
	}

}
