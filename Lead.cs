using System;
using System.Collections.Generic;
using Wikimedia;

public class Lead
{
    private List<IToken> tokens;

	public int Sentences
	{
        get
        {
            int count = 0;
            foreach (var token in tokens)
            {
                switch (token.Type)
                {
                    case TokenType.Text:
                        if (token.Text.Contains('.'))
                        {
                            count++;
                        }
                        break;
                }
            }
            return count;
        }
    }

    public Lead(ArticlePage page)
	{
		tokens = page.Contents.Contents.GetRange(0, page.FindIndex(page.Sections[0]));
	}
}
