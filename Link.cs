using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Link : IToken, IPageElement, ITreeElement
    {
        public TokenType Type { get; set; }
        public virtual string Text { get; set; }
        public Namespace Namespace { get; set; }
        public string Data { get; set; }
        public string Redirect { get; set; }
        public Tokens Contents { get; set; }

        public string Value
        {
            get => Redirect.Equals("") ? Data : Redirect;
        }

        public override string ToString()
        {
            return Text;
        }

        public void Added(Page page)
        {
            page.Links.Add(this);
        }

        public void Removed(Page page)
        {
            page.Links.Remove(this);
        }

        public Tokens TreeContents()
        {
            return Contents;
        }

        private static Link SpecialLinks(Link link, Tokens contents)
        {
            if (ExtendedImage.IsExtendedImage(link))
            {
                return new ExtendedImage(contents);
            }
            return link;
        }
        private void Parse(Tokens tokens)
        {
            Tokens queue = tokens.Clone();
            // Console.WriteLine ("Link = '" + queue.Text + "'");

            bool colon = false;
            if (queue.IsToken(TokenType.Colon))
            {
                colon = true;
                queue.Dequeue();
            }

            string ns = "";
            if (queue.Any(2) && Namespace.IsNamespace(queue.Peek().Text) && queue.Peek(1).Type == TokenType.Colon)
            {
                ns = queue.Dequeue().Text;
                queue.Dequeue();
            }
            Namespace = new Namespace(ns, colon);

            Redirect = "";
            Tokens data = new Tokens();
            while (queue.Any())
            {
                if (queue.IsToken(TokenType.Pipe))
                {
                    queue.Dequeue();
                    Redirect = queue.Text;
                    break;
                }
                data.Add(queue.Dequeue());
            }
            Data = data.Text;
            // Console.WriteLine ("Data = '" + data + "' Redirect='" + Redirect + "'");
        }

        public Link(Page page, Tokens tokens)
        {
            Tokens contents = new Tokens();
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            contents.Add(new Template(page, tokens));
                            break;
                        case TokenType.Reference:
                            contents.Add(new Reference(page, token));
                            break;
                        case TokenType.OpenReference:
                            contents.Add(new Reference(page, token, tokens));
                            break;
                        case TokenType.CloseBracket:
                            Tokens queue = tokens.Clone();
                            Namespace = new Namespace("");
                            Data = "";
                            Redirect = "";
                            Parse(contents);
                            Text = "[[" + contents.Text + "]]";
                            Type = TokenType.Link;
                            page.Links.Add(SpecialLinks(this, contents));
                            Contents = contents;
                            return;
                        default:
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed link on " + page.Title + ": " + contents.Preview());
        }

        public Link()
        {
            Text = "";
            Type = TokenType.Link;
            Namespace = new Namespace("");
            Data = "";
            Redirect = "";
            Contents = new Tokens();
        }

        public Link(string data)
        {
            Text = "[[" + data + "]]";
            Type = TokenType.Link;
            Namespace = new Namespace("");
            Data = data;
            Redirect = "";
            Contents = new Tokens();
        }

        public Link(string data, string redirect)
        {
            Text = "[[" + data + "|" + redirect + "]]";
            Type = TokenType.Link;
            Namespace = new Namespace("");
            Data = data;
            Redirect = redirect;
            Contents = new Tokens();
        }

        public Link(Namespace ns, string data)
        {
            Text = "[[" + ns + ":" + data + "]]";
            Type = TokenType.Link;
            Namespace = ns;
            Data = data;
            Redirect = "";
            Contents = new Tokens();
        }

        public Link(Namespace ns, string data, string redirect)
        {
            Text = "[[" + ns + ":" + data + "|" + redirect + "]]";
            Type = TokenType.Link;
            Namespace = ns;
            Data = data;
            Redirect = redirect;
            Contents = new Tokens();
        }
    }

    public class ExtendedImage : Link
    {
        public string Name { get; set; }
        public string? ImageType { get; set; }
        public string? Border { get; set; }
        public string? Location { get; set; }
        public string? Alignment { get; set; }
        public string? Size { get; set; }
        public string? Upright { get; set; }
        public string? ImageLink { get; set; }
        public string? Alt { get; set; }
        public string? ImagePage { get; set; }
        public string? Lang { get; set; }
        public string? Caption { get; set; }

        public override string Text
        {
            get
            {
                Debug.Entered();
                if (Contents.Empty())
                {
                    Contents = new Tokens();
                    var pipe = "|";
                    Contents.Add(Namespace.Text, ":", Name);
                    if (null != ImageType)
                    {
                        Contents.Add(pipe, ImageType);
                    }
                    if (null != Border)
                    {
                        Contents.Add(pipe, Border);
                    }
                    if (null != Location)
                    {
                        Contents.Add(pipe, Location);
                    }
                    if (null != Alignment)
                    {
                        Contents.Add(pipe, Alignment);
                    }
                    if (null != Size)
                    {
                        Contents.Add(pipe, Size);
                    }
                    if (null != ImageLink)
                    {
                        Contents.Add(pipe, ImageLink);
                    }
                    if (null != Alt)
                    {
                        Contents.Add(pipe, Alt);
                    }
                    if (null != ImagePage)
                    {
                        Contents.Add(pipe, ImagePage);
                    }
                    if (null != Lang)
                    {
                        Contents.Add(pipe, Lang);
                    }
                    if (null != Caption)
                    {
                        Contents.Add(pipe, Caption);
                    }
                }
                Debug.Exited("Text=" + Contents.Text);
                return "[[" + Contents.Text + "]]";
            }
            set
            {
                Contents = new Tokens(value);
            }
        }

        static public bool IsExtendedImage(Link link)
        {
            return link.Namespace.Equals("File");
        }

        public ExtendedImage(Tokens contents) : base()
        {
            Type = TokenType.ExtendedImage; 
            Contents = contents;
            var tokens = contents.Clone ().Contents;
            Namespace = new Namespace(tokens[0].Text);
            tokens.RemoveAt(0);
            tokens.RemoveAt(0); // Colon
            Name = tokens[0].Text;
            tokens.RemoveAt(0);
            while (tokens.Count > 0)
            {
                if (tokens[0].Type == TokenType.Text)
                {
                    var text = tokens[0].Text;
                    if (text.Matches("thumb|frame"))
                    {
                        ImageType = text;
                    }
                    else if (text.Matches("border"))
                    {
                        Border = text;
                    }
                    else if (text.Matches("right|left|center|none"))
                    {
                        Location = text;
                    }
                    else if (text.Matches("baseline|middle|sub|super|text-top|text-bottom|top|bottom"))
                    {
                        Alignment = text;
                    }
                    else if (text.Matches(@"upright"))
                    {
                        if (tokens.Count > 0 && tokens[1].Type == TokenType.Equals)
                        {
                           tokens.RemoveAt(0);
                           tokens.RemoveAt(0);
                           Upright = tokens[0].Text;
                        } 
                        else
                        {
                            Upright = "";
                        }
                    }
                    else if (text.Matches(@"\d+\s*px"))
                    {
                        Size = text;
                    }
                    else if (text.Matches("^link$"))
                    {
                        if (tokens.Count > 2 && tokens[1].Type == TokenType.Equals)
                        {
                            tokens.RemoveAt(0);
                            tokens.RemoveAt(0);
                            ImageLink = tokens[0].Text;
                        }
                    }
                    else if (text.Matches("^alt="))
                    {
                        Alt = text;
                    }
                    else if (text.Matches("^page="))
                    {
                        ImagePage = text;
                    }
                    else if (text.Matches("^lang"))
                    {
                        Lang = text;
                    }
                    else
                    {
                        Caption = text;
                    }
                }
                tokens.RemoveAt(0);
            }
        }

        public ExtendedImage(string name) : base()
        {
            Name = name;
            Type = TokenType.ExtendedImage;
            Namespace = new Namespace("File");
        }

     }
}
