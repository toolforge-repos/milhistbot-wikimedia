﻿using System;

namespace Wikimedia
{
	public class MagicTemplate : Template
    {
        public override string Text { get => "{{" + Contents + "}}"; }

        public static bool IsMagicTemplate (string name)
		{
            return name.Matches("^(#|fullurl)");
        }

        public override string ToString()
        {
            return Text;
        }

        new public void Added(Page page)
        {
            Page = page;
            Page.Templates.Add(this);
        }

        private void Parse(Tokens tokens)
        {
            Tokens queue = tokens.Clone();
            var name = TemplateName(queue);
            Name = RemoveComments(name).Text.Trim();
         }

        public MagicTemplate(Page page, Tokens tokens) : base (page, "")
        {
            Name = "";
            Tokens contents = new Tokens();

            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenGallery:
                            contents.Add(new Gallery(page, token, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.Reference:
                            contents.Add(new Reference(page, token));
                            break;
                        case TokenType.OpenReference:
                            contents.Add(new Reference(page, token, tokens));
                            break;
                        case TokenType.OpenParameter:
                            contents.Add(new Parameter(page, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            var template = TemplateFactory.GetTemplate(page, tokens);
                            template.Owner = this;
                            contents.Add(template);
                            break;
                        case TokenType.CloseTemplate:
                            Parse(contents);
                            Contents = contents;
                            Added(page);
                            return;
                        default:
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("MagicTemplate: Unclosed template on " + page.Title + ": " + contents.Preview());
        }

	}
}

