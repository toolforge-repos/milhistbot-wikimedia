using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections;

namespace Wikimedia.MilHist
{
    public class MilHist
    {
        public const string ProjectPath = "Wikipedia:WikiProject Military history";
        public const string ProjectTalkPath = "Wikipedia talk:WikiProject Military history";
        private const string TemplateNameRegex = "^(WikiProject Military History|WikiProject Military history|WPCAS|WikiProject Colditz|MILHIST|Milhist|WP Military History|WikiProject MILHIST|WikiProject War|WP Military history|WPMILHIST|WPMilhist|MilHist|Mil Hist|WPMH|WP Mil|Military history|WPMIL|WP MILHIST|WikiProject Military|Wpmil)$";
        private readonly TalkPage talk;
        private ProjectTemplate? projectTemplate;
         private Coordinators? coordinators;

        public ProjectTemplate ProjectTemplate
        {
            get
            {
                if (null == projectTemplate)
                {
                    var template = talk.Templates.Find(x => x is ProjectTemplate);
                    if (null == template)
                    {
                        projectTemplate = new ProjectTemplate(talk);
                    }
                    else
                    {
                        projectTemplate = template as ProjectTemplate ;
                    }
                }
                return projectTemplate!;
            }
            set
            {
                if (null != projectTemplate && value != projectTemplate)
                {
                    talk.Remove(projectTemplate);
                }
                projectTemplate = value;
            }
        }

        // The template already gets and sets the banner shell
        public string Class
        {
            get
            {
                return ProjectTemplate.Class;
            }
            set
            {
                ProjectTemplate.Class = value;
            }
        }

        public bool IsAClass
        {
            get
            {
                return ProjectTemplate.AClass.Equals("pass");
            }
        }

        /*
        private void initMilHistTemplateNameRegex()
        {
            // Regenerate from the redirects
            var templatePage = new TemplatePage("Template:WikiProject Military history");
            var redirects = templatePage.Redirects();
            List<string> s = new List<string>();
            foreach (var redirect in redirects)
            {
                s.Add(redirect.TitleWithoutNs());
            }
            string milHistTemplateNameRegex2 = String.Join("|", s);
            Console.WriteLine(milHistTemplateNameRegex2);
        }
        */

        public static bool IsMilHist(string name)
        {
            Debug.Entered("name='" + name + "'");
            var matches = name.Trim ().Matches(TemplateNameRegex);
            Debug.Exited("matches=" +  matches);
            return(matches);
        }

        public static bool IsMilHist(TalkPage page)
        {
            page.Load();
            return page.Exists(TemplateNameRegex);
        }

        public static bool IsMilHist(ArticlePage page)
        {
            return IsMilHist(page.Talk);
        }

        public bool IsCoordinator(string name)
        {
            if (null == coordinators)
            {
                coordinators = new Coordinators("MILHIST");
            }
            return coordinators.IsCoordinator(name);
        }

        public MilHist(TalkPage talk)
        {
            this.talk = talk;
            talk.MilHist = this;
        }
    }

    public class Rating
    {
        public string Class { get; set; }
        public bool HasReferences { get; set; }
        public bool HasCoverage { get; set; }
        public bool HasStructure { get; set; }
        public bool HasGrammar { get; set; }
        public bool HasSupport { get; set; }

        private const string addenda = "Notes|References|External links|Further reading|See Also|Bibliography|Citations";

        private bool unsourced(Page article)
        {
            return article.InCategory("Category:All articles with unsourced statements");
        }

        private string listRating(string rating)
        {
            rating = rating switch
            {
                "Start" or "Stub" => "List",
                "C" => "CL",
                "B" => "BL",
                "SIA" => "SIA",
                "Redirect" => "Redirect",
                _ => throw new ApplicationException("unknown rating: '" + rating + "'"),
            };
            return rating;
        }

        private List<IToken> getLineTokens(Page article)
        {
            int firstIndex = 0;

            int lastIndex = article.FindLastIndex();
            if (lastIndex < 1)
            {
                return new List<IToken>();
            }

            var firstSection = article.FindSection(".");
            if (null != firstSection)
            {
                firstIndex = article.FindIndex(firstSection) + 1;
            }

            var lastSection = article.FindSection(addenda);
            if (null != lastSection)
            {
                lastIndex = article.FindIndex(lastSection);
            }

            if (lastIndex <= firstIndex)
            {
                firstIndex = 0;
            }

            var lineTokens = article.GetRange(firstIndex, lastIndex - firstIndex);

            return lineTokens;
        }

        private bool inlineCitations(ArticlePage article)
        {
            Debug.Entered();
            bool isReferenced = true;
            if (unsourced(article))
            {
                isReferenced = false;
            }
            else
            {
                var count = article.Sections.Count - article.FindAllSections(addenda).Count;
                if (count <= 0)
                {
                    isReferenced = false;
                }
                else if (article.Exists(@"^(Verify|" +
                            "Not verified|" +
                            "Cleanup-verify|" +
                            "Notverified|" +
                            "Cite sources|" +
                            "Sources|" +
                            "More sources|" +
                            "Citations missing|" +
                            "Referenced|" +
                            "Citations needed|" +
                            "Moresources|" +
                            "Missing citations|" +
                            "Morerefs|" +
                            "Morereferences|" +
                            "Moreref|" +
                            "Fewreferences|" +
                            "Cleanup-cite|" +
                            "Cleanup cite|" +
                            "Few references|" +
                            "More citations needed|" +
                            "More references|" +
                            "Improve-refs|" +
                            "Ref-improve|" +
                            "Ref improve|" +
                            "Improve references|" +
                            "Improvereferences|" +
                            "Improverefs|" +
                            "Improve refs|" +
                            "RefImprove|" +
                            "Verification|" +
                            "Additionalcitations|" +
                            "Additional citations|" +
                            "Improveref|" +
                            "Fewrefs|" +
                            "Few refs|" +
                            "More refs|" +
                            "Ref Improve|" +
                            "Reference improve|" +
                            "Refimproved|" +
                            "Needs more references|" +
                            "Citationsneeded|" +
                            "Refim|" +
                            "Add references|" +
                            "Addref|" +
                            "Rip|" +
                            "Improve-references|" +
                            "Few sources)$"))
                {
                    Debug.WriteLine("Matched improve references template");
                    isReferenced = false;
                }
                else
                {
                    var lineTokens = getLineTokens(article);
                    var lines = lineTokens.FindAll(ind => (ind.Type == TokenType.Newline));

                    foreach (var line in lines)
                    {
                        IToken last;
                        int index = article.FindIndex(line);
                        if (index <= 0)
                        {
                            isReferenced = false;
                            break;
                        }
                        do
                        {
                            last = article.GetToken(--index);
                        } while (index > 0 && (last.Type == TokenType.Newline || last.Type == TokenType.Comment || last.Type == TokenType.Table));

                        if (index > 0 && last.Type != TokenType.Section && last.Type != TokenType.Reference && last.Type != TokenType.Template && last.Type != TokenType.Link && last.Type != TokenType.Colon && last.Type != TokenType.Blockquote && !String.IsNullOrWhiteSpace(last.Text) && !last.Text.Substring(0, 1).Equals(";"))
                        {
                            Debug.WriteLine("Unreferenced = '" + last.Text + "' type = " + last.Type);
                            isReferenced = false;
                            break;
                        }
                    }
                }
            }
            Debug.Exited("isReferenced=" + isReferenced);

            return isReferenced;
        }

        private bool coverage(ArticlePage article)
        {
            Debug.Entered();
            bool result = false;
            if (article.Exists("Empty section"))
            {
                Debug.WriteLine("\tEmpty section");
            }
            else
            {
                var bytesCount = article.BytesCount();
                if (bytesCount < 1500)
                {
                    Debug.WriteLine("\tToo short - " + bytesCount + " bytes");
                }
                else
                {
                    var lead = new Lead(article);
                    if (lead.Sentences <= 1)
                    {
                        Debug.WriteLine("\tOne sentence lead");
                    }
                    else
                    {
                        var prediction = article.Prediction();
                        Debug.WriteLine("\tprediction = " + prediction);
                        result = !(prediction.Equals("C") || prediction.Equals("Start") || prediction.Equals("Stub"));
                    }
                }
            }
            Debug.Exited();
            return result;
        }

        private bool structure(ArticlePage article)
        {
            return article.Sections.Count > article.FindAllSections(addenda).Count;
        }

        private bool grammar()
        {
            return true;
        }

        private bool support(ArticlePage article)
        {
            return article.Exists("Infobox") || article.ExistsNs("File");
        }

        public Rating(TalkPage talk)
        {
            Debug.Entered();
            ArticlePage article = talk.Article;
            if (article.IsDisambiguation)
            {
                Debug.WriteLine("   Disambiguation page");
                Class = "Disambig";
            }
            else if (article.IsProject)
            {
                Debug.WriteLine("   Project page");
                Class = "Project";
            }
            else if (article.IsSetIndex)
            {
                Debug.WriteLine("   Set Index page");
                Class = "SIA";
            }
            else if (talk.IsRedirect || article.IsRedirect)
            {
                Debug.WriteLine("   Redirect");
                Class = "Redirect";
            }
            else
            {
                article.Load();
                HasReferences = inlineCitations(article);
                HasCoverage = coverage(article);
                HasStructure = structure(article);
                HasGrammar = grammar();
                HasSupport = support(article);

                string rating = ((HasReferences && HasCoverage) && HasStructure && HasGrammar && HasSupport) ? "B" :
                                ((HasReferences || HasCoverage) && HasStructure && HasGrammar && HasSupport && article.BytesCount() > 1500) ? "C" :
                                article.Prediction().Equals("Stub") ? "Stub" :
                                "Start";

                if (talk.MilHist.ProjectTemplate.IsList)
                {
                    rating = listRating(rating);
                }
                Class = rating;
            }
            Debug.Exited("Class=" + Class);
        }
    }

    public class TaskForces
    {
        private static readonly Dictionary<string, string> dictionary;
        public HashSet<string> TaskForceList { get; private set; }

        public bool Any()
        {
            return TaskForceList.Count > 0;
        }

        public override string ToString()

        {
            return String.Join(", ", TaskForceList.ToList());
        }

        private bool isMaintenance(string category)
        {
            const string maintenance = "CS1 maintenance|Commons category link is on Wikidata|(Hidden|Tracking|Wikipedia) (articles|categories)|CatAutoTOC|Pages with|Template Large category|Use dmy|Use mdy|Category TOC";
            Match match = Regex.Match(category, maintenance, RegexOptions.IgnoreCase);
            return match.Success;
        }

        public TaskForces(ArticlePage article)
        {
            TaskForceList = new HashSet<string>();

            var categories = article.Categories.FindAll(ind => !isMaintenance(ind));
            for (int i = 0; i < 3; i++)
            { 
                // Gets all parent categories as well
                var parents = new List<string>();
                foreach (var category in categories)
                {
                    var parent = new Page(category);
                    parents.AddRange(parent.Categories.FindAll(ind => !isMaintenance(ind)));
                }
                categories.AddRange(parents);

                foreach (var category in categories)
                {
                    Debug.WriteLine(category);
                    foreach (KeyValuePair<string, string> pair in dictionary)
                    {
                        Match match = Regex.Match(category, pair.Key, RegexOptions.IgnoreCase);
                        if (match.Success)
                        {
                            Debug.WriteLine(category);
                            Debug.WriteLine("    Found (" + pair.Value + ") " + category);
                            TaskForceList.Add(pair.Value);
                        }
                    }
                }

                if (TaskForceList.Contains("Muslim") && !TaskForceList.Contains("Medieval"))
                {
                    Debug.WriteLine("    Removing 'Muslim'");
                    TaskForceList.Remove("Muslim");
                }

                if (TaskForceList.Count > 0)
                {
                    break;
                }
            }
        }

        static TaskForces()
        {
            dictionary = new Dictionary<string, string>()
        {
		    // General topics
		    { "Aviation|Aircraft|Aerospace|Air Force", "Aviation" },
            { @"Births|Deaths|People(?!(s|'))", "Biography" },
            { @"($<!Recipients.+)(Awards|Badges|Bands|Customs|Decorations|Emblems|Flags|Holidays|Insignia|Ranks)", "Culture" },
            { "Films", "Films" },
            { "Castle|Fortification|Gatehouse|Tower", "Fortifications" },
            { "History books|Historians", "Historiography" },
            { "Logistics", "Logistics" },
            { "Maritime|Navy|Naval|Ships", "Maritime" },
            { "Memorial", "Memorials" },
            { @"Muslim|Islam(?!(ic (Republic|State)))", "Muslim" },
            { "Formations|Units|Military of", "National" },
            { "Science|Technology|Tactics|Ethics|Strategy|Military (Electronics|Equipment)", "SciTech" },
            { "Ammunition|Armour$|Weapon", "Weaponry" },

		    // nations and regions
		    { "Africa|Ethiopia|Eritrea|Commonwealth War Graves", "African" },
            { "Austria", "European" },
            { "China|Chinese", "Chinese" },
            { @"Russia(?<!Prussia)|Belarus|Ukraine|Armenia|Soviet Union",  "Russian" },
            { "Indian|Commonwealth War Graves",  "Indian" },
            { "Afghan|Pakistan|Nepal|Bangladesh|Sri Lanka|Sikh",  "South-Asian" },
            { "Italy|Italian(?!-language)",  "Italian" },
            { "Japan",  "Japanese" },
            { "Poland|Polish",  "Polish" },
            { "Portugal|Portuguese", "European" },
            { "($<!Wikiproject )Spain|Spanish(?!(-language| Wikipedia))",   "Spanish" },
            { "United States", "US" },
            { "Netherlands|Dutch(?!(-language| Wikipedia))", "Dutch" },
            { "Ottoman|Turkish", "Ottoman" },
            { "France|French(?!.language)", "French" },
            { @"German(?!(-language| Wikipedia))|Prussia", "German" },
            { "Australia|New Zealand|Commonwealth War Graves", "ANZSP" },
            { "Britain|British|Scotland|Scottish|United Kingdom|Commonwealth War Graves", "British" },
            { "Canada|Canadian|Commonwealth War Graves", "Canadian" },
            { "Burma|Indonesia|Cambodia|Laos|Malaysia|Singapore|Thai|Timor|Vietnam|Southeast Asia|South East Asia", "Southeast-Asian" },
            { "Korea", "Korean" },
            { "Swedish|Sweden|Denmark|Danish|Norway|Norweigan|Finland|Finnish|Iceland|Viking", "Nordic" },
            { "Lithuania|Latvia|Estonia|Baltic", "Baltic" },
            { "Iran|Arabia|Arab Emirates|Algeria|Egypt|Israel|Jordan|Syria|Palestin|Middle East|Persia", "Middle-Eastern" },
            { "Albania|Balkan|Bosnia|Bulgaria|Croatia|Greece|Greek|Hungar|Kosovo|Montenegr|Romania|Serbia|Yugoslav", "Balkan" },
            { "Argentina|Bolivia|Brazil|Columbia|Ecuador|Falklands|Malvinas|Paraguay|Uraguay|Venezuala|South America", "South-American" },
            { "Mexic|Central America", "Latin-American" },

		    // Periods and conflicts
		    { "191[4-9] (?!(births|deaths|.*books))|World War I(?!I)", "WWI" },
            { "1939|194[0-5] (?!(births|deaths|.*books))|World War II", "WWII" },
            { "19[5-8]\\d (?!(births|deaths|.*books))|Cold War", "Cold-War" },
            { "(199\\d|20\\d\\d) (?!(births|deaths|.*books))", "Post-Cold-War" },
            { "Crusade", "Crusades" },
            { "Ancient|Antiquity|Classical", "Classical" },
            { "Napoleon", "Napoleonic" },
            { "American Civil War", "ACW" },
            { "Byzantine|($<!Holy )Roman(?!(ia| Catholic))", "Roman" },
            { @"Medieval|:(5|6|7|8|9|1[0-4])th( |-)century|(1[0-4])\d\d", "Medieval" },
            { "1[5-8]th( |-)century", "Early-Modern" },
        };
            // dictionary.TrimExcess ();
        }
    }

    public class ProjectTemplate : ProjectBanner
    {
        private Rating? rating;

        private bool flag(string key)
        {
            if (Exists(key))
            {
                var p = Find(key)!;
                return p.Value.Equals("y") || p.Value.Equals("yes");
            }
            return false;
        }

        public string AClass
        {
            get => Fetch("A-Class", "");
            set => Add("A-Class", value);
        }

        public bool IsList
        {
            get => flag("list") || Page.Title.Matches("List of");
            set => Add("list", "yes");
        }

        public bool HasReferences
        {
            get => flag("b1");
            set => Add("b1", value);
        }

        public bool HasCoverage
        {
            get => flag("b2");
            set => Add("b2", value);
        }

        public bool HasStructure
        {
            get => flag("b3");
            set => Add("b3", value);
        }

        public bool HasGrammar
        {
            get => flag("b4");
            set => Add("b4", value);
        }

        public bool HasSupport
        {
            get => flag("b5");
            set => Add("b5", value);
        }

        public bool Missing()
        {
            return FindAll(@"b\d").Count < 5;
        }

        public void Clear ()
        {
            RemoveAll("^class$|^importance$|^b\\d$|^B-Class-\\d$");
        }

        public Rating Rating
        {
            get
            {
                if (null == rating)
                {
                    rating = new Rating(Page);
                }
                return rating;
            }
            set
            {
                rating = value;
                if (!Class.Matches("Disambig|Project|Redirect|SIA|Stub"))
                {
                    HasReferences = value.HasReferences;
                    HasCoverage = value.HasCoverage;
                    HasStructure = value.HasStructure;
                    HasGrammar = value.HasGrammar;
                    HasSupport = value.HasSupport;
                }
            }
        }

        public void Add(TaskForces taskForces)
        {
            var taskForceList = taskForces.TaskForceList;
            foreach (var taskForce in taskForceList)
            {
                Add(taskForce, "yes");
            }
        }

        public ProjectTemplate(TalkPage page, Tokens tokens) : base(page, tokens)
        {
            Debug.Entered("adding " + this.Name + " to page.MilHist.ProjectTemplate");
            page.MilHist = new MilHist(page);
            page.MilHist.ProjectTemplate = this;
            Debug.Exited();
        }

        public ProjectTemplate(TalkPage page) : base(page, "WikiProject Military history\n")
        {
            Debug.Entered();

            if (page.Empty())
            {
                page.Add(this, new Token("\n"));
            }
            else
            {
                var last = page.FindLast("WikiProject");
                page.Insert((last ?? page.Top()), this, new Token("\n"));
            }
            Debug.Exited();
        }
    }
}