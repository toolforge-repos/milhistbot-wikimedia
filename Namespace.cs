using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia
{
    public class Namespace : IToken
    {
        static private string[] namespaces = new string[]{  "Media", "Special", "Talk", "User", "User talk", "Project", "Project talk", "File", "File talk",
                                            "Image", "Image talk", "MediaWiki", "MediaWiki talk", "Template", "Template talk", "Help", "Help talk",
                                            "Category", "Category talk", "WP", "Wikipedia", "Wikipedia Talk"};

        static private Dictionary<string, string> mainspace2talk;
        static private Dictionary<string, string> talkspace2main;

        public string Text { get; }
        public TokenType Type { get; }

        // This allows the idiom of comparing a namespace against a string
        public override bool Equals(object? obj)
        {
            if (obj is string)
            {
                return Text.Equals(obj);
            }
            return base.Equals(obj);
        }

        // This is required by the compiler
        // If two objects are equal then they must both have the same hash code
        public override int GetHashCode()
        {
             return base.GetHashCode();
        }

        public override string ToString()
        {
            return Text;
        }

        public string Append(string title)
        {
            return title.StartsWith(Text + ":") ? title : Text + ":" + title;
        }

        static public bool IsNamespace(string ns)
        {
            return Array.Exists(namespaces, element => element == ns);
        }

        public bool IsMainspace
        {
            get => mainspace2talk.ContainsKey(Text);
        }

        public bool IsTalkspace
        {
            get => talkspace2main.ContainsKey(Text);
        }

        static public Namespace Parse (string title)
        {
            string ns = "";
            Match match = Regex.Match(title, @"^(?<namespace>.+?):");
            if (match.Success)
            {
                string s = match.Groups["namespace"].Value;
                if (s != null && Wikimedia.Namespace.IsNamespace(s))
                {
                    ns = s;
                }
            }
            return new Namespace (ns);
        }

        public Namespace Mainspace
        {
            get
            {
                if (IsMainspace)
                {
                    return this;
                }
                else if (talkspace2main.ContainsKey(Text))
                {
                    return new Namespace(talkspace2main[Text]);
                }
                else
                {
                    throw new Exception("No main space for namespace " + Text);
                }
            }
        }

        public Namespace Talkspace
        {
            get
            {
                if (IsTalkspace)
                {
                    return this;
                }
                else if (mainspace2talk.ContainsKey(Text))
                {
                    return new Namespace(mainspace2talk[Text]);
                }
                else
                {
                    throw new Exception("No talk space for namespace " + Text);
                }
            }
        }

        static Namespace()
        {
            mainspace2talk = new Dictionary<string, string>()
            {
                { "", "Talk" },
                { "User", "User talk" },
                { "Project", "Project talk" },
                { "File", "File talk" },
                { "Image", "Image talk" },
                { "MediaWiki", "MediaWiki talk" },
                { "Template", "Template talk" },
                { "Help", "Help talk" },
                { "Category", "Category talk" },
                { "WP", "Wikipedia Talk" },
                { "Wikipedia", "Wikipedia Talk" },
            };
            talkspace2main = new Dictionary<string, string>()
            {
                { "Talk", ""  },
                { "User talk", "User"  },
                { "Project talk", "Project" },
                { "File talk", "File" },
                { "Image talk", "Image"  },
                { "MediaWiki talk", "MediaWiki"  },
                { "Template talk", "Template" },
                { "Help talk", "Help"  },
                { "Category talk", "Category"  },
                { "Wikipedia Talk", "Wikipedia"  },
            };
        }

        public Namespace(string name, bool colon = false)
        {
            Text = name;
            Type = TokenType.Namespace;
        }
    }
}