﻿
using System;
using System.Collections.Generic;

namespace Wikimedia.MilHist
{
	public class Newsletter: Page
	{
		// Create as a singleton
		private static readonly Lazy<Newsletter> _instance = new Lazy<Newsletter>(() => new Newsletter());
		public static Newsletter Instance => _instance.Value;

		const string SectionHeader = "WPMILHIST Newsletter section header 2";

        private static string Path
		{
			get
			{
				var month = DateTime.Now.AddMonths(1).ToString("MMMM yyyy");
				var title = MilHist.ProjectPath + "/News/" + month;
				return title;
			}
		}

		public void Add (AClassArticlePage article, List<string> nominatorList)
		{
			var footer = Find(Path + "/Footer");
			if (null == footer)
			{
				throw new Exception("A class section footer template '" + Path + "/Footer' not found");
			}

			int count = 1;
			var user = new Namespace("User");
			Insert(footer, new Token("; "), new Link(article.Title), new Token(" ("));
            foreach (var nominator in nominatorList)
            {
				var link = new Link(user, nominator, nominator);
				Insert(footer, link);
				if (count == nominatorList.Count - 1)
				{
					Insert(footer, " and ");
				}
				else if (count < nominatorList.Count)
				{
					Insert(footer, ", ");
				}
				else if (count == nominatorList.Count)
				{
                    Insert(footer, ")\n\n");
                }
				++count;
            }

        }
        
		private Newsletter(): base (Path + "/Articles")
		{
			try
			{
				Load();
            }
            catch (PageMissingException)
			{
                var header = new Template(this, Path + "/Header");
				Add(header);
				Add(" __NOTOC__\n\n");
				string[] subheadings = {
					"New featured articles",
					"New featured lists",
					"New featured topics",
					"New featured pictures",
					"New featured portals",
					"New A-Class articles",
				};
				foreach (var subheading in subheadings)
				{
					var section = new Template(this, SectionHeader);
					section.Add(1, subheading);
					Add(section, Token.Newline);
                }
				var footer = new Template(this, Path + "/Footer");
				Add(footer, Token.Newline, Token.Newline);
            }
		}
	}
}
