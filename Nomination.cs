using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Wikimedia
{
	public class NominationPage : WikipediaPage
	{
		private ArticlePage? article;
		private TalkPage? articleTalk;
		private NominationsPage? nominations;

        private List<string>? nominators;
		private Page? archives;
        private DateTime? date;
        private Revision? revision;

        public string Action { get; set; }
        public string CloseTemplateName { get; set; }
        public string TitleMatch { get; set; }

        public Revision Closed
        {
            get
            {
                if (null == revision)
                {
                    var history = History();
                    Revision old = history.First();
                    foreach (var entry in history)
                    {
                        var page = new NominationPage(Title, entry.RevId);
                        if (null == page.Find(CloseTemplateName))
                        {
                            break;
                        }
                        old = entry;
                    }
                    revision = old;
                }
                return (Revision)revision;
            }
        }

        public DateTime Date
        {
            get
            {
                if (null == date)
                {
                    date = Closed.Timestamp.Date;
                }
                return (DateTime)date;
            }
        }

        public string Diff
        {
            get
            {
                return "https://en.wikipedia.org/w/index.php?title=" + Title + "&diff=" + Closed.RevId + "&oldid=" + Closed.RevId;
            }
        }

        public List<string> Nominators
        {
            get
            {
                if (null == nominators)
                {
                    nominators = new List<string>();
                    IToken? text = FindText(@"Nominator");
                    if (null == text)
                    {
                        throw new Exception("unable to find nominators in " + Title);
                    }
                    List<IToken> line = GetLine(text);
                    foreach (var token in line)
                    {
                        if (token is Link)
                        {
                            Link link = (Link)token;
                            if (link.Namespace.Equals("User"))
                            {
                                nominators.Add(link.Data);
                            }
                        }
                        if (token is Template)
                        {
                            Template template = (Template)token;
                            if (template.Name.Equals("^(u|user0)$"))
                            {
                                var nominator = template["1"];
                                if (null != nominator)
                                {
                                    nominators.Add(nominator);
                                }
                            }
                        }
                    }
                }
                return (List<string>)nominators;
            }
        }

        protected string GetArticleTitle()
        {
			string title;
            Regex filter = new Regex(TitleMatch);
            var match = filter.Match(Title);
            if (match.Success)
            {
				title = match.Groups["title"].Value;
            }
            else
            {
                throw new Exception("Bad nomination name: '" + Title + "'");
            }
            return title;
        }

        public ArticlePage Article
        {
            get
            {
                if (null == article)
                {
					var title = GetArticleTitle();
                    article = new ArticlePage(title);
				}
                return article;
            }
            set
            {
                article = value; 
            }
        }

        public TalkPage ArticleTalk
        {
            get
            {
                if (null == articleTalk)
                {
                    var title = GetArticleTitle();
                    articleTalk = new TalkPage("Talk:" + title);
                }
                return articleTalk;
            }
            set
            {
                articleTalk = value;
            }
        }

        public NominationsPage Nominations
		{
            get
            {
                if (null == nominations)
                {
					throw new Exception("nominations page is not set");
                }
                return nominations;
            }
			set => nominations = value;
        }

		public Page Archives
		{
            get
            {
                if (null == archives)
                {
                    throw new Exception("archive page is not set");
                }
                return archives;
            }
            set => archives = value;
        }

        public void Archive(string comment)
		{
			Load();
			Template archiveTop = new Template("article top");
			Insert(Top(), archiveTop, Token.Newline, new Token(comment), Token.Newline);
			Template archiveBottom = new Template("archive bottom");
			Add(archiveBottom, Token.Newline);
		}

		// Name of nomination, not article
		public NominationPage(string name, int revid = -1): base(name, revid)
		{
			Action = "";
            TitleMatch = "";
            CloseTemplateName = "";
        }
	}

	public class NominationsPage : WikipediaPage
	{
        public string TitleMatch { get; set; }

        public bool UpdateDailyMarkerRequired()
        {
            Debug.Entered();
            var user = Cred.Instance.User;
            var query = new Query(title: Title, limit: 5, user: user); 
            var history = Bot.History(query);

            var revision = (from r in history
                            where r.Comment.Equals("update daily marker")
                            select r).First();

            TimeSpan ts = DateTime.Now.Subtract(revision.Timestamp.Date);
            bool required = (ts.TotalDays >= 1);
            Debug.Exited("days=" + ts.TotalDays + " required=" + required);
            return required;
        }

        public void UpdateDailyMarker()
        {
            Debug.Entered();
            var currentNominationsSection = FetchSection("Nominations");
            var olderNominationsSection = FetchSection("Older nominations");
            var currentNominations = Nominations(currentNominationsSection);

            IEnumerable<NominationPage> query = from nomination in currentNominations
                                            where DateTime.Now.Subtract(nomination.Created.Timestamp).TotalDays > 20
                                            orderby nomination.Created.Timestamp descending
                                            select nomination;

            foreach (var nomination in query)
            {
                var template = new Template(this, nomination.Title);
                currentNominationsSection.Remove(template);
                olderNominationsSection.Insert(template, Token.Newline);
            }
            Debug.Exited();
        }

        private List<NominationPage> Nominations(Section section)
        {
            var nominations = new List<NominationPage>();
            var templates = section.Templates();
            foreach (var template in templates)
            {
                nominations.Add(new NominationPage(template.Name));
            }
            return nominations;
        }

        public List<NominationPage> Nominations()
		{
            var nominations = new List<NominationPage>();
			{
				if (!nominations.Any())
				{
					Load();
					List<Template>? nominationTemplates = FindAll(TitleMatch);
					foreach (Template template in nominationTemplates)
					{
						NominationPage nominationPage = new NominationPage(template.Name);
						nominations.Add(nominationPage);
					}
				}
				nominations.Reverse();
				return nominations;
			}
        }

        public void Remove(NominationPage nomination)
		{
			Load();
			var template = Find(nomination.Title);
            if (null == template)
            {
                throw new Exception("Cannot find template " + nomination.Title + " on page " + Title);
            }
 			Remove(template);
		}

		public NominationsPage(string title) : base(title)
		{
            TitleMatch = "";
        }
	}
}

