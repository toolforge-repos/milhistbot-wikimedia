﻿using System;
namespace Wikimedia.Featured
{
	public class NotifyTemplate : Template
	{
		public NotifyTemplate(string user, FeaturedNominationPage nomination) : base(new Page("User talk:" + user), "subst:FC pass talk message")
		{
			this["page"] = nomination.Article.Title;
			this["user"] = user;
			this["discussion"] = nomination.Title;
			this["type"] = nomination.ContentType.ToString();
			var posterTemplate = new UserTemplate(Page, nomination.Coordinator);
			this["poster"] = posterTemplate + " via ~~~~";
		}

		public NotifyTemplate(string user, FeaturedListNomination nomination) : base(new Page("User talk:" + user), "subst:FC pass talk message")
		{
			this["page"] = nomination.Article.Title;
			this["user"] = user;
			this["discussion"] = nomination.Title;
			this["type"] = nomination.ContentType.ToString();
			var posterTemplate = new UserTemplate(Page, nomination.Coordinator);
			this["poster"] = posterTemplate + " via ~~~~";
		}
	}
}
