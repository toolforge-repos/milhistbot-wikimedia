using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Nowiki : IToken
    {
        public TokenType Type { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        
        private readonly Tokens contents;

        public override string ToString()
        {
            return Text;
        }

        private void Parse(Page page, Tokens tokens)
        {
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                switch (token.Type)
                {
                    case TokenType.CloseNowiki:
                         return;
                    default:
                        contents.Add(token);
                        break;
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed <nowiki> on " + page.Title + ": " + contents.Preview());
        }

        public Nowiki(Page page, Tokens tokens)
        {
            contents = new Tokens();
            Parse(page, tokens);
            Value = contents.Text;
            Text = "<nowiki>" + Value + "</nowiki>";
            Type = TokenType.Nowiki;
        }
    }
}
