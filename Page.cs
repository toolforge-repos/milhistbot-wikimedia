using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using static System.Net.WebRequestMethods;

namespace Wikimedia
{
    public interface IPageElement
    {
        void Added(Page page);
        void Removed(Page page);
    }

    public class ParseException : Exception
    {
        public ParseException(string message) : base(message)
        {
        }
    }

    public class Page
    {
        public Bot Bot { get; set; }
        public int Ns { get; set; }
        public int PageId { get; set; }
        public bool ReadOnly { get; set; }
        public int RevId { get; set; }
        public int OldRevId { get; set; }
        public List<Link> Links { get; set; }
        public List<Reference> References { get; set; }
        public List<Section> Sections { get; set; }
        public List<Template> Templates { get; set; }
        public Query? Query { get; set; }

        private List<string>? categories;
        private Namespace? _namespace;
        private Tokens _tokens;
        private string? prediction;
        private bool checkedRedirect;
        private Revision? created;
        private Page? redirectsTo;
        private bool loaded;

        public List<string> Categories
        {
            get
            {
                if (null == categories)
                {
                    categories = Bot.Categories(Title);
                }
                return categories;
            }
        }

        public string Content
        {
            get => _tokens.Text;
            set => Parse(value);
        }

        public Tokens Contents
        {
            get => _tokens;
        }

        public Revision Created
        {
            get
            {
                if (null == created)
                {
                    Query query = new Query(title: Title, limit:1, direction: "newer");
                    created = Bot.History(query).First();
                }
                return (Revision)created;
            }
        }

        public bool IsDisambiguation
        {
            get => InCategory("disambiguation") && !InCategory("links needing disambiguation");
        }

        public bool IsProject
        {
            get => Namespace.Text.Equals("Wikipedia");
         }

        public bool IsRedirect
        {
            get => null != RedirectsTo;
        }

        public bool IsSetIndex
        {
            get => InCategory("Set index articles");
        }

        public Namespace Namespace
        {
            get
            {
                if (null == _namespace)
                {
                    _namespace = Namespace.Parse(Title);
                }
                return _namespace;
            }
            set => _namespace = value;
        }

        public Page? RedirectsTo
        {
            get
            {
                if (!checkedRedirect)
                {
                    redirectsTo = Bot.RedirectsTo(Title);
                    checkedRedirect = true;
                }
                return redirectsTo;
            }
        }

        public string Title { get; set; }
 
        public string TitleWithoutNamespace
        {
            get => Namespace.Text.Equals("") ? Title : Title.Substring(Namespace.Text.Length + 1);
        }

        public override string ToString()
        {
            return Content;
        }

        private void Parse(string content)
        {
            Debug.Entered("Page=" + Title);
            _tokens = new Tokens();
            Tokens tokens = new Tokens(content);
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            _tokens.Add(new Comment(this, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            _tokens.Add(new Nowiki(this, tokens));
                            break;
                        case TokenType.OpenBracket:
                            _tokens.Add(new Link(this, tokens));
                            break;
                        case TokenType.OpenExternalLink:
                            _tokens.Add(new ExternalLink(this, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            _tokens.Add(TemplateFactory.GetTemplate(this, tokens));
                            break;
                        case TokenType.OpenBlockquote:
                            _tokens.Add(new Blockquote(this, tokens));
                            break;
                        case TokenType.Reference:
                            _tokens.Add(new Reference(this, token));
                            break;
                        case TokenType.OpenReference:
                            _tokens.Add(new Reference(this, token, tokens));
                            break;
                        case TokenType.OpenTable:
                            _tokens.Add(new Table(this, tokens));
                            break;
                        case TokenType.OpenGallery:
                            _tokens.Add(new Gallery(this, token, tokens));
                            break;
                        case TokenType.MultiEquals:
                            _tokens.Add(new Section(this, tokens));
                            break;
                        default:
                            _tokens.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    _tokens.Add(token);
                }
            }
            Debug.Exited();
        }

        public void Add(params IToken[] items)
        {
            _tokens.Contents.AddRange(items);
            Added(items);
        }

        public void Add(params string[] strings)
        {
            foreach (var s in strings)
            {
                Add(new Tokens(s));
            }
        }

        public void AddCategory(string name)
        {
            var ns = new Namespace("Category");
            var category = new Link(ns, name);
            Add(Token.Newline, category);
        }

        public void Added(params IToken[] items)
        {
            foreach (IToken item in items)
            {
                if (item is IPageElement)
                {
                    var pageElement = item as IPageElement;
                    pageElement!.Added(this);
                }
            }
        }

        public void Append(IToken token, params IToken[] items)
        {
            Contents.Append(token, items);
            Added(items);
        }

        public bool Empty()
        {
            return Contents.Empty();
        }

        public bool Exists(string match)
        {
            return Templates.Exists(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Template? Find(string match)
        {
            return Templates.Find(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Template? FindLast(string match)
        {
            return Templates.FindLast(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public List<Template> FindAll(string match)
        {
            return Templates.FindAll(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Comment? FindComment(string match)
        {
            return _tokens.Contents.Find(ind => (ind is Comment comment && Regex.IsMatch(comment.Value, match, RegexOptions.IgnoreCase))) as Comment;
        }

        public bool ExistsNs(string ns)
        {
            return _tokens.Contents.Exists(ind => (ind.Type == TokenType.Link && ns.Equals(((Link)ind).Namespace.Text)));
        }

        public List<Link> FindAllNs(string ns)
        {
            var tokenList = _tokens.Contents.FindAll(x => (x.Type == TokenType.Link && ns.Equals(((Link)x).Namespace.Text)));
            return tokenList.ConvertAll(x => (Link)x);
        }

        public Section? FindSection(string match)
        {
            return Sections.Find(ind => (Regex.IsMatch(ind.Title, match, RegexOptions.IgnoreCase)));
        }

        public List<Section> FindAllSections(string match)
        {
            return Sections.FindAll(ind => (Regex.IsMatch(ind.Title, match, RegexOptions.IgnoreCase)));
        }

        public IToken? FindText(string match)
        {
            return _tokens.Contents.Find(ind => ind.Type == TokenType.Text && Regex.IsMatch(ind.Text, match, RegexOptions.IgnoreCase));
        }

        public List<IToken> FindAllText(string match)
        {
            return _tokens.Contents.FindAll(ind => (ind.Type == TokenType.Text && Regex.IsMatch(ind.Text, match, RegexOptions.IgnoreCase)));
        }

        public int FindIndex(IToken token)
        {
            return Contents.FindIndex(token);
        }

        public int FindLastIndex()
        {
            return _tokens.Contents.Count - 1;
        }

        public Link? FindLink(string match)
        {
            return Links.Find(ind => (Regex.IsMatch(ind.Data, match, RegexOptions.IgnoreCase)));
        }
 
        public Template Fetch(String name)
        {
            var template = Find(name);
            if (null == template)
            { 
                throw new Exception("Could not find '" + name + "' template on page '" +Title +"'");
            }
            return template;
        }

        public Section FetchSection(string match)
        {
            var section = FindSection(match);
            if (null == section)
            {
                throw new Exception("unable to find '" + match + "' section");
            }
            return section;
        }

        public List<IToken> GetRange(int index, int count)
        {
            return Contents.GetRange(index, count);
        }

        public IToken GetToken(int index)
        {
            return Contents.GetToken(index);
        }

        public void Insert(int index, params IToken[] items)
        {
            Contents.InsertRange(index, items);
            Added(items);
        }

        public void Insert(IToken token, params IToken[] items)
        {
            Contents.InsertRange(token, items);
            Added(items);
        }

        public void Insert(IToken token, params string[] strings)
        {
            var items = new List<IToken>();
            foreach (var s in strings)
            {
                items.Add(new Token(s));
            }
            Insert(token, items.ToArray());
        }

        public void Remove(Template template)
        {
            Contents.Trim(template);
            Contents.Remove(template);
            Removed(template);
        }

        public void RemoveRange(int index, int count)
        {
            List<IToken> items = Contents.RemoveRange(index, count);
            Removed(items.ToArray());
        }

        public void Removed(params IToken[] items)
        {
            foreach (IToken item in items)
            {
                if (item is IPageElement)
                {
                    var pageElement = item as IPageElement;
                    pageElement!.Removed(this);
                }
            }
        }

        public void Rename(string newTitle, string reason)
        {
            Bot.Move(Title, newTitle, reason);
            Title = newTitle;
        }

        public void Replace(Template template1, Template template2)
        {
            Append(template1, template2);
            Remove(template1);
        }

        public void Replace(string pattern, string replacement)
        {
            Regex regex = new Regex(pattern, RegexOptions.Multiline);
            Content = regex.Replace(Content, replacement);
        }

        public IToken Top()
        {
            return Contents.Peek(0);
        }

        public IToken EndOfLine(IToken token)
        {
            return Contents.EndOfLine(token);
        }

        public int EndOfLineIndex(IToken token)
        {
            return Contents.EndOfLineIndex(token);
        }

        public IToken StartOfLine(IToken token)
        {
            return Contents.StartOfLine(token);
        }

        public int StartOfLineIndex(IToken token)
        {
            return Contents.StartOfLineIndex(token);
        }

        public List<IToken> GetLine(IToken token)
        {
            return Contents.GetLine(token);
        }

        public List<IToken> RemoveLine(IToken token)
        {
            var line = Contents.RemoveLine(token);
            Removed(line.ToArray());
            return line;
        }

        public bool InCategory(string match)
        {
            return Categories.Exists(ind => Regex.IsMatch(ind, match, RegexOptions.IgnoreCase));
        }

        public void LoadFromStrings(string content)
        {
            Content = content;
            loaded = true;
        }

        public void LoadFromFile(string path)
        {
            Content = System.IO.File.ReadAllText(path, Encoding.UTF8);
            loaded = true;
        }

        public void SaveToFile(string path)
        {
            System.IO.File.WriteAllText(path, Content);
        }

        public void Load()
        {
            if (!loaded)
            {
                Bot.Load(this, RevId);
                loaded = true;
            }
        }

        public void Save(string summary)
        {
            Bot.Save(this, summary);
        }

        public void Save(Section section, string summary)
        {
            Bot.Save(section, summary);
        }

        public string Prediction()
        {
            if (null == prediction)
            {
                prediction = Bot.Prediction(Title);
            }
            return prediction;
        }

        public List<Revision> History()
        {
            if (null == Query)
            {
                Query = new Query(Title);
            }
            return Bot.History(Query);
        }

        public List<Revision> History(int limit)
        {
            if (null == Query)
            {
                Query = new Query(Title, limit);
            }
            return Bot.History(Query);
        }

       public List<Page> Redirects()
        {
            var query = new Query(Title);
            var redirects = new List<Page>();
            do
            {
                List<Page> batch = Bot.Redirects(query);
                redirects.AddRange(batch);
            } while (query.Continue);
            return redirects;
        }

        public int BytesCount()
        {
            int count = 0;
            foreach (var token in _tokens.Contents)
            {
                switch (token.Type)
                {
                    case TokenType.Comment:
                    case TokenType.ExternalLink:
                    case TokenType.Reference:
                    case TokenType.Section:
                    case TokenType.Table:
                    case TokenType.Template:
                        break;
                    case TokenType.Link:
                        Link link = (Link)token;
                        if (link.Namespace.Text.Equals(""))
                        {
                            count += link.Value.Length;
                        }
                        break;
                    default:
                        if (String.IsNullOrWhiteSpace(token.Text))
                            break;
                        count += token.Text.Length;
                        break;
                }
            }
            return count;
        }

        public int WordsCount()
        {
            int count = 0;
            foreach (var token in _tokens.Contents)
            {
                switch (token.Type)
                {
                    case TokenType.Comment:
                    case TokenType.ExternalLink:
                    case TokenType.Reference:
                    case TokenType.Section:
                    case TokenType.Table:
                    case TokenType.Template:
                        break;
                    case TokenType.Link:
                        Link link = (Link)token;
                        if (link.Namespace.Text.Equals(""))
                        {
                            count += link.Value.WordsCount();
                        }
                        break;
                    default:
                        count += token.Text.WordsCount();
                        break;
                }
            }
            return count;
        }

        public Page(string title, int revid = -1)
        {
            Bot = Bot.Instance;
            RevId = revid;
             Title = title;
            _tokens = new Tokens();
            Links = new List<Link>();
            References = new List<Reference>();
            Sections = new List<Section>();
            Templates = new List<Template>();
        }
    }
}
