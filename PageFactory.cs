using System;
namespace Wikimedia
{
	public class PageFactory
	{
        public static Page GetPage(string title)
		{
			Page page;
			var ns = Namespace.Parse(title);
			switch (ns.Text)
            {
                case "":
					page = new ArticlePage(title);
					break;
				case "Talk":
					page = new TalkPage(title);
					break;
                case "File":
                    page = new FilePage(title);
                    break;
                case "File talk":
                    page = new FileTalkPage(title);
                    break;
                case "Template":
					page = new TemplatePage(title);
					break;
				case "Template talk":
					page = new TemplateTalkPage(title);
					break;
                case "Wikipedia":
                    page = new WikipediaPage(title);
                    break;
                case "Wikipedia talk":
                    page = new WikipediaTalkPage(title);
                    break;
                default:
					page = new Page(title);
					break;
            }

			return page;
		}

		public PageFactory()
		{
		}
	}
}

