using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Microsoft.VisualBasic;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Wikimedia
{
    public class Parameter : IToken
    {
        public string Text { get { return ToString(); } }
        public TokenType Type { get; private set; }

        public string Name { get; private set; }
        public string Value { get; private set; }
        public bool IsDefault { get; set; }
        public bool IsSpaced { get; set; }

        private Tokens nameTokens;
        private Tokens valueTokens;
        private readonly bool IsTransclusionParameter;
        private readonly string text;

        public Tokens NameTokens
        {
            get => nameTokens;
            set
            {
                nameTokens = value;
                Name = removeComments(nameTokens);
            }
        }

        public Tokens ValueTokens
        {
            get => valueTokens;
            set
            {
                valueTokens = value;
                Value = removeComments(valueTokens);
            }
        }

        private string removeComments(Tokens input)
        {
            var contents = input.Contents.FindAll(ind => ind.Type != TokenType.Comment);
            var output = new Tokens(contents);
            return output.Text.Trim();
        }

        public override string ToString()
        {
            return IsTransclusionParameter ? text :
                IsDefault ? ValueTokens.Text :
                nameTokens.Text + (IsSpaced ? " = " : "=") + ValueTokens.Text;
        }

        private static Tokens ParameterName(Tokens queue)
        {
            var tokens = new Tokens();
            while (queue.Any() && !queue.IsToken(TokenType.Colon) && !queue.IsToken(TokenType.Pipe))
            {
                tokens.Add(queue.Dequeue());
            }
            return tokens;
        }

        public Parameter(Tokens nameTokens, Tokens valueTokens)
        {
            Type = TokenType.Parameter;
            this.nameTokens = nameTokens;
            this.valueTokens = valueTokens;
            ValueTokens = valueTokens;
            NameTokens = nameTokens;
            Name = removeComments(nameTokens);
            Value = removeComments(valueTokens);
            IsDefault = false;
            IsSpaced = false;
            IsTransclusionParameter = false;
            text = "";
        }

        public Parameter(string name, string value) : this(new Tokens(name), new Tokens(value))
        {
        }

        public Parameter(int id, Tokens valueTokens) : this(new Tokens(id.ToString()), valueTokens)
        {
            IsDefault = true;
        }

        public Parameter(int id, string value) : this(id, new Tokens(value))
        {
        }

        public Parameter(Page page, Tokens tokens)
        {
            Tokens contents = new Tokens();
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            contents.Add(new Template(page, tokens));
                            break;
                        case TokenType.Reference:
                            contents.Add(new Reference(page, token));
                            break;
                        case TokenType.OpenReference:
                            contents.Add(new Reference(page, token, tokens));
                            break;
                        case TokenType.CloseParameter:
                            Tokens queue = tokens.Clone();
                            nameTokens = ParameterName(queue);
                            Name = removeComments(nameTokens);
                            valueTokens = queue;
                            Value = queue.ToString();
                            IsTransclusionParameter = true;
                            text = "{{{" + contents.Text + "}}}";
                            Type = TokenType.Parameter;
                            return;
                        default:
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed parameter on " + page.Title + ": " + contents.Preview());
        }
    }
}
