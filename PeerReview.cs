using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wikimedia
{
	public class PeerReviewNominationsPage : NominationsPage
	{
		// Create as a singleton
        private static readonly Lazy<PeerReviewNominationsPage> _instance = new Lazy<PeerReviewNominationsPage>(() => new PeerReviewNominationsPage());
        public static PeerReviewNominationsPage Instance => _instance.Value;

		private readonly List<PeerReviewPage> nominations;

		public new List<PeerReviewPage> Nominations
		{
			get
			{
				if (!nominations.Any())
				{
                    var query = new Query("Current peer reviews", 100);
					do
					{
						var pages = query.Pages;
						foreach (Page page in pages)
						{
							if (page.Title.Matches(TitleMatch))
							{
								var nominationPage = new PeerReviewPage(page.Title);
								nominations.Add(nominationPage);
							}
						}
					}
					while (query.Continue);
				}
				return nominations;
			}
		}

		private PeerReviewNominationsPage() : base("Wikipedia:Peer review")
        {
			nominations = new List<PeerReviewPage>();
			TitleMatch = @"Wikipedia:Peer review/(?<title>.+)/archive\d+";
		}
	}

	public class PeerReviewPage: NominationPage
    {
        public PeerReviewPage(string title) : base(title)
        {
            Action = "PR";
            Nominations = new NominationsPage("");
            TitleMatch = Nominations.TitleMatch;
        }
    }
}
