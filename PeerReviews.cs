using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public class PeerReviews
	{
		private PeerReviews()
		{
			const string category = "Old requests for peer review";
			Bot bot = Bot.Instance;
			var query = new Query(category, 100);
			int count = 1;

			do
			{
				var pages = bot.Category(query);
				foreach (var page in pages)
				{
					if (page is TalkPage)
					{
						TalkPage talk = (TalkPage)page;
						talk.Load();

						var templates = talk.Templates.FindAll(ind => ind is OldPeerReview);
						foreach (var template in templates)
						{
							if (null != talk.ArticleHistory)
							{
								// Console.WriteLine (page.Title);
								talk.ArticleHistory.Add(template);
								talk.ArticleHistory.Sort();
								talk.Remove(template);
								talk.Save("Merge old peer review into article history");

								Template diff = new Template("Diff");
								diff.Add("title", talk.Title);
								diff.Add("oldid", talk.OldRevId.ToString());
								diff.Add("diff", talk.RevId.ToString());
								diff.Add("label", count.ToString());
								// Console.WriteLine(diff);
								count++;
							}
						}
					}
				}
			}
			while (query.Continue);
		}
	}
}
