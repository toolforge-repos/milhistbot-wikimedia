using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class ProjectBanner: Template
	{
        new public TalkPage Page;
        private ProjectBannerShell? shell;

        private static Dictionary<string, bool> templateCache;

        public static bool isProjectBanner(TalkPage page, string name)
        {
            Debug.Entered("template=" +name);
            bool isProjectTemplate;
            if (templateCache.ContainsKey(name))
            {
                Debug.WriteLine("in cache");
                isProjectTemplate = templateCache[name];
            }
            else
            {
                Debug.WriteLine("added to cache");
                Page templatePage = new TemplatePage("Template:" + name);
                isProjectTemplate = templatePage.InCategory("WikiProject banners with quality assessment");
                templateCache[name] = isProjectTemplate;
            }
            Debug.Exited(isProjectTemplate ? "true" : "false");
            return isProjectTemplate;
        }

        public static string FirstCharToUpper(string input)
        {
            return String.IsNullOrEmpty(input) ? "" : input.Substring(0, 1).ToUpper() + input.Substring(1).ToLower();
        }

        public string Class
        {
            get
            {
                if (null == shell)
                {
                    return FirstCharToUpper(Fetch("^class$", ""));
                }
                else
                {
                    return shell.Class;
                }
            }
            set
            {
                if (null == shell)
                {
                    Add("class", value);
                }
                else
                {
                    shell.Class = value;
                }
            }
        }

        static ProjectBanner ()
		{
            templateCache = new Dictionary<string, bool>();
        }

        public ProjectBanner(TalkPage page, Tokens tokens) : base(page, tokens)
        {
            Page = page;
            shell = ProjectBannerShell.GetProjectBannerShell(Page);
        }

        public ProjectBanner(TalkPage page, string name) : this(page , new Tokens(name))
        {
        }
    }
}