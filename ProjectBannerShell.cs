﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Wikimedia
{
	public class ProjectBannerShell : ProjectBanner
	{
        public static bool isProjectBannerShell(TalkPage page, string name)
        {
            Debug.Entered("template=" + name);
            bool isProjectTemplate= name.Matches(@"WikiProject banner shell|WikiProjectBannerShell|WPBS");
            Debug.Exited(isProjectTemplate ? "true" : "false");
            return isProjectTemplate;
        }

        public static ProjectBannerShell? GetProjectBannerShell(TalkPage talk)
        {
            ProjectBannerShell? shell = talk.Templates.Find(ind => ind is ProjectBannerShell) as ProjectBannerShell;
            return shell;
        }

        public void Add(ProjectBanner banner)
        {
            Parameter? parameter = Find("1");
            if (null == parameter)
            {
                var contents = new List<IToken>();
                contents.Add(banner);
                Tokens tokens = new Tokens(contents);
                Add(new Parameter(1, tokens));
            }
            else
            {
                parameter.ValueTokens.Add(Token.Newline);
                parameter.ValueTokens.Add(banner);
            }
            banner.Owner = this; 
        }

        public void AddAll ()
        {
            List<Template> templates = Page.Templates.FindAll(ind => ind is ProjectBanner);
            foreach (var template in templates)
            {
                ProjectBanner banner = (ProjectBanner)template;
                if (banner.Owner != this)
                {
                    Page.Remove(banner);
                    Add(banner);
                }
            }
        }

        public new string Class
        {
            get => FirstCharToUpper(Fetch("^class$", ""));
            set => Add("class", value);
        }

        public ProjectBannerShell(TalkPage page, Tokens tokens): base (page, tokens)
		{
		}

        public ProjectBannerShell(TalkPage page): base(page, "WikiProject banner shell")
        {
        }

    }
}

