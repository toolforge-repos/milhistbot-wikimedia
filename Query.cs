using System.Collections.Generic;

namespace Wikimedia
    {
    public class Query
    {
        public string Title { get; set; }
        public string Limit { get; set; }
        public string? Start { get; set; }
        public string? End { get; set; }
        public string Direction { get; set; }
        public Namespace Namespace { get; set; }
        public string? User { get; set; }
        public bool Continue { get; set; }

        public List<Page> Pages { get => Bot.Instance.Category(this); }

        public Query(string title, int limit = 10, string? start = null, string? end = null, string direction = "older", string ns = "", string? user = null)
        {
            Title = title;
            Limit = limit.ToString();
            Start = start;
            End = end;
            Direction = direction;
            Namespace = new Namespace(ns);
            User = user;
            Continue = false;
        }
    }   
}
