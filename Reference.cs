using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;

namespace Wikimedia
{
    public class Reference : IToken, IPageElement
    {
        public TokenType Type { get; set; }
        public string Text { get; set; }
        public string Name { get; private set; }
        public Page Page { get; private set; }
        public Tokens Contents { get; set; }

        private Regex regex = new Regex(@"<ref name=(?<name>\w+)", RegexOptions.Compiled);

        public override string ToString()
        {
            return Text;
        }

        public void Added(Page page)
        {
            Page = page;
            Page.References.Add(this);
        }

        public void Removed(Page page)
        {
            page.References.Remove(this);
        }

        private void parse()
        {
            Debug.Entered("text=>'" + Text + "'", "Reference::parse");
            try
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(Text.Replace("&", "&amp;"));
                XmlNode? node = document.FirstChild;
                if (null == node)
                {
                    // Console.WriteLine("Unable to find FirstChild");
                    throw new XmlException("Unable to find FirstChild");
                }
                XmlElement element = (XmlElement) node;
                XmlAttribute? attribute = element.GetAttributeNode("name");
                Name = (null == attribute) ? "" : attribute.InnerXml;
            }
            catch (XmlException)
            {
                MatchCollection matches = regex.Matches(Text);
                foreach (Match match in matches)
                {
                    GroupCollection groups = match.Groups;
                    Name = groups["name"].Value;
                }
            }
            Debug.Exited("name=>'" + Name + "'", "Reference::parse");
        }


        public Reference(Page page, IToken open, Tokens tokens) : this (page, "")
        {
            Tokens contents = new Tokens();
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            contents.Add(new Template(page, tokens));
                            break;
                        case TokenType.CloseReference:
                            Text = open.Text + contents.Text + token.Text;
                            parse();
                            Contents = contents;
                            return;
                        default:
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed reference on " + page.Title + ": " + contents.Preview());
        }

        public Reference(Page page, IToken token) : this (page, token.Text)
        {
            parse();
        }

        public Reference(Page page, string text)
        {
            Page = page;
            Type = TokenType.Reference;
            Text = text;
            Name = "";
            Contents = new Tokens();
            Added(page);
        }
    }
}