using System;

public struct Revision
{
    public int RevId        { get; set; }    
    public int ParentId     { get; set; }
    public string Title     { get; set; }
    public string Comment   { get; set; }
    public DateTime Timestamp { get; set; }
    public string User      { get; set; }

    public override string ToString ()
    {
        return "Timestamp=" + Timestamp + " User=" + User + " Title=" + Title + " Comment=" + Comment + " RevId=" + RevId; 
    }
}
