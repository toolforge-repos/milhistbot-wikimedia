using System;
using System.Collections.Generic;

namespace Wikimedia
{
    public class Section : IToken, IPageElement
    {
        public Page Page { get; set; }
        public TokenType Type { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public int Level { get; set; }
        public string Number { get; set; }
        public Tokens Contents { get; set; }

        public override string ToString()
        {
            return Text;
        }

        public void Add(params IToken[] items)
        {
            Contents.Contents.AddRange(items);
        }

        public void Add(string s)
        {
            Add(new Tokens(s));
        }

        public void Add(Section section)
        {
            Add(section.Contents);
        }

        public void Added(Page page)
        {
            Page = page;
            Page.Sections.Add(this);
        }

        public void Insert(params IToken [] items)
        {
            Contents.Contents.InsertRange(0, items);
        }

        public void Remove(IToken item)
        {
            Contents.Contents.Remove(item);
        }

        public void Removed(Page page)
        {
            page.Sections.Remove(this);
        }

        public void Save(string summary)
        {
            Page.Bot.Save(this, summary);
            if (Number.Equals("new"))
            {
                int number = Page.Sections.Count - 1;
                Number = number.ToString();
            }
        }

        public List<Template> Templates ()
        {
            var templates = new List<Template>();
            foreach (var token in Contents.Contents)
            {
                if (token is Template)
                {
                    templates.Add((Template)token);
                }
            }
            return templates;
        }

        public Section(Page page, Tokens tokens)
        {
            Page = page;
            Text = "";
            Title = "";
            Number = "";
            Contents = new Tokens();
            Tokens contents = new Tokens();
            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                switch (token.Type)
                {
                    case TokenType.MultiEquals:
                        Title = contents.Text.Trim();
                        Level = token.Text.Length;
                        Text = token.Text + contents.Text + token.Text;
                        Contents = contents;
                        Type = TokenType.Section;
                        Number = page.Sections.Count.ToString();
                        Added(page);
                        return;
                    default:
                        contents.Add(token);
                        break;
                }
            }
        }

        public Section(Page page, string heading, int level)
        {
            const string s = "========";
            Page = page;
            Title = heading;
            Level = level;
            Text = s.Substring(0, level) + " " + heading + " " + s.Substring(0, level);
            Type = TokenType.Section;
            Contents = new Tokens();
            Number = "new";
            Added(page);
        }

        public Section(Page page, string heading) : this(page, heading, 2)
        {
        }
    }
}