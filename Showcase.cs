using System;
using System.Collections.Generic;

namespace Wikimedia.MilHist
{
    public class Showcase : WikipediaPage
    {
        private const string showcasePageTitle = MilHist.ProjectPath + "/Showcase";


        private (MagicTemplate, int) FindContents (IToken token)
        {
            foreach (var template in Templates)
            {
                var contents = template.Contents.Contents;
                int index = contents.FindIndex(x => x == token);
                if (index >= 0)
                {
                    if (template is MagicTemplate)
                    {
                        return ((MagicTemplate)template, index);
                    }
                }
            }
            throw new Exception("unable to find " + token);
        }

        private void UpdateCount(int d)
        {
            Debug.Entered();
            Comment? comment = FindComment("COUNT");
            if (null == comment)
            {
                throw new Exception("Unable to find COUNT in " + Title);
            }
            var index = FindIndex(comment);
            if (-1 == index)
            {
                throw new Exception("Unable to find COUNT token in " + Title);
            }
            var countToken = GetToken(index + 1);
            int count = Convert.ToInt32(countToken.Text) + d;
            Contents.Contents[index + 1] = new Token(count.ToString());
            Debug.Exited("Count = " + count);
        }

        public void Add(string title)
        {
            Debug.Entered("title=" + title);
            var input = new Tokens (new List<IToken> { new Token("* "), new Link(title), new Token("\n") });

            Link? entry = Links.Find(link => link.Namespace.Text.Equals("") && String.Compare(link.Data, title) >= 0);
            if (null == entry)
            {
                Debug.WriteLine("null found - add to end of list");
                entry = Links.FindLast(link => link.Namespace.Text.Equals(""));
                if (null == entry)
                {
                    throw new Exception("Unable to find any links on " + Title);
                }
                var (template, index) = FindContents(entry);
                template.Contents.InsertRange(index + 2, input);
            }
            else if (entry.Data.Equals(title))
            {
                return;
            }
            else
            {
                var (template, index) = FindContents(entry);
                template.Contents.InsertRange(index - 1, input);
            }

            UpdateCount(1);
            Debug.Exited();
        }

        public void Remove(string title)
        {
            Debug.Entered("title=" + title);
            var entry = Links.Find(link => link.Data.Equals(title));
            if (null == entry)
            {
                throw new Exception("Unable to find links " + Title);
            }
            var (template, index) = FindContents(entry);
            template.Contents.Contents.RemoveRange(index - 1, 3);
            UpdateCount(-1);
            Debug.Exited();
        }

        public List<Page> Articles()
        {
            var articles = new List<Page>();
            var links = Links.FindAll(x => x.Namespace.Text.Equals(""));
            foreach (var link in links)
            {
                var page = new ArticlePage(link.Data);
                articles.Add(page);
            }
            return articles;
        }

        public Showcase(string title, int revId = -1) : base(showcasePageTitle + "/" + title, revId)
        {
            Load();
        }
    }

    public class AClassShowcasePage : Showcase
    {
        // Create as a singleton
        private static readonly Lazy<AClassShowcasePage> _instance = new Lazy<AClassShowcasePage>(() => new AClassShowcasePage());
        public static AClassShowcasePage Instance => _instance.Value;

        private AClassShowcasePage() : base("A")
        {
        }
    }

    public class FeaturedShowcasePage : Showcase
    {
        // Create as a singleton
        private static readonly Lazy<FeaturedShowcasePage> _instance = new Lazy<FeaturedShowcasePage>(() => new FeaturedShowcasePage());
        public static FeaturedShowcasePage Instance => _instance.Value;

        private FeaturedShowcasePage() : base("FA")
        {
        }
    }
        public class AClassListShowcasePage : Showcase
    {
        // Create as a singleton
        private static readonly Lazy<AClassListShowcasePage> _instance = new Lazy<AClassListShowcasePage>(() => new AClassListShowcasePage());
        public static AClassListShowcasePage Instance => _instance.Value;

        private AClassListShowcasePage() : base("AL")
        {
        }
    }

    public class FeaturedListShowcasePage : Showcase
    {
        // Create as a singleton
        private static readonly Lazy<FeaturedListShowcasePage> _instance = new Lazy<FeaturedListShowcasePage>(() => new FeaturedListShowcasePage());
        public static FeaturedListShowcasePage Instance => _instance.Value;

        private FeaturedListShowcasePage() : base("FL")
        {
        }
    }
}
