using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia
{
    public class Cell
    {
        public string Data { get; set; }
        public Dictionary<string, string> Attributes { get; set; }

        public void Add(string key, string value)
        {
            Attributes.Add(key, value);
        }

        public void Align(string align)
        {
            Add("align", align);
        }

        public Cell(string data, params string[] attributes)
        {
            Data = data;
            Attributes = new Dictionary<string, string>();
            for (int i = 0; i < attributes.Length; i += 2)
            {
                Add(attributes[i], attributes[i + 1]);
            }
        }

        public Cell() : this("")
        {
        }

        public override string ToString()
        {
            var tokenStringList = new List<string>();
            foreach (KeyValuePair<string, string> pair in Attributes)
            {
                tokenStringList.Add(pair.Key);
                tokenStringList.Add("=");
                if (pair.Value.Length > 1 && pair.Value.StartsWith("{{"))
                {
                    tokenStringList.Add(pair.Value);
                }
                else
                {
                    tokenStringList.Add(pair.Value.Quote());
                }
                tokenStringList.Add(" ");
            }
            if (0 != Attributes.Count)
            {
                tokenStringList.Add("| ");
            }
            tokenStringList.Add(Data);
            tokenStringList.Add(" ");

            return String.Join("", tokenStringList);
        }
    }

    public class Row
    {
        protected List<Cell> cells;

        public void Add(Cell cell)
        {
            cells.Add(cell);
        }

        public void Add(string data, params string[] attributes)
        {
            cells.Add(new Cell(data, attributes));
        }

        public Row()
        {
            cells = new List<Cell>();
        }

        public override string ToString()
        {
            bool firstpass = true;
            var tokenStringList = new List<string>();
            tokenStringList.Add("|-\n");
            foreach (var cell in cells)
            {
                tokenStringList.Add(firstpass ? "| " : "|| ");
                tokenStringList.Add(cell.ToString());
                firstpass = false;
            }
            tokenStringList.Add("\n");
            return String.Join("", tokenStringList);
        }

    }

    public class Header : Row
    {
        public override string ToString()
        {
            bool firstpass = true;
            var tokenStringList = new List<string>();
            tokenStringList.Add("|-\n");
            foreach (var cell in cells)
            {
                tokenStringList.Add(firstpass ? "! " : "!! ");
                tokenStringList.Add(cell.ToString());
                firstpass = false;
            }
            tokenStringList.Add("\n");
            return String.Join("", tokenStringList);
        }
    }

    public class Table : IToken
    {
        private Tokens contents;
        public static bool Parse { get; set; }

        public TokenType Type { get; private set; }

        public Tokens Contents
        {
            get => contents;
        }

        public string Text
        {
            get => ToString();
            set => parse(new Tokens(value));
        }

        public Tokens Caption { get; set; }
        public string Class { get; set; }
        public string Style { get; set; }
        public bool NoInclude { get; set; }
        public Header Header { get; private set; }
        public List<Row> Rows { get; private set; }

         private Tokens readLine(Tokens tokens)
        {
            var list = new Tokens();
            while (tokens.Any() && tokens.Peek().Type != TokenType.Newline)
            {
                list.Add(tokens.Dequeue());
            }
            return list;
        }

        private string readStart(Tokens tokens)
        {
            string retval = "";
            Debug.WriteLine("readStart: token='" + tokens.Text + "'");
            if (tokens.Any())
            {
                if (tokens.Peek().Type == TokenType.Pipe)
                {
                    Debug.WriteLine("    found pipe...");
                    var firstChar = tokens.Any(1) && tokens.Peek(1).Type == TokenType.Text ? tokens.Peek(1).Text : "";
                    Debug.WriteLine("    first='" + firstChar + "'");
                    if (firstChar.Equals("+"))
                    {
                        retval = "|+";
                    }
                    else if (firstChar.Equals("-"))
                    {
                        retval = "|-";
                    }
                    else
                    {
                        retval = "|";
                    }
                }
                else if (tokens.Peek().Type == TokenType.Text && tokens.Peek().Text.Matches("!"))
                {
                    retval = "!";
                }
            }

            Debug.WriteLine("readStart='" + retval + "'");
            return retval;
        }

        private Dictionary<string, string> parseAttributes(Tokens tokens)
        {
            var attributes = new Dictionary<string, string>();
            MatchCollection matches = Regex.Matches(tokens.Text, @"\s*(\w+)\s*=\s*""(.+?)""");
            foreach (Match match in matches)
            {
                Debug.WriteLine(match.Groups[1].Value + " => " + match.Groups[2].Value);
                attributes.Add(match.Groups[1].Value, match.Groups[2].Value);
            }
            return attributes;
        }

        private void parseTableAttributes(Tokens tokens)
        {
            var line = readLine(tokens);
            Debug.Entered("line='" + line + "'");

            Dictionary<string, string> attributes = parseAttributes(line);
            if (attributes.ContainsKey("class"))
            {
                //Console.WriteLine ("Class='" + Class + "'");
                Class = attributes["class"];
            }

            if (attributes.ContainsKey("style"))
            {
                //Console.WriteLine ("Style='" + Style + "'");
                Style = attributes["style"];
            }

            Debug.Exited();
        }

        private void parseCaption(Tokens tokens)
        {
            var start = readStart(tokens);
            if (start.Equals("|+"))
            {
                tokens.Dequeue(); // swallow pipe
                tokens.Dequeue(); // swallow '+'
                Caption = readLine(tokens);
                Debug.WriteLine("Caption='" + Caption.Text + "'");
            }
        }

        private Cell parseCell(Tokens tokens)
        {
            Debug.Entered("text = '" + tokens.Text + "'");
            Cell cell = new Cell(tokens.Text);
            if (tokens.Contains(TokenType.Pipe))
            {
                var data = new Tokens();
                while (tokens.Any())
                {
                    var token = tokens.Dequeue();
                    if (token.Type == TokenType.Pipe)
                    {
                        cell = new Cell(tokens.Text);
                        var attributes = parseAttributes(data);
                        cell.Attributes = attributes;
                        break;
                    }
                    data.Add(token);
                }
            }
            Debug.Exited();
            return cell;
        }

        private void parseHeader(Tokens tokens)
        {
            Debug.Entered();
            tokens.Dequeue(); // swallow bang
            var data = new Tokens();
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                Debug.WriteLine("parseHeader: token='" + token.Text + "'");
                if (token.Text.Equals("!!"))
                {
                    var cell = parseCell(data);
                    Header.Add(cell);
                    data = new Tokens();
                }
                else if (token.Type == TokenType.Pipe && tokens.Peek().Type == TokenType.Pipe)
                {
                    tokens.Dequeue(); // swallow pipe
                    var cell = parseCell(data);
                    Header.Add(cell);
                    data = new Tokens();
                }
                else
                {
                    data.Add(token);
                }
            }
            if (data.Any())
            {
                var cell = parseCell(data);
                Header.Add(cell);
            }
            Debug.Exited();
        }

        private void parseRow(Row row, Tokens tokens)
        {
            Debug.Entered();
            tokens.Dequeue(); // swallow pipe
            var data = new Tokens();
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                Debug.WriteLine("parseRow: token='" + token.Text + "'");
                if (token.Type == TokenType.Pipe && tokens.Peek().Type == TokenType.Pipe)
                {
                    tokens.Dequeue(); // swallow pipe
                    var cell = parseCell(data);
                    row.Add(cell);
                    data = new Tokens();
                }
                else
                {
                    data.Add(token);
                }
            }
            if (data.Any())
            {
                var cell = parseCell(data);
                row.Add(cell);
            }
            Debug.Exited();
        }

        private void parseRows(Tokens tokens)
        {
            Debug.Entered();
            Row row = new Row();
            while (tokens.Any())
            {
                var start = readStart(tokens);
                if (start.Equals("|-"))
                {
                    Debug.WriteLine("New row found");
                    tokens.Dequeue(); // swallow pipe
                    tokens.Dequeue(); // swallow '-'
                    tokens.Dequeue(); // swallow NL

                    var line = readLine(tokens);
                    Debug.WriteLine("parseRows line='" + line + "'");

                    start = readStart(line);
                    Debug.WriteLine("parseRows start='" + start + "'");
                    if (start.Equals("!"))
                    {
                        parseHeader(line);
                    }
                    else
                    {
                        row = new Row();
                        parseRow(row, line);
                        Rows.Add(row);
                    }
                }
                else if (start.Equals("|"))
                {
                    var line = readLine(tokens);
                    parseRow(row, line);
                }
                else
                {
                    Debug.WriteLine("Found token: " + start);
                    break;
                }
            }
            Debug.Exited();
        }

        private Tokens retokenize(Tokens contents)
        {
            Tokens tokens = new Tokens();
            foreach (var token in contents.Contents)
            {
                if (token.Type == TokenType.Text)
                {
                    const string delimiters = @"(\+|\-|!+)";
                    string[] substrings = Regex.Split(token.Text, delimiters);
                    foreach (string match in substrings)
                    {
                        if (string.Empty.Equals(match))
                            continue;

                        tokens.Add(new Token(match));
                        Debug.WriteLine("retokenize: token=" + match);
                    }
                }
                else
                {
                    tokens.Add(token);
                }
            }
            return tokens;
        }

        private void parse(Tokens contents)
        {
            // Very basic for now
            /* {| class="wikitable sortable" style="overflow-wrap: anywhere;"
                |-
                ! scope="col" style="width: 6em" | Date last FAC or FAR 
                ! scope="col" style="width: 6em" | TFA date 
                ! scope="col" style="width: 17em" | Article notif
                ! scope="col" | Most recent FAC or FAR 
                ! scope="col" style="width: 7em" | Notes 
                |-
                | 2004-08-02 
                | 2016-06-19 
                | [[Quatermass and the Pit]] 
                | [[Wp:Featured article candidates/Quatermass and the Pit]] 
                |-
                |}
    | 		*/

            var tokens = retokenize(contents);
            parseTableAttributes(tokens);
            parseCaption(tokens);
            parseRows(tokens);
        }

        public override string ToString()
        {
            var tokenStringList = new List<string>();
            tokenStringList.Add("{|");

            if (contents.Any())
            {
                tokenStringList.Add(contents.Text);
            }
            else
            {
                tokenStringList.Add(" class=" + Class.Quote());
                tokenStringList.Add(" style=" + Style.Quote());
                tokenStringList.Add("\n");

                tokenStringList.Add(Header.ToString());
                foreach (var row in Rows)
                {
                    tokenStringList.Add(row.ToString());
                }
            }

            tokenStringList.Add("|}");
            return String.Join("", tokenStringList);
        }

        static Table()
        {
            Parse = false;
        }

        public Table(Page page, Tokens tokens): this()
        {
            while (tokens.Any())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            contents.Add(new Template(page, tokens));
                            break;
                        case TokenType.CloseTable:
                            if (Parse)
                            {
                                parse(contents);
                            }
                            return;
                        default:
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed table on " + page.Title + ": " + contents.Preview());
        }

        public Table()
        {
            Type = TokenType.Table;
            Class = "wikitable sortable";
            Style = "text-align:center";
            NoInclude = false;
            Header = new Header();
            Rows = new List<Row>();
            Caption = new Tokens();
            contents = new Tokens();
         }
    }
}
