using System;
using System.Diagnostics;
namespace Wikimedia
{
    public class TalkPage : Page
    {
        private ArticlePage? article;
        private ProjectBannerShell? projectBannerShell;

        public ArticleHistory ArticleHistory { get; set; }
        public MilHist.MilHist MilHist { get; set; }

        public ArticlePage Article
        {
            get =>  article ??= new ArticlePage(TitleWithoutNamespace);
            set => article = value;
        }

        public ProjectBannerShell ProjectBannerShell
        {
            get
            {
                if (null == projectBannerShell)
                {
                    projectBannerShell = ProjectBannerShell.GetProjectBannerShell(this);
                    if (null == projectBannerShell)
                    {
                        projectBannerShell = new ProjectBannerShell(this);
                        projectBannerShell.AddAll();
                        Insert(0, projectBannerShell);
                    }
                }
                return projectBannerShell;
            }
            set => projectBannerShell = value;
        }

        public string Class
        {
            get => ProjectBannerShell.Class;
            set => ProjectBannerShell.Class = value;
        }

        public TalkPage(Namespace ns, string title, int revid = -1) : base (ns.Append(title), revid)
        {
            Debug.Entered();
            // These are normally set when the page is parsed (if present)
            Namespace = ns;
            if (null == MilHist)
            { 
                MilHist = new Wikimedia.MilHist.MilHist(this);
            }
            if (null == ArticleHistory)
            { 
                ArticleHistory = new ArticleHistory(this);
            }
            Debug.Exited();
        }

        public TalkPage(string title, int revid = -1) : this (new Namespace("Talk"), title, revid)
        {
        }
    }
}