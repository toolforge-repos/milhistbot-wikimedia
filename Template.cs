using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wikimedia
{
    public class Template : IToken, IPageElement, ITreeElement
    {
        public string Name { get; set; }
        public Namespace Namespace { get; set; }
        public Page Page { get; set; }
        public TokenType Type { get; private set; }
        public virtual string Text { get { return TemplateText(); } }
        public bool Deleted { get; set; }
        public Template? Owner { get; set; }
        public Tokens Contents { get; set; }

        private Tokens name;
        private List<Parameter> parameters;
        private int defaultCount;

        public string? this[string index]
        {
            get
            {
                if (string.IsNullOrEmpty(index))
                {
                    return null;
                }
                return Exists(index) ? Find(index)!.Value : null;
            }
            set
            {
                if (null != value)
                {
                    Add(index, value);
                }
            }
        }

        private string TemplateText()
        {
            if (Deleted)
            {
                return string.Empty;
            }

            var tokenStringList = new List<string>();
            tokenStringList.Add("{{");
            // Name includes namespace
            tokenStringList.Add(Name);

            foreach (var parameter in parameters)
            {
                tokenStringList.Add("|");
                tokenStringList.Add(parameter.Text);
            }
            tokenStringList.Add("}}");
            string tokenString = String.Join(string.Empty, tokenStringList);
            return tokenString;
        }

        public static Tokens RemoveComments(Tokens tokens)
        {
            return new Tokens(tokens.Contents.FindAll(ind => (ind.Type != TokenType.Comment)));
        }

        private static Tokens ParameterName(Tokens queue)
        {
            var tokens = new Tokens();
            while (queue.Any() && !queue.IsToken(TokenType.Equals) && !queue.IsToken(TokenType.Pipe))
            {
                tokens.Add(queue.Dequeue());
            }
            return tokens;
        }

        public static Tokens TemplateName(Tokens queue)
        {
            var tokens = new Tokens();
            while (queue.Any() && !queue.IsToken(TokenType.Colon) && !queue.IsToken(TokenType.Pipe))
            {
                tokens.Add(queue.Dequeue());
            }
            return tokens;
        }

        private static Tokens ParameterValue(Tokens queue)
        {
            var tokens = new Tokens();
            while (queue.Any() && !queue.IsToken(TokenType.Pipe))
            {
                tokens.Add(queue.Dequeue());
            }
            return tokens;
        }

        private void Parse(Tokens tokens)
        {
            Tokens queue = tokens.Clone();
            name = TemplateName(queue);
            Name = RemoveComments(name).Text.Trim();

            if (queue.IsToken(TokenType.Colon))
            {
                queue.Dequeue();
                if (Namespace.IsNamespace(Name))
                {
                    Namespace = new Namespace(Name);
                }
                name.Add(":");
                name.Add(ParameterName(queue));
                Name = RemoveComments(name).Text.Trim();
            }
            else
            {
                Namespace = new Namespace("");
            }

            while (queue.Any())
            {
                if (queue.IsToken(TokenType.Pipe))
                {
                    queue.Dequeue();
                    var parameterTokens = ParameterName(queue);
                    if (queue.IsToken(TokenType.Equals))
                    {
                        queue.Dequeue();
                        parameters.Add(new Parameter(parameterTokens, ParameterValue(queue)));
                    }
                    else
                    {
                        parameters.Add(new Parameter(++defaultCount, parameterTokens));
                    }
                }
                else
                {
                    throw new ParseException("unexpected token: '" + queue.Contents[0].Text + "'");
                }
            }
        }

        public void Added(Page page)
        {
            // Debug.Entered();
            Page = page;
            Page.Templates.Add(this);
            // Debug.Exited();
        }

        public void Add(Parameter parameter)
        {
            int pos = FindIndex(parameter);
            if (pos >= 0)
            {
                parameters[pos] = parameter;
            }
            else
            {
                parameters.Add(parameter);
            }
        }

        public void Add(string name, string value)
        {
            Add(new Parameter(name, value));
        }

        public void Add(string name, bool value)
        {
            Add(name, value ? "yes" : "no");
        }

        public void Add(int id, string value)
        {
            Add(new Parameter(id, value));
        }

        public bool Exists(string match)
        {
            return parameters.Exists(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public bool Exists(string match, string value)
        {
            return parameters.Exists(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase) && Regex.IsMatch(ind.Value, value, RegexOptions.IgnoreCase));
        }

        public int FindIndex(Parameter parameter)
        {
            return parameters.FindIndex(ind => parameter.Name.Equals(ind.Name, StringComparison.OrdinalIgnoreCase));
        }

        public Parameter? Find(string match)
        {
            return parameters.Find(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public List<Parameter> FindAll(string match)
        {
            return parameters.FindAll(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public List<Parameter> FindAll(string match, string value)
        {
            return parameters.FindAll(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase) && Regex.IsMatch(ind.Value, value, RegexOptions.IgnoreCase));
        }

        public Parameter? FindLast(string match)
        {
            return parameters.FindLast(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase));
        }

        public Parameter? FindLast(string match, string value)
        {
            return parameters.FindLast(ind => Regex.IsMatch(ind.Name, match, RegexOptions.IgnoreCase) && Regex.IsMatch(ind.Value, value, RegexOptions.IgnoreCase));
        }

        public String Fetch(String name, String defaultValue)
        {
            String returnValue = defaultValue;
            if (Exists(name))
            {
                var p = Find(name);
                returnValue = p!.Value;
            }
            return returnValue;
        }

        public String Fetch(String name)
        {
            String returnValue;
            if (Exists(name))
            {
                var p = Find(name);
                returnValue = p!.Value;
            }
            else
            {
                throw new Exception("Could not find " + name + " parameter in " + Page.Title + " " + Name + " template");
            }
            return returnValue;
        }

        public void Insert(int index, Parameter parameter)
        {
            parameters.Insert(index, parameter);
        }

        public void Insert(int index, string name, string value)
        {
            Insert(index, new Parameter(name, value));
        }

        public void Remove(Parameter parameter)
        {
            parameters.Remove(parameter);
        }

        public void RemoveAll(string match)
        {
            var parameters = FindAll(match);
            foreach (var parameter in parameters)
            {
                Remove(parameter);
            }
        }

        public void Removed(Page page)
        {
            page.Templates.Remove(this);
        }

        public override string ToString()
        {
            return TemplateText();
        }

        public Tokens TreeContents()
        {
            return Contents;
        }

        private void Init()
        {
            Type = TokenType.Template;
            defaultCount = 0;
            Deleted = false;
            Owner = null;
            parameters = new List<Parameter>();
        }

        public Template(Page page, Tokens tokens)
        {
            Page = page;
            Name = "";
            Namespace = new Namespace("");
            name = new Tokens();
            Tokens contents = new Tokens();
            parameters = new List<Parameter>();

            while (!tokens.Empty())
            {
                var token = tokens.Dequeue();
                try
                {
                    switch (token.Type)
                    {
                        case TokenType.OpenComment:
                            contents.Add(new Comment(page, tokens));
                            break;
                        case TokenType.OpenGallery:
                            contents.Add(new Gallery(page, token, tokens));
                            break;
                        case TokenType.OpenNowiki:
                            contents.Add(new Nowiki(page, tokens));
                            break;
                        case TokenType.OpenBracket:
                            contents.Add(new Link(page, tokens));
                            break;
                        case TokenType.Reference:
                            contents.Add(new Reference(page, token));
                            break;
                        case TokenType.OpenReference:
                            contents.Add(new Reference(page, token, tokens));
                            break;
                        case TokenType.OpenParameter:
                            contents.Add(new Parameter(page, tokens));
                            break;
                        case TokenType.OpenTemplate:
                            var template = TemplateFactory.GetTemplate(page, tokens);
                            template.Owner = this;
                            contents.Add(template);
                            break;
                        case TokenType.CloseTemplate:
                            Init();
                            Parse(contents);
                            Contents = contents;
                            Added(page);
                            return;
                        default:
                            contents.Add(token);
                            break;
                    }
                }
                catch (ParseException)
                {
                    contents.Add(token);
                }
            }
            tokens.InsertRange(0, contents);
            throw new ParseException("Unclosed template on " + page.Title + ": " + contents.Preview());
        }

        public Template(Page page, string name) : this (page, new Tokens(name + "}}"))
        {
        }

        public Template(string name) : this (new Page (""), name)
        {
        }
    }
}
