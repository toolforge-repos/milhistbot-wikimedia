using System;

namespace Wikimedia
{
    public class TemplateFactory
    {
        public static Template GetTemplate(ArticlePage page, Tokens tokens)
        {
            string name = tokens.Contents[0].Text;
            Debug.Entered("name='" + name + "'");
 
            if (MilHist.AnnouncementsTemplate.IsAnnouncements(name))
            {
                return new MilHist.AnnouncementsTemplate(page, tokens);
            }
            else if (MilHist.AwardNominationTemplate.IsNomination(name))
            {
                return new MilHist.AwardNominationTemplate(page, tokens);
            }
            else if (MagicTemplate.IsMagicTemplate(name))
            {
                return new MagicTemplate(page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public static Template GetTemplate(TalkPage page, Tokens tokens)
        {
            string name = tokens.Contents[0].Text;

            if (ArticleHistory.IsArticleHistory(name))
            {
                return new ArticleHistory(page, tokens);
            }
            else if (ArticleForDeletion.IsArticleForDeletion(name))
            {
                return new ArticleForDeletion(page, tokens);
            }
            else if (DidYouKnow.IsDidYouKnow(name))
            {
                return new DidYouKnow(page, tokens);
            }
            else if (GoodArticles.IsGoodArticle(name))
            {
                return new GoodArticles(page, tokens);
            }
            else if (InTheNews.IsInTheNews(name))
            {
                return new InTheNews(page, tokens);
            }
            else if (OldPeerReview.IsOldPeerReview(name))
            {
                return new OldPeerReview(page, tokens);
            }
            else if (OnThisDay.IsOnThisDay(name))
            {
                return new OnThisDay(page, tokens);
            }
            else if (ProjectBannerShell.isProjectBannerShell(page, name))
            {
                return new ProjectBannerShell(page, tokens);
            }
            else if (MilHist.MilHist.IsMilHist(name))
            {
                return new MilHist.ProjectTemplate(page, tokens);
            }
            else if (ProjectBanner.isProjectBanner(page, name))
            {
                return new ProjectBanner(page, tokens);
            }
            else if (MagicTemplate.IsMagicTemplate(name))
            {
                return new MagicTemplate(page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public static Template GetTemplate(TemplatePage page, Tokens tokens)
        {
            string name = tokens.Contents[0].Text;

            if (MilHist.AnnouncementsTemplate.IsAnnouncements(name))
            {
                return new MilHist.AnnouncementsTemplate(page, tokens);
            }
            else if (MagicTemplate.IsMagicTemplate(name))
            {
                return new MagicTemplate(page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public static Template GetTemplate(WikipediaPage page, Tokens tokens)
        {
            string name = tokens.Contents[0].Text;
            // Console.WriteLine("GetTemplate: " + page.Title + " (" + typeof(Page) + ")");

            if (MagicTemplate.IsMagicTemplate(name))
            {
                return new MagicTemplate(page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public static Template GetTemplate(WikipediaTalkPage page, Tokens tokens)
        {
            string name = tokens.Contents[0].Text;
            // Console.WriteLine("GetTemplate: " + page.Title + " (" + typeof(Page) + ")");

            if (MagicTemplate.IsMagicTemplate(name))
            {
                return new MagicTemplate(page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public static Template GetTemplate(Page page, Tokens tokens)
        {
            Debug.Entered(page.Title + " (" + page.GetType() + ")");
            // This goes first since TemplatePage is based on ArticlePage
            if (page is TemplatePage)
            {
                return GetTemplate((TemplatePage)page, tokens);
            }
            else if (page is ArticlePage)
            {
                return GetTemplate((ArticlePage)page, tokens);
            }
            else if (page is TalkPage)
            {
                return GetTemplate((TalkPage)page, tokens);
            }
            else if (page is WikipediaPage)
            {
                return GetTemplate((WikipediaPage)page, tokens);
            }
            else if (page is WikipediaTalkPage)
            {
                return GetTemplate((WikipediaTalkPage)page, tokens);
            }
            else
            {
                return new Template(page, tokens);
            }
        }

        public TemplateFactory()
        {
        }
    }
}
