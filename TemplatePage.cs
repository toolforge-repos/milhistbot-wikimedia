using System;
using System.ComponentModel;
using System.Xml.Linq;

namespace Wikimedia
{
	public class TemplatePage: ArticlePage
	{
        private TemplateTalkPage? talk;
        public new TemplateTalkPage Talk
        {
            get => talk ??= new TemplateTalkPage(TitleWithoutNamespace);
        }

        public TemplatePage(string title, int revid = -1) : base (new Namespace("Template"), title, revid)
		{
        }
	}

	public class TemplateTalkPage: TalkPage
	{
        private TemplatePage? article;
        public new TemplatePage Article
        {
            get => article ??= new TemplatePage(TitleWithoutNamespace);
        }

        public TemplateTalkPage(string title, int revid = -1) : base(new Namespace("Template talk"), title, revid)
        {
        }
    }
}
