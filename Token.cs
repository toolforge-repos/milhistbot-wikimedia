using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using Newtonsoft.Json.Linq;
using Wikimedia;

public enum TokenType
{
    OpenParameter,
    CloseParameter,
	OpenBlockquote,
	CloseBlockquote,
    OpenBracket,
    CloseBracket,
    OpenComment,
    CloseComment,
    OpenGallery,
    CloseGallery,
    OpenExternalLink,
    CloseExternalLink,
    OpenNowiki,
    CloseNowiki,
    OpenTemplate,
    CloseTemplate,
    Colon,
    Equals,
    MultiEquals,
    Newline,
    Nowiki,
    Pipe,
    Text,
    Blockquote,
    Comment,
    ExtendedImage,
    ExternalLink,
    Gallery,
    Link,
    Namespace,
    Parameter,
    Reference,
    OpenReference,
    CloseReference,
    Section,
    OpenTable,
    CloseTable,
    Table,
    Template,
    Tokens,
}

public interface IToken
{
    TokenType Type { get; }
    string Text { get; }
}

public class Tokens : IToken
{
    public List<IToken> Contents { get; private set; }
    public TokenType Type { get; private set; }
    public string Text
    {
        get
        {
            var tokenStringList = new List<string>();
            foreach (var token in Contents)
            {
                tokenStringList.Add(token.Text);
            }
            return String.Join("", tokenStringList);
        }
    }

    public override string ToString ()
    {
        return Text;
    }

    public string Preview ()
    {
        string preview = Text;
	    if (preview.Length >= 80)
        {
            return preview.Substring (0, 80);
        }
        return preview;
    }

	public void Add (params string [] strings)
	{
		foreach (var s in strings)
		{
			Contents.Add (new Token (s));
		}
	}
	
    public void Add(params IToken[] tokens)
    {
        foreach (var token in tokens)
        {
            Contents.Add(token);
        }
    }

    public void Add(Tokens tokens)
    {
        foreach (var token in tokens.Contents)
        {
            Contents.Add(token);
        }
    }

    public bool Any(int n = 0)
    {
        return Contents.Count > n;
    }

    public void Append(IToken token, params IToken[] items)
    {
        int index = FindIndex(token);
        Append(index, items);
    }

    public void Append(int index, params IToken[] items)
    {
        if (index + 1 < Contents.Count)
        {
            Contents.InsertRange(index + 1, items);
        }
        else
        {
            Add(items);
        }
    }

    public Tokens Clone()
    {
        return new Tokens(Contents.GetRange(0, Contents.Count));
    }

    public bool Contains(TokenType type)
    {
        return Contents.Exists(x => x.Type == type);
    }

    public IToken Dequeue()
    {
        var dequeue = Contents[0];
        Contents.RemoveAt (0);
        return dequeue;
    }

    public void Dump()
    {
        foreach (var token in Contents)
        {
            Debug.WriteLine("token Type=" + token.Type + " value='" + token.Text + "'");
        }
    }

    public bool Empty()
    {
        return Contents.Count == 0;
    }

    public IToken EndOfLine(IToken token)
    {
        int index = EndOfLineIndex(token);
        return Contents[index];
    }

    public int EndOfLineIndex(IToken token)
    {
        int index = FindIndex(token);
        while (Contents[index].Type != TokenType.Newline)
        {
            ++index;
        }
        return index;
    }

    public int FindIndex(IToken token)
    {
        return Contents.FindIndex(x => x == token);
    }

    public List<IToken> GetLine(IToken token)
    {
        int start = StartOfLineIndex(token);
        int end = EndOfLineIndex(token);
        int count = end - start + 1;
        return GetRange(start, count);
    }

    public List<IToken> GetRange(int start, int count)
    {
        return Contents.GetRange(start, count);
    }

    public IToken GetToken(int index)
    {
        return Contents.ElementAt(index);
    }

    public void InsertRange(int index, Tokens tokens)
    {
        Contents.InsertRange(index, tokens.Contents);
    }

    public void InsertRange(int index, params IToken[] items)
    {
        Contents.InsertRange(index, items);
    }

    public void InsertRange(IToken token, Tokens tokens)
    {
        int index = FindIndex(token);
        InsertRange(index, tokens);
    }

    public void InsertRange(IToken token, params IToken[] items)
    {
        int index = FindIndex(token);
        InsertRange(index, items);
    }

    public void InsertRange(IToken token, params string[] strings)
    {
        var items = new List<IToken>();
        foreach (var s in strings)
        {
            items.Add(new Token(s));
        }
        InsertRange(token, items.ToArray());
    }

    public bool IsToken(TokenType type)
    {
        return Any() && type == Contents[0].Type;
    }

    public IToken Peek (int n = 0)
    {
        return Contents[n];
    }

    public void Remove(IToken token)
    {
        int index = FindIndex(token);
        Contents.RemoveAt(index);
    }

    public List<IToken> RemoveLine(IToken token)
    {
        int start = StartOfLineIndex(token);
        int end = EndOfLineIndex(token);
        int count = end - start + 1;
        return RemoveRange(start, count);
    }

    public List<IToken> RemoveRange(int start, int count)
    {
        List<IToken> items = Contents.GetRange(start, count);
        Contents.RemoveRange(start, count);
        return items;
    }

    public IToken StartOfLine(IToken token)
    {
        int index = StartOfLineIndex(token);
        return Contents[index];
    }

    public int StartOfLineIndex(IToken token)
    {
        var index = FindIndex(token);
        while (Contents[index].Type != TokenType.Newline)
        {
            --index;
        }
        return index + 1;
    }

    public void Trim(IToken token)
    {
        int index = FindIndex(token) + 1;
        while (index > 0 && index < Contents.Count && String.IsNullOrWhiteSpace(Contents[index].Text))
        {
            Contents.RemoveAt(index);
        }
    }

    public Tokens(List<IToken> contents)
    {
        Contents = contents;
        Type = TokenType.Tokens;
    }

    public Tokens (string content)
    {
        Contents = new List<IToken>();
        const string delimiters = "(}}}}|{{{|}}}|{{|}}|\\|}}}|\\|}}|\\[\\[|\\]\\]|{\\||\\|}|\\||:|<gallery *.*?>|</ *gallery>|<!--|-->|<ref>|<ref *name.*?/*>|</ *ref>|<nowiki>|</ *nowiki>|<blockquote>|</ *blockquote>|\n|\\[http|\\]|=+)";
        string[] substrings = Regex.Split(content, delimiters);
        foreach (string match in substrings)
        {
            if (string.Empty.Equals(match))
            {
                continue;
            }

            if (match.Equals("}}}}"))
            {
                Contents.Add(new Token("}}"));
                Contents.Add(new Token("}}"));
                continue;
            }

            if (match.Equals("|}}}"))
            {
                Contents.Add(new Token("|"));
                Contents.Add(new Token("}}}"));
                continue;
            }

            if (match.Equals ("|}}"))
            {
                Contents.Add (new Token ("|"));
                Contents.Add (new Token ("}}"));
                continue;
            }

            var token = new Token (match);
            Contents.Add (token);
        }
        Type = TokenType.Tokens;
    }

    public Tokens()
    {
        Contents = new List<IToken>();
        Type = TokenType.Tokens;
    }
}

public class Token : IToken
{
    public TokenType Type { get; private set; }
    public string Text { get; set; }

    private static Token? newline;
    public static Token Newline
    {
        get
        {
            if (null == newline)
            {
                newline = new Token("\n");
            }
            return newline;
        }
    }

    public override string ToString ()
    {
        return Text;
    }

    public Token(TokenType type)
    {
        Type = type;
        Text = string.Empty;
    }

    public Token(string value)
    {
        if ("{{{".Equals(value))
            Type = TokenType.OpenParameter;
        else if ("}}}".Equals(value))
            Type = TokenType.CloseParameter;
        if ("{{".Equals (value))
            Type = TokenType.OpenTemplate;
        else if ("}}".Equals (value))
            Type = TokenType.CloseTemplate;
        else if ("[[".Equals (value))
            Type = TokenType.OpenBracket;
        else if ("]]".Equals (value))
            Type = TokenType.CloseBracket;
        else if ("<!--".Equals (value))
            Type = TokenType.OpenComment;
        else if ("-->".Equals (value))
            Type = TokenType.CloseComment;
        else if ("{|".Equals (value))
            Type = TokenType.OpenTable;
        else if ("|}".Equals (value))
            Type = TokenType.CloseTable;
        else if ("[http".Equals (value))
            Type = TokenType.OpenExternalLink;
        else if ("]".Equals (value))
            Type = TokenType.CloseExternalLink;
        else if (Regex.IsMatch (value, "==+"))
            Type = TokenType.MultiEquals;
        else if (Regex.IsMatch (value, "<ref.+?/>"))
            Type = TokenType.Reference;
        else if (Regex.IsMatch (value, "<ref>"))
            Type = TokenType.OpenReference;
        else if (Regex.IsMatch (value, "<ref +name.+?>"))
            Type = TokenType.OpenReference;
        else if (Regex.IsMatch (value, "</ *ref>"))
            Type = TokenType.CloseReference;
        else if (Regex.IsMatch (value, "<nowiki>"))
            Type = TokenType.OpenNowiki;
        else if (Regex.IsMatch (value, "</nowiki>"))
            Type = TokenType.CloseNowiki;
    	else if (Regex.IsMatch (value, "<gallery *.*?>"))
            Type = TokenType.OpenGallery;
        else if (Regex.IsMatch (value, "</ *gallery>"))
            Type = TokenType.CloseGallery;
        else if (Regex.IsMatch (value, "<blockquote>"))
            Type = TokenType.OpenBlockquote;
        else if (Regex.IsMatch (value, "</blockquote>"))
            Type = TokenType.CloseBlockquote;    
        else if ("=".Equals (value))
            Type = TokenType.Equals;
        else if ("|".Equals (value))
            Type = TokenType.Pipe;
        else if (":".Equals (value))
            Type = TokenType.Colon;
    	else if ("\n".Equals (value))
             Type = TokenType.Newline;
	    else
            Type = TokenType.Text;

        Text = value;
     }

    public Token(TokenType type, string value)
    {
        Type = type;
        Text = value;
    }

}
