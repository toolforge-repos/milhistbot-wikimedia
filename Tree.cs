using System;

namespace Wikimedia
{
	public interface ITreeElement
	{
		Tokens TreeContents();
	}

	public static class Tree
	{
		public static Tokens? Find(Tokens tokens, IToken item)
		{
			if (tokens.Contents.Contains(item))
			{
				return tokens;
			}
			foreach (var token in tokens.Contents)
			{
				if (token is ITreeElement)
				{
					ITreeElement element = (ITreeElement)token;
					Tokens contents = element.TreeContents();
					if (null != Tree.Find(contents, item))
					{
                        return contents;
					}
				}
			}
			return null;
		}
	}
}