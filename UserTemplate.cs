﻿using System;
namespace Wikimedia
{
	public class UserTemplate: Template
	{
		public UserTemplate(Page page, string user): base (page, "user0")
		{
            Add(1, user);
        }
	}
}