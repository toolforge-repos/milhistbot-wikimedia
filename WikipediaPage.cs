﻿using System;
namespace Wikimedia
{
	public class WikipediaPage: ArticlePage
    {
        private WikipediaTalkPage? talk;
        public new WikipediaTalkPage Talk
        {
            get => talk ??= new WikipediaTalkPage(TitleWithoutNamespace);
        }

        public WikipediaPage(string title, int revid = -1) : base(new Namespace("Wikipedia"), title, revid)
        {
        }
    }

    public class WikipediaTalkPage : TalkPage
    {        
        private WikipediaPage? page;
        public new WikipediaPage Article
        {
            get => page ??= new WikipediaPage(TitleWithoutNamespace);
        }

        public WikipediaTalkPage(string title, int revid = -1) : base(new Namespace("Wikipedia talk"), title, revid)
        {
        }
    }
}
